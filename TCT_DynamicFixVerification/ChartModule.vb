﻿Imports System.Windows.Forms.DataVisualization.Charting

Module ChartModule

	Private m_Chart As DataVisualization.Charting.Chart

	Public Sub SetChartReference(ByRef m_refChart As DataVisualization.Charting.Chart)
		m_Chart = m_refChart
	End Sub

	Private Enum GraphIDEnum
		GRAPH_0 = 0
		GRAPH_1 = 1
		GRAPH_2 = 2
		GRAPH_3 = 3
		GRAPH_4 = 4
		GRAPH_5 = 5
		GRAPH_6 = 6
		GRAPH_7 = 7
		GRAPH_8 = 8
		GRAPH_9 = 9
		GRAPH_10 = 10
		GRAPH_11 = 11
		GRAPH_12 = 12
		GRAPH_13 = 13
		GRAPH_14 = 14
		GRAPH_15 = 15
		GRAPH_16 = 16
		GRAPH_17 = 17
		GRAPH_18 = 18
		GRAPH_19 = 19
		GRAPH_20 = 20
		GRAPH_21 = 21
		GRAPH_22 = 22
		GRAPH_23 = 23
		GRAPH_24 = 24
		GRAPH_25 = 25
		GRAPH_26 = 26
		GRAPH_27 = 27
		GRAPH_28 = 28
		GRAPH_29 = 29
		GRAPH_MAX = 30
	End Enum


	Public Sub InitializeChart()

		m_Chart.Series.Clear()

		For i = 0 To GraphIDEnum.GRAPH_MAX - 1
			m_Chart.Series.Add(New Series())
			m_Chart.Series(i).ChartType = SeriesChartType.Line
			m_Chart.Series(i).Color = System.Drawing.Color.Black
			m_Chart.Series(i).ChartArea = "ChartArea1"
			m_Chart.Series(i).IsVisibleInLegend = False
		Next

		m_Chart.ChartAreas("ChartArea1").AxisX.LabelStyle.Enabled = False
		m_Chart.ChartAreas("ChartArea1").AxisX.Minimum = 0
		m_Chart.ChartAreas("ChartArea1").AxisX.Maximum = 500
		m_Chart.ChartAreas("ChartArea1").AxisX.LineColor = Color.Red
		m_Chart.ChartAreas("ChartArea1").AxisX.Crossing = DATA_POINTS_MIDPT '0.0
		m_Chart.ChartAreas("ChartArea1").AxisX.Interval = 50

		m_Chart.ChartAreas("ChartArea1").AxisY.Minimum = 10000 ' -500 ' -200
		m_Chart.ChartAreas("ChartArea1").AxisY.Maximum = 60000 ' 800
		m_Chart.ChartAreas("ChartArea1").AxisY.Interval = 5000
		m_Chart.ChartAreas("ChartArea1").AxisY.LineColor = Color.Red
		m_Chart.ChartAreas("ChartArea1").AxisY.Crossing = 35000.0
		m_Chart.ChartAreas("ChartArea1").AxisY.LabelStyle.Enabled = False

		m_Chart.Series(0).Points.AddXY(0, 0)

	End Sub

	Public Sub DrawLine(ByVal iChannel As Integer, ByVal iEyesetting As Integer, ByVal iShot As Integer, ByVal iNumPoints As Integer, ByVal iWaveform() As Long)
		Dim iIdx As Integer = 1
		Dim iSeries As Integer = 0
		If iChannel = SCOPE_CHANNEL.SCOPE_CHANNEL_1 Then
			If iEyesetting = EYESETTING.EYESET_MAX Then
				iSeries = GraphIDEnum.GRAPH_0
			ElseIf iEyesetting = EYESETTING.EYESET_HI_C Then
				Select Case iShot
					Case 1
						iSeries = GraphIDEnum.GRAPH_0
					Case 2
						iSeries = GraphIDEnum.GRAPH_1
					Case 3
						iSeries = GraphIDEnum.GRAPH_2
					Case 4
						iSeries = GraphIDEnum.GRAPH_3
					Case 5
						iSeries = GraphIDEnum.GRAPH_4
				End Select
			ElseIf iEyesetting = EYESETTING.EYESET_MID_B Then
				Select Case iShot
					Case 1
						iSeries = GraphIDEnum.GRAPH_5
					Case 2
						iSeries = GraphIDEnum.GRAPH_6
					Case 3
						iSeries = GraphIDEnum.GRAPH_7
					Case 4
						iSeries = GraphIDEnum.GRAPH_8
					Case 5
						iSeries = GraphIDEnum.GRAPH_9
				End Select
			ElseIf iEyesetting = EYESETTING.EYESET_LOW_A Then
				Select Case iShot
					Case 1
						iSeries = GraphIDEnum.GRAPH_10
					Case 2
						iSeries = GraphIDEnum.GRAPH_11
					Case 3
						iSeries = GraphIDEnum.GRAPH_12
					Case 4
						iSeries = GraphIDEnum.GRAPH_13
					Case 5
						iSeries = GraphIDEnum.GRAPH_14
				End Select
			End If
		Else 'CHANNEL 2
			If iEyesetting = EYESETTING.EYESET_MAX Then
				iSeries = GraphIDEnum.GRAPH_15
			ElseIf iEyesetting = EYESETTING.EYESET_HI_C Then
				Select Case iShot
					Case 1
						iSeries = GraphIDEnum.GRAPH_15
					Case 2
						iSeries = GraphIDEnum.GRAPH_16
					Case 3
						iSeries = GraphIDEnum.GRAPH_17
					Case 4
						iSeries = GraphIDEnum.GRAPH_18
					Case 5
						iSeries = GraphIDEnum.GRAPH_19
				End Select
			ElseIf iEyesetting = EYESETTING.EYESET_MID_B Then
				Select Case iShot
					Case 1
						iSeries = GraphIDEnum.GRAPH_20
					Case 2
						iSeries = GraphIDEnum.GRAPH_21
					Case 3
						iSeries = GraphIDEnum.GRAPH_22
					Case 4
						iSeries = GraphIDEnum.GRAPH_23
					Case 5
						iSeries = GraphIDEnum.GRAPH_24
				End Select
			ElseIf iEyesetting = EYESETTING.EYESET_LOW_A Then
				Select Case iShot
					Case 1
						iSeries = GraphIDEnum.GRAPH_25
					Case 2
						iSeries = GraphIDEnum.GRAPH_26
					Case 3
						iSeries = GraphIDEnum.GRAPH_27
					Case 4
						iSeries = GraphIDEnum.GRAPH_28
					Case 5
						iSeries = GraphIDEnum.GRAPH_29
				End Select
			End If
		End If

		m_Chart.Series(iSeries).Points.Clear()
		For iIdx = 0 To iNumPoints
			m_Chart.Series(iSeries).Points.AddXY(CDbl(iIdx), CDbl(iWaveform(iIdx)))
		Next

	End Sub
	Public Sub ClearCharts()
		If m_Chart.Series.Count > 0 Then
			For iIdx = 0 To GraphIDEnum.GRAPH_MAX - 1
				m_Chart.Series(iIdx).Points.Clear()
			Next
		End If
		m_Chart.Invalidate()
		m_Chart.Update()
	End Sub

End Module
