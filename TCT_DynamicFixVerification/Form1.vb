﻿Imports TCT_DynamicFixVerification.SerialPortClass
Imports TCT_DynamicFixVerification.OrigDatabaseModule
Imports TCT_DynamicFixVerification.OutFileModule
Imports TCT_DynamicFixVerification.TestFunctionModule
Imports TCT_DynamicFixVerification.ConfigModule

Imports System.Windows.Forms.DataVisualization.Charting

Imports System.Deployment.Application
Imports System.ComponentModel

Public Class Form1

	Private m_refXAxisLabel As Label
	Private m_refYAxisLabel As Label
	Private m_refLabel8 As Label
	Private m_refSerialNum As TextBox
	Private m_refStatus As TextBox
	Private m_refTCTPatApp As TextBox
	Private m_refTCTStat As TextBox
	Private m_refTCTTrig As TextBox
	Private m_refTCTTrigStat As TextBox
	Private m_refPatApp As TextBox
	Private m_refDruckStat As TextBox
	Private m_refStartCalVerBtn As Button
	Private m_refStartDynCalBtn As Button

	Private m_refChart As DataVisualization.Charting.Chart

	'Private mCommPort1 As SerialPortClass
	Private mCommPort2 As SerialPortClass

	Public Const MAX_COM_PORTS As Integer = 6

	'****************************************************************************
	'
	'	Name:			Sub InitLabelReferences()
	'
	'	Purpose:		Initializes the .
	'
	'	Resources:		NONE
	'
	'	Interactions:	NONE
	'
	'	Inputs:			NONE
	'
	'	Outputs:		NONE
	'
	'	Dependencies: 	NONE
	'
	'	Err Conditions:	NONE
	'
	'	Notes:		    
	'
	'	History:		02/05/18 - J. Dillon: Created.
	'
	'***************************************************************************
	Private Sub InitLabelReferences()
		m_refXAxisLabel = Label3
		m_refYAxisLabel = Label2
		m_refLabel8 = Label8
		m_refStatus = txtStatus
		m_refSerialNum = txtTCTNumber
		m_refTCTPatApp = txtTCTPatApp
		m_refTCTStat = txtTCTStat
		m_refTCTTrig = txtTCTTrig
		m_refTCTTrigStat = txtTCTTrigStat
		m_refPatApp = txtPatApp
		m_refDruckStat = txtDruckStat
		m_refChart = Chart1
	End Sub

	Private Sub cboTestSelection_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTestSelection.SelectedIndexChanged
		If cboTestSelection.SelectedIndex = 0 Then
			Label1.Text = ""
			btnStartTest.Enabled = False
		ElseIf cboTestSelection.SelectedIndex = CalType.CAL_TYPE_DYN Then
			Label1.Text = "DYNAMIC CALIBRATOR SW Rev: " & Application.ProductVersion
			' Go to Project Properties, Application, Assembly Info, File Version to update visible version
			btnStartTest.Enabled = True
		ElseIf cboTestSelection.SelectedIndex = CalType.CAL_TYPE_TCT Then
			Label1.Text = "SLIP-ON CALIBRATOR SW Rev: " & Application.ProductVersion

			btnStartTest.Enabled = True
		End If
	End Sub

	Private Sub Button3_Click(sender As Object, e As EventArgs) Handles btnQuit.Click
		Me.Close()
	End Sub

	Private Sub btnStartTest_Click(sender As Object, e As EventArgs) Handles btnStartTest.Click
		Dim sError As String = ""

		If mCommPort2 Is Nothing Then
			mCommPort2 = New SerialPortClass
		End If

		If Not mCommPort2.IsOpen Then
			If Not mCommPort2.OpenPort(cboCommPort.SelectedItem, sError) Then
				txtStatus.Text = sError
				Exit Sub
			End If
		End If

		RunTest()

	End Sub

	Private Sub RunTest()
		Dim sStatus As String = ""
		Dim bSuccess As Boolean = True

		Dim sError As String = ""
		If Not OrigDatabaseModule.OpenMySqlDatabase(sError) Then
			txtStatus.Text = sError
			Exit Sub
		End If

		If cboTestSelection.SelectedIndex = 0 Then
			txtStatus.Text = "Select a valid test"
			Exit Sub
		End If


		TestFunctionModule.SetTestParameters(cboTestSelection.SelectedIndex, mCommPort2, m_refXAxisLabel, m_refYAxisLabel, m_refLabel8,
											 m_refStatus, m_refSerialNum, m_refTCTPatApp, m_refTCTStat, m_refTCTTrig, m_refTCTTrigStat,
											 m_refPatApp, m_refDruckStat, m_refChart)

		If Not TestFunctionModule.RunTesting() Then
			Me.Refresh()
			Exit Sub
		End If
	End Sub

	Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
		Dim sPort As String = ""

		cboCommPort.Items.Clear()

		For i As Integer = 1 To MAX_COM_PORTS
			cboCommPort.Items.Add("COM" & i.ToString)
		Next

		sPort = ConfigModule.GetCommPort()
		sPort.Trim()

		If sPort.Length > 0 Then
			If CInt(sPort) > cboCommPort.Items.Count - 1 Then
				cboCommPort.Items.Add("COM" & sPort)
			End If

			sPort = "COM" & sPort
			cboCommPort.SelectedItem = sPort
		Else
			cboCommPort.SelectedIndex = 0
		End If
	End Sub


	Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCommPort.SelectedIndexChanged
		If cboCommPort.Items.Count > 0 Then
			ConfigModule.SetCommPort(cboCommPort.SelectedItem)
			' Close previously selected COM Port
			If Not mCommPort2 Is Nothing Then
				mCommPort2.Close()
			End If
		End If
	End Sub

	Private Sub Form1_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
		ConfigModule.WriteConfigFile()
	End Sub

	Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
		Dim sError As String = ""
		If Not OrigDatabaseModule.OpenMySqlDatabase(sError) Then
			txtStatus.Text = sError
			Exit Sub
		End If

		OrigDatabaseModule.Remove_DB_Entry(TextBox1.Text)
	End Sub
End Class
