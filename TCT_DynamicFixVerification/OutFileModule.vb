﻿'****************************************************************************
'
'	File:		OutFileModule.vb
'
'	Project:	VRxSoftwareDrawingApp
'
'	History:    04/24/18 - J. Dillon: Initial Version 
'
'	Purpose:	This file provides operations for creating output that will 
'				be opened In excel And used For the software drawing.
'
'	Functions:	Sub WriteFile(ByVal sFileName As String, ByRef sMessage As String)
'
'	Functions affecting patient safety:	none.
'
'	Copyright (C) Reichert Inc. 2018.  All rights reserved.
'
'***************************************************************************
'Imports Microsoft.Office.Interop
Module OutFileModule

	Dim m_fsFileWriter1 As IO.StreamWriter

	Dim sPath As String = "S:\Everyone\Dillon\TCT_testing\"
	Dim sPath1 As String = sPath '"s:\Products\TCT Test Fixture\TCT dynamic calibrator\P1_P2_Data\"
	Dim sPath2 As String = sPath '"s:\Products\TCT Test Fixture\TCT dynamic calibrator\test_cal data\"

	'	1. "s:\Products\TCT Test Fixture\TCT dynamic calibrator\P1_P2_Data\TCT_" & m_TCTSerialNum & "_Setting_" & Eyesetting & ".txt"
	'	2. "s:\Products\TCT Test Fixture\TCT dynamic calibrator\test_cal data\tct_" & m_TCTSerialNum & "_cal.txt"
	'	3.  s:\Products\TCT Test Fixture\TCT dynamic calibrator\test_cal data\cpk_tct_" & m_TCTSerialNum & ".txt"
	'	4.  s:\Products\TCT Test Fixture\TCT dynamic calibrator\test_cal data\CofV_TCT_Fix_" & m_TCTSerialNum & "_" & tempstring & ".txt"
	'	5.  s:\Products\TCT Test Fixture\TCT dynamic calibrator\test_cal data\CofC_dyn_cal_fix_" & DynCal_Num & "_" & tempstring & ".txt"
	'	6.  s:\Products\TCT Test Fixture\TCT dynamic calibrator\test_cal data\tct_" & m_TCTSerialNum & "_correlation.txt"
	'	7.  s:\Products\TCT Test Fixture\TCT dynamic calibrator\test_cal data\cpk_tct_" & m_TCTSerialNum & "_correlation.txt"

	'****************************************************************************
	'
	'	Name:	Sub WriteFile1(ByVal sFileName As String, ByRef sMessage As String)		
	'
	'	Purpose:		Write a comma-separated text file with driver data.
	'
	'	Resources:		NONE
	'
	'	Interactions:	NONE
	'
	'	Inputs:			sFileName - Name of the file to be written
	'
	'	Outputs:		sMessage - Message detailing success or failure
	'
	'	Dependencies: 	NONE
	'
	'	Err Conditions:	NONE
	'
	'	Notes:		    
	'
	'	History:		04/24/18 - J. Dillon: Created.
	'
	'***************************************************************************
	Public Sub WriteFile1(ByVal bAppend As Boolean, ByVal sSerialNum As String, ByVal iEyeSetting As Integer, ByVal Shot As Integer,
						  ByVal DynCal_Num As Integer, ByVal P1_Druck_trigger As Single, ByVal P2_Druck_trigger As Single)

		'Convert 0-based eye setting from 0,1,2 to 1,2,3
		Dim sEyeSetting As String = (iEyeSetting + 1).ToString

		Dim sFilename As String = sPath1 & "TCT_" & sSerialNum & "_Setting_" & sEyeSetting & ".txt"
		Dim sLine As String = ""
		Dim dtNow As DateTime = DateTime.Now
		Dim sTimeStamp As String = dtNow.ToString("G")

		Try
			m_fsFileWriter1 = My.Computer.FileSystem.OpenTextFileWriter(sFilename, bAppend)

			' Line looks like this in file => 1, 5/31/2012 10:06:41 AM, 2, 1.54552529146895, 1.470985
			sLine = Shot & ", " & sTimeStamp & ", " & DynCal_Num & ", " & P1_Druck_trigger & ", " & P2_Druck_trigger
			m_fsFileWriter1.WriteLine(sLine)

			m_fsFileWriter1.Close()
		Catch ex As Exception
			MsgBox("ERROR writing file TCT_<SN>_Setting_<SETTING>.txt: " & ex.Message, vbExclamation)
			Exit Sub
		End Try

	End Sub

	Public Sub WriteFile2Header(ByVal bAppend As Boolean, ByVal sSerialNum As String, ByVal sOperator As String, strtDruck As DruckMaxMin)

		Dim sFilename As String = sPath2 & "tct_" & sSerialNum & "_cal.txt"
		Dim sLine As String = ""
		Dim dtNow As DateTime = DateTime.Now
		Dim sTimeStamp As String = dtNow.ToString("G")

		Try
			m_fsFileWriter1 = My.Computer.FileSystem.OpenTextFileWriter(sFilename, bAppend)

			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine(sTimeStamp)
			m_fsFileWriter1.WriteLine("Operator: " & sOperator)
			m_fsFileWriter1.WriteLine("TCT serial Number = " & sSerialNum)
			m_fsFileWriter1.WriteLine("C_DRUCK_MAX = " & strtDruck.C_DRUCK_MAX)
			m_fsFileWriter1.WriteLine("C_DRUCK_MIN = " & strtDruck.C_DRUCK_MIN)
			m_fsFileWriter1.WriteLine("B_DRUCK_MAX = " & strtDruck.B_DRUCK_MAX)
			m_fsFileWriter1.WriteLine("B_DRUCK_MIN = " & strtDruck.B_DRUCK_MIN)
			m_fsFileWriter1.WriteLine("A_DRUCK_MAX = " & strtDruck.A_DRUCK_MAX)
			m_fsFileWriter1.WriteLine("A_DRUCK_MIN = " & strtDruck.A_DRUCK_MIN)

			m_fsFileWriter1.Close()
		Catch ex As Exception
			MsgBox("ERROR writing file tct_<SN>_cal.txt: " & ex.Message, vbExclamation)
			Exit Sub
		End Try

	End Sub


	Public Sub WriteFile3(ByVal bAppend As Boolean, ByVal sSerialNum As String, ByVal sOperator As String, ByVal NumShots As Integer, ByVal strt_TCT_Setting() As TCT_Setting)

		Dim sFilename As String = sPath2 & "cpk_tct_" & sSerialNum & ".txt"
		Dim sLine As String = ""
		Dim dtNow As DateTime = DateTime.Now
		Dim sTimeStamp As String = dtNow.ToString("G")
		Dim Temp As Integer = 1

		Try
			m_fsFileWriter1 = My.Computer.FileSystem.OpenTextFileWriter(sFilename, bAppend)

			For Temp = 1 To NumShots
				m_fsFileWriter1.WriteLine(sSerialNum & ", " & sTimeStamp & ", " & sOperator & ", " & Temp & ", " &
										  strt_TCT_Setting(EYESETTING.EYESET_LOW_A).V_TCT(Temp) & ", " &
										  strt_TCT_Setting(EYESETTING.EYESET_MID_B).V_TCT(Temp) & ", " &
										  strt_TCT_Setting(EYESETTING.EYESET_HI_C).V_TCT(Temp) & ", " &
										  strt_TCT_Setting(EYESETTING.EYESET_LOW_A).V_Druck(Temp) & ", " &
										  strt_TCT_Setting(EYESETTING.EYESET_MID_B).V_Druck(Temp) & ", " &
										  strt_TCT_Setting(EYESETTING.EYESET_HI_C).V_Druck(Temp))
			Next Temp

			m_fsFileWriter1.Close()
		Catch ex As Exception
			MsgBox("ERROR writing file cpk_tct_<SN>.txt: " & ex.Message, vbExclamation)
			Exit Sub
		End Try

	End Sub

	Public Sub WriteFile4(ByVal bAppend As Boolean, ByVal sSerialNum As String, ByVal sOperator As String, ByVal strct_DBData As TCT_Trigger_Voltages_Struct, ByVal strct_Averages() As Averages, ByVal NumShots As Integer, ByVal iDynCalNum As Integer, ByVal strtDruck As DruckMaxMin, ByVal strtSetting As SettingMaxMin)

		'Fisrt get date, and re-configue it for appending to the filename
		Dim dtNow As DateTime = DateTime.Now
		Dim dtNextCalDate As DateTime = dtNow.AddMonths(3)

		Dim sDateInFile As String = dtNow.ToString("d")
		Dim sNextCalDate As String = dtNextCalDate.ToString("d")
		Dim sDateFileName As String = dtNow.ToString("MM_dd_yyyy")

		Dim sFilename As String = sPath2 & "CofV_TCT_Fix_" & sSerialNum & "_" & sDateFileName & ".txt"
		Dim sTCTtype As String = "13906"

		Try
			m_fsFileWriter1 = My.Computer.FileSystem.OpenTextFileWriter(sFilename, bAppend)

			m_fsFileWriter1.WriteLine("                              VERIFICATION CERTIFICATE")
			m_fsFileWriter1.WriteLine("                         NCT Tonometer Calibration Tool(TCT)")
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()

			m_fsFileWriter1.WriteLine(" Model #:                             " & sTCTtype)
			m_fsFileWriter1.WriteLine(" Fixture SN:                          " & sSerialNum)
			m_fsFileWriter1.WriteLine(" Date:                                " & sDateInFile)

			m_fsFileWriter1.WriteLine(" Created by:                          " & sOperator)
			m_fsFileWriter1.WriteLine(" Dynamic Calibrator Used(13905-803):  " & iDynCalNum)
			m_fsFileWriter1.WriteLine(" Software Rev. of Test Code:          " & Application.ProductVersion)
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine(" TCT Setting     Druck at        Allowed Druck       TCT at     Allowed TCT  ")
			m_fsFileWriter1.WriteLine("               Applanation.           Range       Applanation      Range     ")
			m_fsFileWriter1.WriteLine("                 (Volts)             (Volts)        (Volts)       (Volts)    ")
			m_fsFileWriter1.WriteLine(" -----------   ------------     ---------------   -----------   -----------  ")
			m_fsFileWriter1.WriteLine("     L             " & Format(strct_Averages(EYESETTING.EYESET_LOW_A).AvgDruck, "0.00") &
						 "           " & Format(strtDruck.A_DRUCK_MIN, "0.00") & " - " & Format(strtDruck.A_DRUCK_MAX, "0.00") &
						 "         " & Format(strct_Averages(EYESETTING.EYESET_LOW_A).AvgTCT, "0.00") & "      " & Format(strtSetting.A_SETTING_MIN, "0.00") & " - " & Format(strtSetting.A_SETTING_MAX, "0.00"))
			m_fsFileWriter1.WriteLine("     M             " & Format(strct_Averages(EYESETTING.EYESET_MID_B).AvgDruck, "0.00") &
						 "           " & Format(strtDruck.B_DRUCK_MIN, "0.00") & " - " & Format(strtDruck.B_DRUCK_MAX, "0.00") &
						 "         " & Format(strct_Averages(EYESETTING.EYESET_MID_B).AvgTCT, "0.00") & "      " & Format(strtSetting.B_SETTING_MIN, "0.00") & " - " & Format(strtSetting.B_SETTING_MAX, "0.00"))
			m_fsFileWriter1.WriteLine("     H             " & Format(strct_Averages(EYESETTING.EYESET_HI_C).AvgDruck, "0.00") &
						 "           " & Format(strtDruck.C_DRUCK_MIN, "0.00") & " - " & Format(strtDruck.C_DRUCK_MAX, "0.00") &
						 "         " & Format(strct_Averages(EYESETTING.EYESET_HI_C).AvgTCT, "0.00") & "      " & Format(strtSetting.C_SETTING_MIN, "0.00") & " - " & Format(strtSetting.C_SETTING_MAX, "0.00"))
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine(" Cal. Sheet Pressure    Allowed Pressure        Slope        Offset     R^2")
			m_fsFileWriter1.WriteLine("      (mmHg)             Range (mmHg)        (volts/mmHg)    (volts)      ")
			m_fsFileWriter1.WriteLine(" ------------------     -----------------    ------------    -------    ------")
			m_fsFileWriter1.WriteLine("     L - " & Format(strct_DBData.IOP_TCT(EYESETTING.EYESET_LOW_A), "00.00") & "            " & Format((strct_DBData.IOP_TCT(EYESETTING.EYESET_LOW_A) - 1), "00.00") & " - " & Format((strct_DBData.IOP_TCT(EYESETTING.EYESET_LOW_A) + 1), "00.00") & "         " & Format(strct_DBData.TCT_Slope, "0.0000") & "        " & Format(strct_DBData.TCT_Offset, "0.0000") & "    " & Format(strct_DBData.TCT_R2, "0.0000"))
			m_fsFileWriter1.WriteLine("     M - " & Format(strct_DBData.IOP_TCT(EYESETTING.EYESET_MID_B), "00.00") & "            " & Format((strct_DBData.IOP_TCT(EYESETTING.EYESET_MID_B) - 1), "00.00") & " - " & Format(strct_DBData.IOP_TCT(EYESETTING.EYESET_MID_B) + 1, "0.00"))
			m_fsFileWriter1.WriteLine("     H - " & Format(strct_DBData.IOP_TCT(EYESETTING.EYESET_HI_C), "00.00") & "            " & Format((strct_DBData.IOP_TCT(EYESETTING.EYESET_HI_C) - 1.5), "00.00") & " - " & Format(strct_DBData.IOP_TCT(EYESETTING.EYESET_HI_C) + 1.5, "0.00"))
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine(" TCT Setting    P1 Fire    P2 Fire")
			m_fsFileWriter1.WriteLine(" -----------    -------    -------")
			m_fsFileWriter1.WriteLine("     L            OK         OK")
			m_fsFileWriter1.WriteLine("     M            OK         OK")
			m_fsFileWriter1.WriteLine("     H            OK         OK")
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine(" Note: 1)Measured TCT volts(at Trigger point) Is an average of " & NumShots & " measurements.")
			m_fsFileWriter1.WriteLine("       2)Druck Voltage Is a fixed value where the O'scope triggers, and ")
			m_fsFileWriter1.WriteLine("         the point where the TCT voltage is Measured and recorded.")
			For Index = 1 To 6
				m_fsFileWriter1.WriteLine()
			Next Index
			m_fsFileWriter1.WriteLine(" Authorized Signature: ________________________         Date: _____________")
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine("Reference the latest revision of: RQF-05.18")
			m_fsFileWriter1.WriteLine()

			m_fsFileWriter1.Close()

			MsgBox("Verification Complete. A Certificate of Cal. file has been " &
				   "created on--> s:\Products\TCT Test Fixture\TCT dynamic calibrator\test_cal data\\CofV_TCT_Fix_#_date.txt.", vbOKOnly)

		Catch ex As Exception
			MsgBox("Verification Complete. ERROR writing Certificate of Cal. file: " & ex.Message, vbExclamation)
			Exit Sub
		End Try

	End Sub


	Public Sub WriteFile5(ByVal bAppend As Boolean, ByVal sSerialNum As String, ByVal sOperator As String, ByVal iDynCalNum As Integer, ByVal sngRampRate() As Single, ByVal VmaxPress() As Single)

		'Fisrt get date, and re-configue it for appending to the filename
		Dim dtNow As DateTime = DateTime.Now
		Dim dtNextCalDate As DateTime = dtNow.AddYears(1)

		Dim sDateFileName As String = dtNow.ToString("MM_dd_yyyy")
		Dim sNextCalDate As String = dtNextCalDate.ToString("d")
		Dim sTimeStamp As String = dtNow.ToString("G")

		Dim sFilename As String = sPath2 & "CofC_dyn_cal_fix_" & iDynCalNum & "_" & sDateFileName & ".txt"
		Dim sTCTtype As String = "13905-803"

		Try
			m_fsFileWriter1 = My.Computer.FileSystem.OpenTextFileWriter(sFilename, bAppend)

			'	Open CAL_Filename For Output As #1
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine("CERTIFICATE OF CALIBRATION")
			m_fsFileWriter1.WriteLine("DYNAMIC CALIBRATOR FOR THE TCT")
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine("Fixture:" & vbTab & vbTab & vbTab & "Dynamic Calibrator: " & sTCTtype)
			m_fsFileWriter1.WriteLine("Fixture SN:" & vbTab & vbTab & vbTab & iDynCalNum)
			m_fsFileWriter1.WriteLine("Date:" & vbTab & vbTab & vbTab & vbTab & sTimeStamp)
			m_fsFileWriter1.WriteLine("Created by:" & vbTab & vbTab & vbTab & sOperator)
			m_fsFileWriter1.WriteLine("Due for Calibration:" & vbTab & vbTab & sNextCalDate)
			m_fsFileWriter1.WriteLine("Software Rev. of Test Code:" & vbTab & Application.ProductVersion)
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine(vbTab & vbTab & vbTab & vbTab & "Measured values" & vbTab & vbTab & "Allowed Range" & vbTab & "Result")
			m_fsFileWriter1.WriteLine(vbTab & vbTab & vbTab & vbTab & "---------------" & vbTab & vbTab & "-------------" & vbTab & "------")
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine("Pres. Ramp rate: Drop 1" & vbTab & vbTab & sngRampRate(1) & " (PSI / ms)" & vbTab & vbTab & RRMIN & " - " & RRMAX & vbTab & "PASS")
			m_fsFileWriter1.WriteLine("Pres. Ramp rate: Drop 2" & vbTab & vbTab & sngRampRate(2) & vbTab & vbTab & vbTab & RRMIN & " - " & RRMAX & vbTab & "PASS")
			m_fsFileWriter1.WriteLine("Pres. Ramp rate: Drop 3" & vbTab & vbTab & sngRampRate(3) & vbTab & vbTab & vbTab & RRMIN & " - " & RRMAX & vbTab & "PASS")
			m_fsFileWriter1.WriteLine("Peak Presssure : Drop 1" & vbTab & vbTab & VmaxPress(1) & " (PSI)" & vbTab & vbTab & PMIN & " - " & PMAX & vbTab & "PASS")
			m_fsFileWriter1.WriteLine("Peak Presssure : Drop 2" & vbTab & vbTab & VmaxPress(2) & vbTab & vbTab & vbTab & PMIN & " - " & PMAX & vbTab & "PASS")
			m_fsFileWriter1.WriteLine("Peak Presssure : Drop 3 " & vbTab & VmaxPress(3) & vbTab & vbTab & vbTab & PMIN & " - " & PMAX & vbTab & "PASS")

			For Index = 1 To 15
				m_fsFileWriter1.WriteLine()
			Next Index
			m_fsFileWriter1.WriteLine("Authorized Signature: ________________________         Date: _____________")
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine("RQF 5.24")
			m_fsFileWriter1.WriteLine("Rev A")
			m_fsFileWriter1.WriteLine("10/2/2012")

			m_fsFileWriter1.Close()

			MsgBox("Testing Passed. Cert of Cal available on s:\Products\TCT Test Fixture\TCT dynamic calibrator\test_cal data\CofC_dyn_cal_fix_#_date.txt", vbOKOnly)

		Catch ex As Exception
			MsgBox("Testing Passed. ERROR writing Cert of Cal: " & ex.Message, vbExclamation)
			Exit Sub
		End Try

	End Sub


	Public Sub WriteFile6Header(ByVal bAppend As Boolean, ByVal sSerialNum As String, ByVal sOperator As String)

		Dim sFilename As String = sPath2 & "cpk_tct_" & sSerialNum & ".txt"
		Dim sLine As String = ""
		Dim dtNow As DateTime = DateTime.Now
		Dim sTimeStamp As String = dtNow.ToString("G")
		Dim Temp As Integer = 1

		Try
			m_fsFileWriter1 = My.Computer.FileSystem.OpenTextFileWriter(sFilename, bAppend)

			m_fsFileWriter1.WriteLine()
			m_fsFileWriter1.WriteLine(sTimeStamp)
			m_fsFileWriter1.WriteLine("Operator: " & sOperator)
			m_fsFileWriter1.WriteLine("TCT serial Number = " & sSerialNum)

			m_fsFileWriter1.Close()
		Catch ex As Exception
			MsgBox("ERROR writing file cpk_tct_<SN>.txt: " & ex.Message, vbExclamation)
			Exit Sub
		End Try

	End Sub

	Public Sub WriteFile7(ByVal bAppend As Boolean, ByVal sSerialNum As String, ByVal sOperator As String, ByVal NumShots As Integer, V_Corr_TCT(,) As Single)

		Dim sFilename As String = sPath2 & "cpk_tct_" & sSerialNum & "_correlation.txt"
		Dim sLine As String = ""
		Dim dtNow As DateTime = DateTime.Now
		Dim sTimeStamp As String = dtNow.ToString("G")
		Dim Shot As Integer = 1

		Try
			m_fsFileWriter1 = My.Computer.FileSystem.OpenTextFileWriter(sFilename, bAppend)

			For Shot = 1 To NumShots
				m_fsFileWriter1.WriteLine(sSerialNum & ", " & sTimeStamp & ", " & sOperator & ", " & Shot & ", " & V_Corr_TCT(EYESETTING.EYESET_LOW_A, Shot) & ", " & V_Corr_TCT(EYESETTING.EYESET_MID_B, Shot) & ", " & V_Corr_TCT(EYESETTING.EYESET_HI_C, Shot))
			Next

			m_fsFileWriter1.Close()
		Catch ex As Exception
			MsgBox("ERROR writing file cpk_tct_<SN>_correlation.txt: " & ex.Message, vbExclamation)
			Exit Sub
		End Try

	End Sub

	Public Sub AppendToFile(ByVal iFileID As Integer, ByVal sSerialNum As String, ByVal eStage As Stages, ByVal iEyesetting As Integer,
							ByVal iShot As Integer, ByVal iNumShots As Integer, ByVal SDTCT As Single, ByVal SDDRUCK As Single, ByVal strct_Averages() As Averages,
							ByVal strt_TCTSetting() As TCT_Setting, V_Corr_TCT(,) As Single)
		Dim sFilename As String = ""

		If iFileID = 6 Then
			sFilename = sPath2 & "cpk_tct_" & sSerialNum & ".txt"
		ElseIf iFileID = 2 Then
			sFilename = sPath2 & "tct_" & sSerialNum & "_cal.txt"
		End If

		Try
			m_fsFileWriter1 = My.Computer.FileSystem.OpenTextFileWriter(sFilename, True)

			'TCT & druck data
			If eStage = Stages.STAGE_VERIFY Then
				If iEyesetting = EYESETTING.EYESET_LOW_A Then
					m_fsFileWriter1.WriteLine("Drop # " & iShot & " TCT setting  " & "A" & ",,,  V(TCT) = " & strt_TCTSetting(iEyesetting).V_TCT(iShot) & ",,,  V(Druck) = " & strt_TCTSetting(iEyesetting).V_Druck(iShot))
				ElseIf iEyesetting = EYESETTING.EYESET_MID_B Then
					m_fsFileWriter1.WriteLine("Drop # " & iShot & " TCT setting  " & "B" & ",,,,,,,  V(TCT) = " & strt_TCTSetting(iEyesetting).V_TCT(iShot) & ",,,,,,,  V(Druck) = " & strt_TCTSetting(iEyesetting).V_Druck(iShot))
				Else
					m_fsFileWriter1.WriteLine("Drop # " & iShot & " TCT setting  " & "C" & ",,,,,,,,,,,  V(TCT) = " & strt_TCTSetting(iEyesetting).V_TCT(iShot) & ",,,,,,,,,,,  V(Druck) = " & strt_TCTSetting(iEyesetting).V_Druck(iShot))
				End If
			ElseIf eStage = Stages.STAGE_CORRELATE Then
				If iEyesetting = EYESETTING.EYESET_LOW_A Then
					m_fsFileWriter1.WriteLine("Drop # " & iShot & " TCT setting  " & "A" & ",,,  V(TCT) = " & V_Corr_TCT(iEyesetting, iShot) & ",,,  V(Druck) = " & strt_TCTSetting(iEyesetting).V_Corr_Druck(iShot))
				ElseIf iEyesetting = EYESETTING.EYESET_MID_B Then
					m_fsFileWriter1.WriteLine("Drop # " & iShot & " TCT setting  " & "B" & ",,,,,,,  V(TCT) = " & V_Corr_TCT(iEyesetting, iShot) & ",,,,,,,  V(Druck) = " & strt_TCTSetting(iEyesetting).V_Corr_Druck(iShot))
				Else
					m_fsFileWriter1.WriteLine("Drop # " & iShot & " TCT setting  " & "C" & ",,,,,,,,,,,  V(TCT) = " & V_Corr_TCT(iEyesetting, iShot) & ",,,,,,,,,,,  V(Druck) = " & strt_TCTSetting(iEyesetting).V_Corr_Druck(iShot))
				End If
			End If

			'IF at end of shots for that setting save average and SD
			If iShot = iNumShots Then
				m_fsFileWriter1.WriteLine("TCT: Avg./SD = " & strct_Averages(iEyesetting).AvgTCT & ", " & SDTCT)
				m_fsFileWriter1.WriteLine("Druck: Avg./SD = " & strct_Averages(iEyesetting).AvgDruck & ", " & SDDRUCK)
				m_fsFileWriter1.WriteLine()
			End If

			m_fsFileWriter1.Close()
		Catch ex As Exception
			Dim sText As String

			If iFileID = 6 Then
				sText = "cpk_tct_<SN>.txt"
			ElseIf iFileID = 2 Then
				sText = "tct_<SN>_cal.txt"
			End If

			MsgBox("ERROR appending to file " & sText & ": " & ex.Message, vbExclamation)
		End Try
	End Sub

End Module
