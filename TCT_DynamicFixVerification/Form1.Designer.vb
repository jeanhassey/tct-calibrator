﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
		Me.Chart1 = New System.Windows.Forms.DataVisualization.Charting.Chart()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.txtTCTNumber = New System.Windows.Forms.TextBox()
		Me.txtStatus = New System.Windows.Forms.TextBox()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.btnQuit = New System.Windows.Forms.Button()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.Label7 = New System.Windows.Forms.Label()
		Me.Label8 = New System.Windows.Forms.Label()
		Me.Label9 = New System.Windows.Forms.Label()
		Me.Label10 = New System.Windows.Forms.Label()
		Me.txtTCTPatApp = New System.Windows.Forms.TextBox()
		Me.txtTCTStat = New System.Windows.Forms.TextBox()
		Me.txtTCTTrig = New System.Windows.Forms.TextBox()
		Me.txtTCTTrigStat = New System.Windows.Forms.TextBox()
		Me.txtPatApp = New System.Windows.Forms.TextBox()
		Me.txtDruckStat = New System.Windows.Forms.TextBox()
		Me.Label11 = New System.Windows.Forms.Label()
		Me.Label12 = New System.Windows.Forms.Label()
		Me.Label14 = New System.Windows.Forms.Label()
		Me.cboTestSelection = New System.Windows.Forms.ComboBox()
		Me.Label13 = New System.Windows.Forms.Label()
		Me.btnStartTest = New System.Windows.Forms.Button()
		Me.cboCommPort = New System.Windows.Forms.ComboBox()
		Me.Label15 = New System.Windows.Forms.Label()
		Me.Button1 = New System.Windows.Forms.Button()
		Me.TextBox1 = New System.Windows.Forms.TextBox()
		CType(Me.Chart1, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'Chart1
		'
		ChartArea1.Name = "ChartArea1"
		Me.Chart1.ChartAreas.Add(ChartArea1)
		Me.Chart1.Location = New System.Drawing.Point(149, 181)
		Me.Chart1.Name = "Chart1"
		Me.Chart1.Size = New System.Drawing.Size(534, 444)
		Me.Chart1.TabIndex = 0
		Me.Chart1.Text = "Chart1"
		'
		'Label1
		'
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.Location = New System.Drawing.Point(149, 128)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(534, 50)
		Me.Label1.TabIndex = 1
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'Label2
		'
		Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.Location = New System.Drawing.Point(7, 399)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(136, 19)
		Me.Label2.TabIndex = 4
		Me.Label2.Text = "1.0 Volt/Division"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'Label3
		'
		Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.Location = New System.Drawing.Point(360, 637)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(206, 19)
		Me.Label3.TabIndex = 5
		Me.Label3.Text = "1.0 ms/Division"
		'
		'txtTCTNumber
		'
		Me.txtTCTNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtTCTNumber.Location = New System.Drawing.Point(149, 668)
		Me.txtTCTNumber.Name = "txtTCTNumber"
		Me.txtTCTNumber.ReadOnly = True
		Me.txtTCTNumber.Size = New System.Drawing.Size(100, 22)
		Me.txtTCTNumber.TabIndex = 6
		'
		'txtStatus
		'
		Me.txtStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtStatus.Location = New System.Drawing.Point(149, 705)
		Me.txtStatus.Name = "txtStatus"
		Me.txtStatus.Size = New System.Drawing.Size(534, 22)
		Me.txtStatus.TabIndex = 7
		'
		'Label4
		'
		Me.Label4.AutoSize = True
		Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label4.Location = New System.Drawing.Point(91, 668)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(55, 20)
		Me.Label4.TabIndex = 8
		Me.Label4.Text = "TCT #:"
		'
		'Label5
		'
		Me.Label5.AutoSize = True
		Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label5.Location = New System.Drawing.Point(91, 706)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(56, 20)
		Me.Label5.TabIndex = 9
		Me.Label5.Text = "Status"
		'
		'btnQuit
		'
		Me.btnQuit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btnQuit.Location = New System.Drawing.Point(837, 687)
		Me.btnQuit.Name = "btnQuit"
		Me.btnQuit.Size = New System.Drawing.Size(123, 40)
		Me.btnQuit.TabIndex = 10
		Me.btnQuit.Text = "QUIT"
		Me.btnQuit.UseVisualStyleBackColor = True
		'
		'Label6
		'
		Me.Label6.AutoSize = True
		Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label6.Location = New System.Drawing.Point(730, 128)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(141, 32)
		Me.Label6.TabIndex = 11
		Me.Label6.Text = "TCT Pressure Sensor:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Last Measurement"
		'
		'Label7
		'
		Me.Label7.AutoSize = True
		Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label7.Location = New System.Drawing.Point(808, 242)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(61, 16)
		Me.Label7.TabIndex = 12
		Me.Label7.Text = "Statistics"
		'
		'Label8
		'
		Me.Label8.AutoSize = True
		Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label8.Location = New System.Drawing.Point(730, 327)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(162, 32)
		Me.Label8.TabIndex = 13
		Me.Label8.Text = "Calculated IOPg (mmHg): " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Last Measurement"
		'
		'Label9
		'
		Me.Label9.AutoSize = True
		Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label9.Location = New System.Drawing.Point(808, 442)
		Me.Label9.Name = "Label9"
		Me.Label9.Size = New System.Drawing.Size(61, 16)
		Me.Label9.TabIndex = 14
		Me.Label9.Text = "Statistics"
		'
		'Label10
		'
		Me.Label10.AutoSize = True
		Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label10.Location = New System.Drawing.Point(730, 514)
		Me.Label10.Name = "Label10"
		Me.Label10.Size = New System.Drawing.Size(118, 32)
		Me.Label10.TabIndex = 15
		Me.Label10.Text = "Druck Sensor:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Last Measurement"
		'
		'txtTCTPatApp
		'
		Me.txtTCTPatApp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtTCTPatApp.Location = New System.Drawing.Point(733, 181)
		Me.txtTCTPatApp.Name = "txtTCTPatApp"
		Me.txtTCTPatApp.Size = New System.Drawing.Size(227, 22)
		Me.txtTCTPatApp.TabIndex = 16
		'
		'txtTCTStat
		'
		Me.txtTCTStat.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtTCTStat.Location = New System.Drawing.Point(733, 217)
		Me.txtTCTStat.Name = "txtTCTStat"
		Me.txtTCTStat.Size = New System.Drawing.Size(227, 22)
		Me.txtTCTStat.TabIndex = 17
		'
		'txtTCTTrig
		'
		Me.txtTCTTrig.BackColor = System.Drawing.SystemColors.MenuText
		Me.txtTCTTrig.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtTCTTrig.ForeColor = System.Drawing.SystemColors.Window
		Me.txtTCTTrig.Location = New System.Drawing.Point(733, 377)
		Me.txtTCTTrig.Name = "txtTCTTrig"
		Me.txtTCTTrig.Size = New System.Drawing.Size(224, 22)
		Me.txtTCTTrig.TabIndex = 18
		'
		'txtTCTTrigStat
		'
		Me.txtTCTTrigStat.BackColor = System.Drawing.SystemColors.MenuText
		Me.txtTCTTrigStat.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtTCTTrigStat.ForeColor = System.Drawing.SystemColors.Window
		Me.txtTCTTrigStat.Location = New System.Drawing.Point(733, 417)
		Me.txtTCTTrigStat.Name = "txtTCTTrigStat"
		Me.txtTCTTrigStat.Size = New System.Drawing.Size(224, 22)
		Me.txtTCTTrigStat.TabIndex = 19
		'
		'txtPatApp
		'
		Me.txtPatApp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtPatApp.Location = New System.Drawing.Point(733, 567)
		Me.txtPatApp.Name = "txtPatApp"
		Me.txtPatApp.Size = New System.Drawing.Size(224, 22)
		Me.txtPatApp.TabIndex = 20
		'
		'txtDruckStat
		'
		Me.txtDruckStat.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtDruckStat.Location = New System.Drawing.Point(733, 603)
		Me.txtDruckStat.Name = "txtDruckStat"
		Me.txtDruckStat.Size = New System.Drawing.Size(224, 22)
		Me.txtDruckStat.TabIndex = 21
		'
		'Label11
		'
		Me.Label11.AutoSize = True
		Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label11.Location = New System.Drawing.Point(808, 637)
		Me.Label11.Name = "Label11"
		Me.Label11.Size = New System.Drawing.Size(61, 16)
		Me.Label11.TabIndex = 22
		Me.Label11.Text = "Statistics"
		'
		'Label12
		'
		Me.Label12.BackColor = System.Drawing.SystemColors.ActiveCaptionText
		Me.Label12.Location = New System.Drawing.Point(733, 300)
		Me.Label12.Name = "Label12"
		Me.Label12.Size = New System.Drawing.Size(224, 2)
		Me.Label12.TabIndex = 23
		'
		'Label14
		'
		Me.Label14.BackColor = System.Drawing.SystemColors.ActiveCaptionText
		Me.Label14.Location = New System.Drawing.Point(733, 484)
		Me.Label14.Name = "Label14"
		Me.Label14.Size = New System.Drawing.Size(224, 2)
		Me.Label14.TabIndex = 25
		'
		'cboTestSelection
		'
		Me.cboTestSelection.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cboTestSelection.FormattingEnabled = True
		Me.cboTestSelection.Items.AddRange(New Object() {"--Select Function--", "Dynamic Calibrator Tool (13905-803) Calibration", "Slip-On TCT (13906) Verification"})
		Me.cboTestSelection.Location = New System.Drawing.Point(149, 68)
		Me.cboTestSelection.Name = "cboTestSelection"
		Me.cboTestSelection.Size = New System.Drawing.Size(317, 24)
		Me.cboTestSelection.TabIndex = 26
		'
		'Label13
		'
		Me.Label13.AutoSize = True
		Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label13.Location = New System.Drawing.Point(148, 49)
		Me.Label13.Name = "Label13"
		Me.Label13.Size = New System.Drawing.Size(178, 16)
		Me.Label13.TabIndex = 27
		Me.Label13.Text = "Select Test to be Performed:"
		'
		'btnStartTest
		'
		Me.btnStartTest.Enabled = False
		Me.btnStartTest.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.btnStartTest.Location = New System.Drawing.Point(572, 52)
		Me.btnStartTest.Name = "btnStartTest"
		Me.btnStartTest.Size = New System.Drawing.Size(111, 40)
		Me.btnStartTest.TabIndex = 28
		Me.btnStartTest.Text = "Start"
		Me.btnStartTest.UseVisualStyleBackColor = True
		'
		'cboCommPort
		'
		Me.cboCommPort.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cboCommPort.FormattingEnabled = True
		Me.cboCommPort.Items.AddRange(New Object() {"COM1", "COM2", "COM3", "COM4"})
		Me.cboCommPort.Location = New System.Drawing.Point(472, 68)
		Me.cboCommPort.Name = "cboCommPort"
		Me.cboCommPort.Size = New System.Drawing.Size(93, 24)
		Me.cboCommPort.TabIndex = 29
		'
		'Label15
		'
		Me.Label15.AutoSize = True
		Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label15.Location = New System.Drawing.Point(483, 49)
		Me.Label15.Name = "Label15"
		Me.Label15.Size = New System.Drawing.Size(65, 16)
		Me.Label15.TabIndex = 30
		Me.Label15.Text = "COM Port"
		'
		'Button1
		'
		Me.Button1.Location = New System.Drawing.Point(837, 12)
		Me.Button1.Name = "Button1"
		Me.Button1.Size = New System.Drawing.Size(163, 23)
		Me.Button1.TabIndex = 31
		Me.Button1.Text = "Test Only - Remove TCT from DB"
		Me.Button1.UseVisualStyleBackColor = True
		'
		'TextBox1
		'
		Me.TextBox1.Location = New System.Drawing.Point(835, 42)
		Me.TextBox1.Name = "TextBox1"
		Me.TextBox1.Size = New System.Drawing.Size(100, 20)
		Me.TextBox1.TabIndex = 32
		Me.TextBox1.Text = "7777"
		'
		'Form1
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(1000, 762)
		Me.Controls.Add(Me.TextBox1)
		Me.Controls.Add(Me.Button1)
		Me.Controls.Add(Me.Label15)
		Me.Controls.Add(Me.cboCommPort)
		Me.Controls.Add(Me.btnStartTest)
		Me.Controls.Add(Me.Label13)
		Me.Controls.Add(Me.cboTestSelection)
		Me.Controls.Add(Me.Label14)
		Me.Controls.Add(Me.Label12)
		Me.Controls.Add(Me.Label11)
		Me.Controls.Add(Me.txtDruckStat)
		Me.Controls.Add(Me.txtPatApp)
		Me.Controls.Add(Me.txtTCTTrigStat)
		Me.Controls.Add(Me.txtTCTTrig)
		Me.Controls.Add(Me.txtTCTStat)
		Me.Controls.Add(Me.txtTCTPatApp)
		Me.Controls.Add(Me.Label10)
		Me.Controls.Add(Me.Label9)
		Me.Controls.Add(Me.Label8)
		Me.Controls.Add(Me.Label7)
		Me.Controls.Add(Me.Label6)
		Me.Controls.Add(Me.btnQuit)
		Me.Controls.Add(Me.Label5)
		Me.Controls.Add(Me.Label4)
		Me.Controls.Add(Me.txtStatus)
		Me.Controls.Add(Me.txtTCTNumber)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Chart1)
		Me.Name = "Form1"
		Me.Text = "TCT Fixture and Dynamic Verification Tool Calibration/Verification Program"
		CType(Me.Chart1, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

	Friend WithEvents Chart1 As DataVisualization.Charting.Chart
	Friend WithEvents Label1 As Label
	Friend WithEvents Label2 As Label
	Friend WithEvents Label3 As Label
	Friend WithEvents txtTCTNumber As TextBox
	Friend WithEvents txtStatus As TextBox
	Friend WithEvents Label4 As Label
	Friend WithEvents Label5 As Label
	Friend WithEvents btnQuit As Button
	Friend WithEvents Label6 As Label
	Friend WithEvents Label7 As Label
	Friend WithEvents Label8 As Label
	Friend WithEvents Label9 As Label
	Friend WithEvents Label10 As Label
	Friend WithEvents txtTCTPatApp As TextBox
	Friend WithEvents txtTCTStat As TextBox
	Friend WithEvents txtTCTTrig As TextBox
	Friend WithEvents txtTCTTrigStat As TextBox
	Friend WithEvents txtPatApp As TextBox
	Friend WithEvents txtDruckStat As TextBox
	Friend WithEvents Label11 As Label
	Friend WithEvents Label12 As Label
	Friend WithEvents Label14 As Label

	Public Sub New()

		' This call is required by the designer.
		InitializeComponent()

		' Add any initialization after the InitializeComponent() call.
		cboTestSelection.SelectedIndex = 2
		cboCommPort.SelectedIndex = 0

		ConfigModule.ReadConfigFile()

		InitLabelReferences()

	End Sub

	Friend WithEvents cboTestSelection As ComboBox
	Friend WithEvents Label13 As Label
	Friend WithEvents btnStartTest As Button
	Friend WithEvents cboCommPort As ComboBox
	Friend WithEvents Label15 As Label
	Friend WithEvents Button1 As Button
	Friend WithEvents TextBox1 As TextBox
End Class
