﻿Module ConfigModule

	Private m_FileNamePath As String = ""
	Private m_ConfigComm As String = ""
	Private m_ConfigOperator As String = ""
	Private m_ConfigDynCal As String = ""

	Private Const COMM_CONST As String = "CommPort="
	Private Const OPER_CONST As String = "Operator="
	Private Const DYNCAL_CONST As String = "DynCalNum="

	Public Function GetCommPort() As String
		GetCommPort = m_ConfigComm
	End Function

	Public Function GetOperator() As String
		GetOperator = m_ConfigOperator
	End Function

	Public Function GetDynCal() As String
		GetDynCal = m_ConfigDynCal
	End Function

	Public Sub SetCommPort(ByVal sValue As String)
		m_ConfigComm = sValue
	End Sub

	Public Sub SetOperator(ByVal sValue As String)
		m_ConfigOperator = sValue
	End Sub

	Public Sub SetDynCal(ByVal sValue As String)
		m_ConfigDynCal = sValue
	End Sub

	Public Sub ReadConfigFile()
		' Config file should be with executable
		Dim sPath As String = Application.ExecutablePath
		Dim iIdx As Integer = 0
		Dim iCrIdx As Integer = 0
		Dim sTemp As String = ""

		iIdx = InStrRev(sPath, "\")
		If iIdx > 0 Then
			sPath = Left(sPath, iIdx)
		End If
		m_FileNamePath = sPath & "config.txt"

		If Not My.Computer.FileSystem.FileExists(m_FileNamePath) Then
			Exit Sub
		End If

		Try
			Dim sFileContents As String = My.Computer.FileSystem.ReadAllText(m_FileNamePath)

			iIdx = InStr(sFileContents, COMM_CONST)
			If iIdx > 0 Then
				sTemp = Mid(sFileContents, Len(COMM_CONST) + 3 + iIdx)
				iCrIdx = InStr(sTemp, vbCrLf)
				If iCrIdx > 1 Then
					sTemp = Left(sTemp, iCrIdx - 1)
					sTemp.Trim()
					m_ConfigComm = sTemp
				End If

			End If

			iIdx = InStr(sFileContents, OPER_CONST)
			If iIdx > 0 Then
				sTemp = Mid(sFileContents, Len(OPER_CONST) + iIdx)
				iCrIdx = InStr(sTemp, vbCrLf)
				If iCrIdx > 1 Then
					sTemp = Left(sTemp, iCrIdx - 1)
					sTemp.Trim()
					m_ConfigOperator = sTemp
				End If

			End If

			iIdx = InStr(sFileContents, DYNCAL_CONST)
			If iIdx > 0 Then
				sTemp = Mid(sFileContents, Len(DYNCAL_CONST) + iIdx)
				iCrIdx = InStr(sTemp, vbCrLf)
				If iCrIdx > 1 Then
					sTemp = Left(sTemp, iCrIdx - 1)
					sTemp.Trim()
					m_ConfigDynCal = sTemp
				End If

			End If

		Catch ex As Exception
			MsgBox("Unable to read configuration file. See Engineer. System Msg: " & ex.Message)
			Exit Sub
		End Try

	End Sub

	Public Sub WriteConfigFile()
		Dim fsFileWriter As IO.StreamWriter

		Try
			' Delete then recreate file with latest user-entered parameters
			If My.Computer.FileSystem.FileExists(m_FileNamePath) Then
				My.Computer.FileSystem.DeleteFile(m_FileNamePath)
			End If

			System.IO.File.Create(m_FileNamePath).Dispose()

			fsFileWriter = My.Computer.FileSystem.OpenTextFileWriter(m_FileNamePath, False)
			fsFileWriter.WriteLine(COMM_CONST & m_ConfigComm)
			fsFileWriter.WriteLine(OPER_CONST & m_ConfigOperator)
			fsFileWriter.WriteLine(DYNCAL_CONST & m_ConfigDynCal)

			fsFileWriter.Close()

		Catch ex As Exception
			MsgBox("Unable to create config file. See Engineer. System Msg: " & ex.Message)
			Exit Sub
		End Try

	End Sub

End Module
