﻿Imports System.Data.SqlTypes
Imports System.Data.SqlClient
Imports MySql.Data.MySqlClient

Module OrigDatabaseModule

	Private m_MySqlConnection As New MySqlConnection

	Public Function OpenMySqlDatabase(ByRef sErrorMsg As String) As Boolean

		If m_MySqlConnection.State = ConnectionState.Open Then
			OpenMySqlDatabase = True
			Exit Function
		End If

		Dim bSuccess As Boolean = False
		Dim DatabaseName As String = "MfgEng"
		Dim server As String = "mfgsql" ' "ip address here"
		Dim userName As String = "mfgeng"
		Dim password As String = "mfgeng"

		m_MySqlConnection.ConnectionString = String.Format("server={0}; user id={1}; password={2}; database={3}; pooling=false; SslMode=none", server, userName, password, DatabaseName)
		Try
			If Not m_MySqlConnection Is Nothing Then
				m_MySqlConnection.Open()
				bSuccess = True
			End If
		Catch ex As Exception
			bSuccess = False
			sErrorMsg = ex.Message
		End Try

		OpenMySqlDatabase = bSuccess
	End Function

	Public Sub CloseDatabase()
		If Not m_MySqlConnection Is Nothing Then
			m_MySqlConnection.Close()
		End If
	End Sub
	'**************************************************************************
	'  Function:       Read_SQL_DB(Table,Field)
	'
	'  Purpose:        This will  open Table and read the data out of the Field. This function will
	'                  return the data from the database if the Table and field are found
	'
	'
	'  Resources:      mySQL Database maintained by MIS group
	'
	'  Interactions:
	'
	'  Inputs:        Table and Field name
	'
	'  Outputs:        Function Returns :
	'                     1) "no record exists"
	'                  or 2) actual data from database
	'                  or 2) "error opening DB"
	'                  or 3) "error finding record"

	'
	'
	'  Dependencies:   none.
	'  Err Conditions: none.
	'
	'  Notes:
	'
	'  History:        12/10/04 - T.Hoepfinger: Initial version
	'***************************************************************************/
	Public Function Read_SQL_DB(sSerialNumber As String, ByRef strct_DBData As TCT_Trigger_Voltages_Struct, ByRef sStatus As String) As Boolean
		Dim sSelectString As String
		Dim cMySqlCommand As MySqlCommand
		Dim rMySqlReader As MySqlDataReader
		Dim bAllDataPresent As Boolean = True

		If m_MySqlConnection.State = ConnectionState.Open Then

			'create string of the sql command that will be executed
			sSelectString = "SELECT * FROM TCT_Trigger_Voltages WHERE TCT_Number = " & sSerialNumber

			'Create an MySqlCommand object.
			cMySqlCommand = New MySqlCommand(sSelectString, m_MySqlConnection)

			'Send the CommandText to the connection, and then build a MySqlDataReader.
			'Note: The MySqlDataReader is forward-only.
			rMySqlReader = cMySqlCommand.ExecuteReader()

			If rMySqlReader.Read() Then

				'now get the Motorola Xducer data
				If rMySqlReader("Motorola_A_Voltage") <> "" Then
					strct_DBData.V_TCT_at_Druck_Trig(EYESETTING.EYESET_LOW_A) = CSng(rMySqlReader("Motorola_A_Voltage").ToString())
				Else
					bAllDataPresent = False
				End If

				If rMySqlReader("Motorola_B_Voltage") <> "" Then
					strct_DBData.V_TCT_at_Druck_Trig(EYESETTING.EYESET_MID_B) = CSng(rMySqlReader("Motorola_B_Voltage").ToString())
				Else
					bAllDataPresent = False
				End If

				If rMySqlReader("Motorola_C_Voltage") <> "" Then
					strct_DBData.V_TCT_at_Druck_Trig(EYESETTING.EYESET_HI_C) = CSng(rMySqlReader("Motorola_C_Voltage").ToString())
				Else
					bAllDataPresent = False
				End If

				'get the Druck data first
				If rMySqlReader("Druck_A_Voltage") <> "" Then
					strct_DBData.Druck_Trig(EYESETTING.EYESET_LOW_A) = CSng(rMySqlReader("Druck_A_Voltage").ToString())
				Else
					bAllDataPresent = False
				End If

				If rMySqlReader("Druck_B_Voltage") <> "" Then
					strct_DBData.Druck_Trig(EYESETTING.EYESET_MID_B) = CSng(rMySqlReader("Druck_B_Voltage").ToString())
				Else
					bAllDataPresent = False
				End If

				If rMySqlReader("Druck_C_Voltage") <> "" Then
					strct_DBData.Druck_Trig(EYESETTING.EYESET_HI_C) = CSng(rMySqlReader("Druck_C_Voltage").ToString())
				Else
					bAllDataPresent = False
				End If

				'now get IOP data
				If rMySqlReader("IOPg_A") <> "" Then
					strct_DBData.IOP_TCT(EYESETTING.EYESET_LOW_A) = CSng(rMySqlReader("IOPg_A").ToString())
				Else
					bAllDataPresent = False
				End If

				If rMySqlReader("IOPg_B") <> "" Then
					strct_DBData.IOP_TCT(EYESETTING.EYESET_MID_B) = CSng(rMySqlReader("IOPg_B").ToString())
				Else
					bAllDataPresent = False
				End If

				If rMySqlReader("IOPg_C") <> "" Then
					strct_DBData.IOP_TCT(EYESETTING.EYESET_HI_C) = CSng(rMySqlReader("IOPg_C").ToString())
				Else
					bAllDataPresent = False
				End If

				'now get correlation fit dat
				If rMySqlReader("Slope") <> "" Then
					strct_DBData.TCT_Slope = CSng(rMySqlReader("Slope").ToString())
				Else
					bAllDataPresent = False
				End If

				If rMySqlReader("Offset") <> "" Then
					strct_DBData.TCT_Offset = CSng(rMySqlReader("Offset").ToString())
				Else
					bAllDataPresent = False
				End If

				If rMySqlReader("R2") <> "" Then
					strct_DBData.TCT_R2 = CSng(rMySqlReader("R2").ToString())
				Else
					bAllDataPresent = False
				End If

				strct_DBData.bAllDataFilled = bAllDataPresent

				Read_SQL_DB = True
			Else
				' SN Not found in DB
				sStatus = "Serial Number not found in DB"
				rMySqlReader.Close()
				cMySqlCommand.Dispose()
				Read_SQL_DB = False
				Exit Function
			End If

			rMySqlReader.Close()
			cMySqlCommand.Dispose()
		Else
			sStatus = "DataBase Open Error"
			Read_SQL_DB = False
			Exit Function
		End If  'Connection open 

		Read_SQL_DB = True

	End Function

	Public Function Create_New_DB_Entry(ByVal sSerialNumber As String) As Boolean
		Dim sCommandString As String = "INSERT INTO TCT_Trigger_Voltages (TCT_Number) VALUES ('" & sSerialNumber & "')"
		Dim cCommand As MySqlCommand
		Try
			'Create an OleDbCommand object.
			cCommand = New MySqlCommand(sCommandString, m_MySqlConnection)
			cCommand.ExecuteNonQuery()
		Catch ex As Exception
			MsgBox("Create_New_DB_Entry() failure: " & ex.Message)
			cCommand.Dispose()
			Create_New_DB_Entry = False
			Exit Function
		End Try
		cCommand.Dispose()
		Create_New_DB_Entry = True
	End Function

	Public Function Remove_DB_Entry(ByVal sSerialNumber As String) As Boolean
		Dim sCommandString As String = "DELETE FROM TCT_Trigger_Voltages WHERE TCT_Number=" & sSerialNumber
		Dim cCommand As MySqlCommand
		Try
			'Create an OleDbCommand object.
			cCommand = New MySqlCommand(sCommandString, m_MySqlConnection)
			cCommand.ExecuteNonQuery()
		Catch ex As Exception
			MsgBox("Create_New_DB_Entry() failure: " & ex.Message)
			cCommand.Dispose()
			Remove_DB_Entry = False
			Exit Function
		End Try
		cCommand.Dispose()
		Remove_DB_Entry = True
	End Function

	'**************************************************************************
	'  Function:       Write_SQL_DB()
	'
	'  Purpose:        This will write the Data passed to this function into the "Table"
	'                  and "Field" also passed to this function.
	'
	'
	'  Resources:      mySQL Database maintained by MIS group
	'
	'  Interactions:
	'
	'  Inputs:         Table, Field, Data
	'
	'  Outputs:        Function Returns :
	'                    ---->  "new record"  or "update record" or "all tests done" or "Error"
	'                  To Updates to mySQL--> MfgEng database if allowed
	'
	'  Dependencies:
	'  Err Conditions: none.
	'
	'  Notes:
	'
	'  History:        12/10/04 - T.Hoepfinger: Initial version
	'***************************************************************************/

	Public Function Write_SQL_DB(ByVal sSerialNumber As String, Field As String, Value As String) As Boolean

		Dim sCommandString As String
		Dim cCommand As MySqlCommand

		sCommandString = "UPDATE TCT_Trigger_Voltages Set " & Field & " = '" & Value & "' WHERE TCT_Number = '" & sSerialNumber & "';"
		Try
			'Create an OleDbCommand object.
			cCommand = New MySqlCommand(sCommandString, m_MySqlConnection)
			cCommand.ExecuteNonQuery()
		Catch ex As Exception
			cCommand.Dispose()
			Write_SQL_DB = False
			Exit Function
		End Try

		cCommand.Dispose()
		Write_SQL_DB = True
	End Function
End Module
