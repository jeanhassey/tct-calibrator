﻿Imports TCT_DynamicFixVerification.OutFileModule

Imports System.Windows.Forms.DataVisualization.Charting
Module TestFunctionModule

	'This structure is to be used when using an oscilloscope
	'to take measurements. Set these values before calling the scope
	'function(scope_io)
	Public Structure measscope
		Dim waveform As Boolean         'SET TO TRUE FOR WAVEFORM DUMPS
		Dim acq_mode As String          'SAMPLE,AVERAGE
		Dim channel As String           '1 or 2
		Dim coupling As String          'AC,DC,GND
		Dim vert As String              'i.e.: 1.0E-1
		Dim Offset As String
		Dim vertposition As String      'i.e.: 0.0e0
		Dim horiz As String             'i.e.: 50E-3
		Dim tr_source As String         '1 or 2
		Dim tr_edge As String           'FALL or RISE
		Dim tr_level As String          'i.e.: 2E-0
		Dim measure As String     'AMPlitude(high-low)
		'for others:see tek.prog.manual
	End Structure

	Public Structure TCT_Trigger_Voltages_Struct

		Dim V_TCT_at_Druck_Trig() As Single
		Dim Druck_Trig() As Single
		Dim IOP_TCT() As Single

		Dim TCT_Slope As Single
		Dim TCT_Offset As Single
		Dim TCT_R2 As Single

		Dim bAllDataFilled As Boolean

	End Structure

	Public Structure TCT_Setting
		Dim sSerialNumber As String

		Dim V_TCT() As Single '(5)
		Dim V_Druck() As Single
		Dim Press_Druck() As Single
		Dim V_Corr_Druck() As Single 'stores druck voltages during correlation
	End Structure

	Public Structure Averages
		Dim AvgTCT As Single
		Dim AvgDruck As Single
		Dim AvgPress As Single
	End Structure

	Public Structure SettingMaxMin
		Dim C_SETTING_MAX, C_SETTING_MIN As Single
		Dim B_SETTING_MAX, B_SETTING_MIN As Single
		Dim A_SETTING_MAX, A_SETTING_MIN As Single
	End Structure

	Public Structure DruckMaxMin
		Dim C_DRUCK_MAX, C_DRUCK_MIN As Single
		Dim B_DRUCK_MAX, B_DRUCK_MIN As Single
		Dim A_DRUCK_MAX, A_DRUCK_MIN As Single
	End Structure

	Public Enum CalType
		CAL_TYPE_UNDEF = 0
		CAL_TYPE_DYN = 1
		CAL_TYPE_TCT = 2
	End Enum

	Public Enum Stages
		STAGE_CORRELATE = 0
		STAGE_VERIFY = 1
	End Enum

	Public Enum EYESETTING
		EYESET_LOW_A = 0
		EYESET_MID_B = 1
		EYESET_HI_C = 2
		EYESET_MAX = 3
	End Enum

	Public Enum SCOPE_CHANNEL
		SCOPE_CHANNEL_1 = 1
		SCOPE_CHANNEL_2 = 2
	End Enum

	Public Enum FUNCTION_RETURN
		FUNCTION_RETURN_ERROR = 0
		FUNCTION_RETURN_SUCCESS = 1
		FUNCTION_RETURN_RESTART = 2
	End Enum

	Public Enum CABLE_SETUP
		CABLE_SETUP_NONE = 0
		CABLE_SETUP_DOUBLE_SIDE = 1
		CABLE_SETUP_FIVE_DROPS = 2
		CABLE_SETUP_DYN_CAL = 3
	End Enum

	Public Const PMAX As Single = 3.9              ' Pressure Max (IOP)
	Public Const PMIN As Single = 2.9              ' Pressure Min
	Public Const RRMAX As Single = 0.23            ' Ramp rate Max (PSI/milliseconds)
	Public Const RRMIN As Single = 0.15            ' Ramp rate Min

	'Ramp Rate Testing constants:
	Public Const TCT_RAMPLOW_PSI As Long = 39329    ' 4.0 volts TCT Psensor - 12.5 IOP  ' Start voltage of ramp rate
	Public Const TCT_RAMPHIGH_PSI As Long = 51117   ' 5.8 volts TCT Psensor - 44.7 IOP  ' Stop voltage of ramp rate
	Public Const TCT_CNTS_PER_MS As Integer = 25     ' counts per ms (only if time scale is set-up as 2ms)
	Public Const TCT_VOLTS_PER_STEP As Single = (10 / 65535)  ' ONLY FOR 1VOLTS/DIVISION

	Public Const MAX_NUM_SHOTS As Integer = 5
	Public Const DATA_POINTS As Integer = 500
	Public Const DATA_POINTS_MIDPT As Integer = DATA_POINTS / 2

	Private m_TCTSerialNum As String = "-1"

	Private m_DB_TCT_Trig_Volt As TCT_Trigger_Voltages_Struct
	Private m_DB_Fixed_TCT_Trig_Volt As TCT_Trigger_Voltages_Struct

	Private m_dDruckPressureBaseline, m_dTCTPressureBaseline As Double
	Private m_Averages(EYESETTING.EYESET_MAX - 1) As Averages

	Private m_Waveform1(DATA_POINTS - 1) As Long  'low-res waveform data points from scope
	Private m_Waveform2(DATA_POINTS - 1) As Long
	Private m_TCT_RampRate(EYESETTING.EYESET_MAX - 1, MAX_NUM_SHOTS) As Single

	Private m_V_Corr_TCT(EYESETTING.EYESET_MAX - 1, MAX_NUM_SHOTS) As Single 'stores tct voltages during correlation

	Private m_TCT_Setting(EYESETTING.EYESET_MAX - 1) As TCT_Setting

	Private m_iDynCal_Num As Integer
	Private m_OperatorName As String = ""

	Private scope As measscope
	Private m_CableSetup As CABLE_SETUP = CABLE_SETUP.CABLE_SETUP_NONE

	Private m_XAxisLabel As Label
	Private m_YAxisLabel As Label
	Private m_Label2 As Label
	Private m_Label3 As Label
	Private m_Label8 As Label
	Private m_Status As TextBox
	Private m_TCTPatApp As TextBox
	Private m_TCTStat As TextBox
	Private m_TCTTrig As TextBox
	Private m_TCTTrigStat As TextBox
	Private m_TCTSN As TextBox
	Private m_PatApp As TextBox
	Private m_DruckStat As TextBox
	Private m_msgResult As MsgBoxResult
	Private m_CalType As CalType
	Private m_ioSerialPort As SerialPortClass

	Public Function GetUserInputForInitTest() As FUNCTION_RETURN

		Dim eStatus As FUNCTION_RETURN = FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS

		' Clear out all structures that hold data
		InitTestParameters()

		' Initialize the oscilloscope
		eStatus = InitScope()
		If eStatus <> FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS Then
			Form1.Refresh()
			GetUserInputForInitTest = eStatus
			Exit Function
		End If

		' Reset the oscilloscope
		eStatus = ResetOScope()
		If eStatus <> FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS Then
			Form1.Refresh()
			GetUserInputForInitTest = eStatus
			Exit Function
		End If

		' Testing with the TCT -------------------------------------------------
		If m_CalType = CalType.CAL_TYPE_TCT Then
			eStatus = SetupScopeForTCT()
			If eStatus <> FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS Then
				Form1.Refresh()
				GetUserInputForInitTest = eStatus
				Exit Function
			End If

			' Get the TCT serial number
			Dim sSerialNum As String = ""
			eStatus = GetTCTSerialNumber(sSerialNum)
			If eStatus = FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS Then
				m_TCTSN.Text = sSerialNum
				Form1.Refresh()
			Else
				Form1.Refresh()
				GetUserInputForInitTest = eStatus
				Exit Function
			End If

			' Find the TCT in the DB. If it's not there, create a new entry.
			eStatus = FindTCTInDB()
			If eStatus <> FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS Then
				Form1.Refresh()
				GetUserInputForInitTest = eStatus
				Exit Function
			End If

			' Testing Dynamic Calibrator ----------------------------------------
		ElseIf m_CalType = CalType.CAL_TYPE_DYN Then
			eStatus = SetupScopeForDynCal()

			If eStatus <> FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS Then
				Form1.Refresh()
				GetUserInputForInitTest = eStatus
				Exit Function
			End If
		Else '-------------------------------------------------------------------
			MsgBox("Please select a test.", vbOK)
			GetUserInputForInitTest = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			Exit Function
		End If

		' Get Dynamic Cal serial number and operator name for both tests
		eStatus = GetDynCalSN()
		If eStatus <> FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS Then
			Form1.Refresh()
			GetUserInputForInitTest = eStatus
			Exit Function
		End If

		GetOperatorName()

		' For TCT Testing only----------------------------------------------------
		If m_CalType = CalType.CAL_TYPE_TCT Then
			'verify that the Trigger Output double sided-pulse can be checked
			If Not Double_Side() Then
				GetUserInputForInitTest = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
				Exit Function
			End If

			' Double-side test passed, prepare scope for next phase of testing
			If Not ReArmScopeForTCTSecondPhase() Then
				GetUserInputForInitTest = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
				Exit Function
			End If

			' Make sure all data needed for the test is in the DB and get it if it's not.
			eStatus = VerifyDataInDB()
			If eStatus <> FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS Then
				Form1.Refresh()
				GetUserInputForInitTest = eStatus
				Exit Function
			End If
		End If '-----------------------------------------------------------------

		GetUserInputForInitTest = FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS
	End Function

	'**************************************************************************
	'  Function:       InitScope() As Boolean
	'
	'
	'  Purpose:        Open the comm port, give the scope a reset
	'                  and clear the scope structure.
	'
	'
	'
	'  Resources:
	'
	'  Interactions:
	'
	'  Inputs:
	'
	'  Outputs:        initializes the scope structure values
	'
	'  Dependencies:   none.
	'  Err Conditions: none.
	'
	'  Notes:
	'
	'  History:        6/10/98 - T.Hoepfinger: Initial version
	'                  9/20/99 - T.Hoepfinger: Added getting the scope type
	'                              being used for testing. Mainly required
	'                              form frmvvlscope when downloading a waveform
	'                              from an O'scope(waveform_dump).Aslo turn the
	'                              HEADER OFF for all scopes
	'
	'***************************************************************************/
	Private Function InitScope() As FUNCTION_RETURN
		Dim Readstring As String = ""
		Dim scopetype As String = ""
		Dim bInitAttempt As Boolean = True

		m_Status.Text = "Determining O-scope type"
		Form1.Refresh()

		Do
			'Get scope type
			m_ioSerialPort.ClearInputBuffer()

			' send request
			If Not m_ioSerialPort.Send("ID?" & vbCr) Then
				m_Status.Text = "Error sending ID? command to the scope"
				Form1.Refresh()
			End If

			TimeDelay(200)

			'get the scope data
			If Not m_ioSerialPort.Receive(Readstring) Then
				m_Status.Text = "No data received from serial port."
				InitScope = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
				Exit Function
			End If

			'Read scope type from input
			scopetype = Mid(Readstring, 4, 12)

			If scopetype = "TEK/TDS 3032" Then
				m_Status.Text = "The correct scope is ON-Line"
				Form1.Refresh()
				bInitAttempt = False
			Else
				'fault, no match on any scope
				m_msgResult = MsgBox("Fault getting the correct Oscilloscope type on selected COM port" & vbCr &
				"Check the scope and comm. cable" & vbCr &
				"Do you want to retry now?", vbYesNo, "Oscilloscpoe Communication Fault!")

				If m_msgResult = MsgBoxResult.No Then ' no
					bInitAttempt = False
					InitScope = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
					Exit Function
				End If
			End If
		Loop Until (bInitAttempt = False)
		InitScope = FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS
		'End Get scope type
	End Function

	Private Function ResetOScope() As FUNCTION_RETURN

		m_Status.Text = "Now resetting the O-scope"
		Form1.Refresh()

		'first,reset the scope to a known factory reset condition
		If Not m_ioSerialPort.Send("*RST" & vbCr) Then
			m_Status.Text = "ResetOScope, Command failed: *RST"
			ResetOScope = False
			Exit Function
		End If

		TimeDelay(1000)

		'turn header off
		If Not m_ioSerialPort.Send("HEADER OFF" & vbCr) Then
			m_Status.Text = "ResetOScope, Command failed: HEADER OFF"
			ResetOScope = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			Exit Function
		End If

		TimeDelay(200)
		ResetOScope = FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS
	End Function
	Public Sub SetTestParameters(ByVal eCalType As CalType, ByVal ioSerialPort As SerialPortClass, ByRef m_refXAxisLabel As Label,
								 ByRef m_refYAxisLabel As Label, ByRef m_refLabel8 As Label, ByRef m_refStatus As TextBox, ByRef m_refSerialNum As TextBox, ByRef m_refTCTPatApp As TextBox,
								 ByRef m_refTCTStat As TextBox, ByRef m_refTCTTrig As TextBox, ByRef m_refTCTTrigStat As TextBox,
								 ByRef m_refPatApp As TextBox, ByRef m_refDruckStat As TextBox,
								 ByRef m_refChart As DataVisualization.Charting.Chart)
		m_CalType = eCalType
		m_ioSerialPort = ioSerialPort
		m_XAxisLabel = m_refXAxisLabel
		m_YAxisLabel = m_refYAxisLabel
		m_Label8 = m_refLabel8
		m_Status = m_refStatus
		m_TCTSN = m_refSerialNum
		m_TCTPatApp = m_refTCTPatApp
		m_TCTStat = m_refTCTStat
		m_TCTTrig = m_refTCTTrig
		m_TCTTrigStat = m_refTCTTrigStat
		m_PatApp = m_refPatApp
		m_DruckStat = m_refDruckStat

		ChartModule.SetChartReference(m_refChart)
		ChartModule.ClearCharts()
		ChartModule.InitializeChart()
	End Sub

	Private Sub InitTestParameters()

		For i = 0 To EYESETTING.EYESET_MAX - 1
			ReDim m_TCT_Setting(i).V_Corr_Druck(MAX_NUM_SHOTS)
			ReDim m_TCT_Setting(i).V_Druck(MAX_NUM_SHOTS)
			ReDim m_TCT_Setting(i).V_TCT(MAX_NUM_SHOTS)
			ReDim m_TCT_Setting(i).Press_Druck(MAX_NUM_SHOTS)

			m_Averages(i).AvgDruck = 0
			m_Averages(i).AvgTCT = 0
			m_Averages(i).AvgPress = 0

			For j = 0 To MAX_NUM_SHOTS
				m_V_Corr_TCT(i, j) = 0
			Next
		Next

		ReDim m_DB_Fixed_TCT_Trig_Volt.V_TCT_at_Druck_Trig(EYESETTING.EYESET_MAX - 1)
		ReDim m_DB_Fixed_TCT_Trig_Volt.Druck_Trig(EYESETTING.EYESET_MAX - 1)
		ReDim m_DB_Fixed_TCT_Trig_Volt.IOP_TCT(EYESETTING.EYESET_MAX - 1)

		ReDim m_DB_TCT_Trig_Volt.V_TCT_at_Druck_Trig(EYESETTING.EYESET_MAX - 1)
		ReDim m_DB_TCT_Trig_Volt.Druck_Trig(EYESETTING.EYESET_MAX - 1)
		ReDim m_DB_TCT_Trig_Volt.IOP_TCT(EYESETTING.EYESET_MAX - 1)
		m_DB_TCT_Trig_Volt.bAllDataFilled = False

		m_TCTSerialNum = "-1"
		m_OperatorName = ""

		m_dDruckPressureBaseline = 0
		m_dTCTPressureBaseline = 0
		m_iDynCal_Num = 0

		For i = 0 To DATA_POINTS - 1
			m_Waveform1(i) = 0
			m_Waveform2(i) = 0
		Next

		scope.acq_mode = ""
		scope.channel = ""
		scope.coupling = ""
		scope.horiz = ""
		scope.measure = ""
		scope.tr_edge = ""
		scope.tr_level = ""
		scope.tr_source = ""
		scope.vert = ""
		scope.vertposition = ""
		scope.waveform = True

		For i = 0 To EYESETTING.EYESET_MAX - 1
			For j = 0 To MAX_NUM_SHOTS
				m_TCT_RampRate(i, j) = 0
			Next
		Next

	End Sub

	Private Function SetupScopeForTCT() As FUNCTION_RETURN
		m_Status.Text = "Setting up CH1 on the O-scope"
		Form1.Refresh()

		Dim Response As String = ""
		Dim bSuccess As Boolean = True

		scope.waveform = True
		scope.coupling = "DC"
		scope.channel = "1"
		scope.vert = "1E0"
		scope.horiz = "2E-3"
		scope.Offset = "-3"

		m_XAxisLabel.Text = "2.0 ms/Div"
		m_YAxisLabel.Text = "1.0 volts/Div"
		Form1.Refresh()

		'NOW SEND THE PARAMETERS
		bSuccess = scope_io(False)
		If Not bSuccess Then
			MsgBox("Fault setting up Scope", vbOKOnly, "SCOPE FAULT")
			SetupScopeForTCT = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			Exit Function
		End If

		'----------------------------------------------------------------------------------------
		' setup Ch 2 and parameters on the scope
		'----------------------------------------------------------------------------------------
		scope.tr_edge = "RISE"
		scope.tr_source = "EXT"
		scope.tr_level = "6E0"
		'horizontal-trigger-position to 50%(middle of wavefrom captured)
		If Not m_ioSerialPort.Send("HOR:TRIG:POS 50" & vbCr) Then
			m_Status.Text = "SendTriggerCommands, Command failed: HOR:TRIG:POS 50"
			SetupScopeForTCT = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			Exit Function
		End If
		TimeDelay(200)

		'Set Horizontal Resolution to 500 points
		If Not m_ioSerialPort.Send("HOR:RESOLUTION LOW" & vbCr) Then
			m_Status.Text = "SendTriggerCommands, Command failed: HOR:RESOLUTION LOW"
			SetupScopeForTCT = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			Exit Function
		End If
		TimeDelay(200)

		If SendTriggerCommands() <> FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS Then
			SetupScopeForTCT = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			Exit Function
		End If

		m_Status.Text = "Setting up CH2 on scope for TCT"
		Form1.Refresh()

		'*************************************************************************
		' Set Scope Ch 2 for TCT Cal
		'*************************************************************************
		scope.waveform = True
		scope.channel = "2"
		scope.coupling = "DC"
		scope.Offset = "-3"
		scope.vert = ".5E0"  ''9/6/07  was 2E0  ' Moved druck to this channel
		scope.acq_mode = "SAMPLE"

		'NOW SEND THE PARAMETERS
		If scope_io(False) = False Then
			MsgBox("Fault setting up Scope", vbOKOnly, "SCOPE FAULT")
			SetupScopeForTCT = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			Exit Function
		End If

		SetupScopeForTCT = FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS
	End Function

	Private Function SetupScopeForDynCal() As FUNCTION_RETURN
		m_Status.Text = "Setting up CH1 on the O-scope"
		Form1.Refresh()

		Dim Response As String = ""
		Dim bSuccess As Boolean = True

		scope.waveform = True
		scope.coupling = "DC"
		scope.channel = "2"
		scope.vert = ".5E0"                 ' If you change .vert or .horiz, you must
		scope.horiz = "4E-3"                ' also change cmddyncal_click (CNTS_PER_MS, VOLTS_PER_STEP
		scope.Offset = "-3"

		m_XAxisLabel.Text = "4 ms/Div"         ' RAMPLOW_PSI,RAMPHIGH_PSI
		m_YAxisLabel.Text = "0.5 volts/Div"
		Form1.Refresh()

		'NOW SEND THE PARAMETERS
		bSuccess = scope_io(False)
		If Not bSuccess Then
			MsgBox("Fault setting up Scope", vbOKOnly, "SCOPE FAULT")
			SetupScopeForDynCal = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			Exit Function
		End If

		'----------------------------------------------------------------------------------------
		' setup Ch 2 and parameters on the scope
		'----------------------------------------------------------------------------------------
		scope.tr_edge = "RISE"
		scope.tr_level = "2E0"
		scope.tr_source = "CH2"

		If Not m_ioSerialPort.Send("HOR:TRIG:POS 10" & vbCr) Then
			m_Status.Text = "SendTriggerCommands, Command failed: HOR:TRIG:POS 10"
			SetupScopeForDynCal = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			Exit Function
		End If
		TimeDelay(200)

		'Set Horizontal Resolution to 500 points
		If Not m_ioSerialPort.Send("HOR:RESOLUTION LOW" & vbCr) Then
			m_Status.Text = "SendTriggerCommands, Command failed: HOR:RESOLUTION LOW"
			SetupScopeForDynCal = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			Exit Function
		End If
		TimeDelay(200)

		If SendTriggerCommands() <> FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS Then
			SetupScopeForDynCal = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			Exit Function
		End If

		SetupScopeForDynCal = FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS
	End Function

	'Private Function SetupCH1OnScope() As FUNCTION_RETURN
	'	m_Status.Text = "Setting up CH1 on the O-scope"
	'	Form1.Refresh()

	'	Dim Response As String = ""
	'	Dim bSuccess As Boolean = True

	'	scope.waveform = True
	'	scope.coupling = "DC"

	'	If m_CalType = CalType.CAL_TYPE_TCT Then
	'		scope.channel = "1"
	'		scope.vert = "1E0"
	'		scope.horiz = "2E-3"
	'		m_XAxisLabel.Text = "2.0 ms/Div"
	'		m_YAxisLabel.Text = "1.0 volts/Div"
	'	ElseIf m_CalType = CalType.CAL_TYPE_DYN Then
	'		scope.channel = "2"
	'		scope.vert = ".5E0"                 ' If you change .vert or .horiz, you must
	'		scope.horiz = "4E-3"                ' also change cmddyncal_click (CNTS_PER_MS, VOLTS_PER_STEP
	'		m_XAxisLabel.Text = "4 ms/Div"         ' RAMPLOW_PSI,RAMPHIGH_PSI
	'		m_YAxisLabel.Text = "0.5 volts/Div"
	'	End If

	'	Form1.Refresh()
	'	scope.Offset = "-3"

	'	'NOW SEND THE PARAMETERS
	'	bSuccess = scope_io(False)
	'	If Not bSuccess Then
	'		MsgBox("Fault setting up Scope", vbOKOnly, "SCOPE FAULT")
	'		SetupCH1OnScope = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
	'		Exit Function
	'	End If

	'	SetupCH1OnScope = FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS
	'End Function

	'Private Function SetupCH2ParamsForScope() As FUNCTION_RETURN
	'	m_Status.Text = "Setting up CH1 on the O-scope"
	'	Form1.Refresh()

	'	Dim Response As String = ""
	'	Dim bSuccess As Boolean = True

	'	'----------------------------------------------------------------------------------------
	'	' setup Ch 2 and parameters on the scope
	'	'----------------------------------------------------------------------------------------
	'	If m_CalType = CalType.CAL_TYPE_TCT Then
	'		scope.tr_edge = "RISE"
	'		scope.tr_source = "EXT"
	'		scope.tr_level = "6E0"
	'		'horizontal-trigger-position to 50%(middle of wavefrom captured)
	'		If Not m_ioSerialPort.Send("HOR:TRIG:POS 50" & vbCr) Then
	'			m_Status.Text = "SendTriggerCommands, Command failed: HOR:TRIG:POS 50"
	'			SetupCH2ParamsForScope = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
	'			Exit Function
	'		End If

	'		TimeDelay(200)
	'	ElseIf m_CalType = CalType.CAL_TYPE_DYN Then
	'		scope.tr_edge = "RISE"
	'		scope.tr_level = "2E0"
	'		scope.tr_source = "CH2"

	'		If Not m_ioSerialPort.Send("HOR:TRIG:POS 10" & vbCr) Then
	'			m_Status.Text = "SendTriggerCommands, Command failed: HOR:TRIG:POS 10"
	'			SetupCH2ParamsForScope = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
	'			Exit Function
	'		End If
	'		TimeDelay(200)
	'	End If

	'	'Set Horizontal Resolution to 500 points
	'	If Not m_ioSerialPort.Send("HOR:RESOLUTION LOW" & vbCr) Then
	'		m_Status.Text = "SendTriggerCommands, Command failed: HOR:RESOLUTION LOW"
	'		SetupCH2ParamsForScope = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
	'		Exit Function
	'	End If
	'	TimeDelay(200)
	'	SetupCH2ParamsForScope = FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS
	'End Function

	Private Function SendTriggerCommands() As FUNCTION_RETURN
		m_Status.Text = "Sending trigger commands"
		Form1.Refresh()

		'************************************************************
		'                   TRIGGER MENU
		'************************************************************
		'trigger-main-edge-slope-rising edge
		If Not m_ioSerialPort.Send("TRIG:MAIN:EDGE:SLOPE " & scope.tr_edge & vbCr) Then
			m_Status.Text = "SendTriggerCommands, Command failed: TRIG:MAIN:EDGE:SLOPE"
			SendTriggerCommands = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			Exit Function
		End If
		TimeDelay(200)

		'set trigger source
		If Not m_ioSerialPort.Send("TRIG:MAIN:EDGE:SOURCE " & scope.tr_source & vbCr) Then
			m_Status.Text = "SendTriggerCommands, Command failed: TRIG:MAIN:EDGE:SOURCE"
			SendTriggerCommands = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			Exit Function
		End If
		TimeDelay(200)

		'set trigger COUPLING
		If Not m_ioSerialPort.Send("TRIG:MAIN:EDGE:COUP HFREJ" & vbCr) Then
			m_Status.Text = "SendTriggerCommands, Command failed: TRIG:MAIN:EDGE:COUP HFREJ"
			SendTriggerCommands = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			Exit Function
		End If
		TimeDelay(200)

		SendTriggerCommands = FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS
	End Function

	'Private Function SetupCh2OnScopeTCT() As FUNCTION_RETURN
	'	m_Status.Text = "Setting up CH2 on scope for TCT"
	'	Form1.Refresh()

	'	'*************************************************************************
	'	' Set Scope Ch 2 for TCT Cal
	'	'*************************************************************************
	'	If m_CalType = CalType.CAL_TYPE_TCT Then
	'		scope.waveform = True
	'		scope.channel = "2"
	'		scope.coupling = "DC"
	'		scope.Offset = "-3"
	'		scope.vert = ".5E0"  ''9/6/07  was 2E0  ' Moved druck to this channel
	'		scope.acq_mode = "SAMPLE"

	'		'NOW SEND THE PARAMETERS
	'		If scope_io(False) = False Then
	'			MsgBox("Fault setting up Scope", vbOKOnly, "SCOPE FAULT")
	'			SetupCh2OnScopeTCT = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
	'			Exit Function
	'		End If
	'	End If
	'	SetupCh2OnScopeTCT = FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS
	'End Function

	Private Function GetTCTSerialNumber(ByRef sSerialNumber As String) As FUNCTION_RETURN
		Dim Response As String = ""
		m_Status.Text = "Enter TCT number to check"
		Form1.Refresh()

		'Get TCT serial number
		Do
			Response = InputBox("Enter the TCT Serial number Now", "NEED TCT SERIAL NUMBER") '(Select either 2,4,5,6,10,11,12,16 or 20)")

			'let user know that TCT not valid
			If Response <> "" Then
				'reference everything off TCT2
				m_TCTSerialNum = Response
				sSerialNumber = m_TCTSerialNum
				Exit Do
			Else
				m_msgResult = MsgBox("Do you want to end the test?", vbYesNo, "END TESTING?")
				If m_msgResult = MsgBoxResult.Yes Then
					GetTCTSerialNumber = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
					m_Status.Text = "User ended test"
					Exit Function
				End If
			End If
		Loop

		GetTCTSerialNumber = FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS
	End Function

	Private Function FindTCTInDB() As FUNCTION_RETURN
		'*************************************************************************
		' Check to see if the TCT Number is in Database
		'*************************************************************************
		'Now read the SQL data table for the settings.
		'Check to see if it's in the DB
		'Read_SQL_DB (m_TCTSerialNum)

		Dim sStatus As String = ""
		If m_TCTSerialNum <> "-1" Then
			If Not Read_SQL_DB(m_TCTSerialNum, m_DB_TCT_Trig_Volt, sStatus) Then '= '"Error finding SQL database record" Then
				m_msgResult = MsgBox("Is This a New TCT?" & vbCr & vbCr & "The IOP/Voltage Curve has not been established for this TCT." & vbCrLf & vbCrLf & "Would you like to proceed?", vbYesNo, "ESTABLISH CURVE?")
				If m_msgResult = MsgBoxResult.Yes Then

					If Not OrigDatabaseModule.Create_New_DB_Entry(m_TCTSerialNum) Then
						FindTCTInDB = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
						Exit Function
					End If

					'add in all new data
					If Not New_TCT() Then
						FindTCTInDB = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
						Exit Function
					End If

					If Not First_Voltages() Then
						FindTCTInDB = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
						Exit Function
					End If
					'establish the cal curve using 3 points
					Call Corr_Fit(3)

					MsgBox("The IOP/Voltage Curve has been set." & vbCrLf & vbCrLf &
							   "Please press OK to continue to verification.", vbOKOnly, "CURVE ESTABLISHED")
				Else
					m_Status.Text = "Cannot find DB record for TCT number " & m_TCTSerialNum & "."
					FindTCTInDB = FUNCTION_RETURN.FUNCTION_RETURN_RESTART
					Exit Function
				End If
			Else
				'otherwise check to see if Xducer was changed
				If Not New_Transducer() Then
					FindTCTInDB = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
					Exit Function
				End If
			End If
		Else
			m_Status.Text = "In FindTCTInDB(), Serial number not defined."
			FindTCTInDB = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			Exit Function
		End If
		FindTCTInDB = FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS
	End Function

	Private Function GetDynCalSN() As FUNCTION_RETURN
		Dim Response As String = ""
		m_Status.Text = "Enter Dynamic number"
		Form1.Refresh()

		Do
			Response = InputBox("Enter the Dynamic Verification Tool Serial number Now", "NEED SERIAL NUMBER (select between 1 and 2)", ConfigModule.GetDynCal)
			If Response = "" Then
				m_msgResult = MsgBox("Do you want to end the test?", vbYesNo, "END TESTING?")
				If m_msgResult = MsgBoxResult.Yes Then
					m_Status.Text = "In GetDynCalSN(), user chose to end test."
					GetDynCalSN = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
					Exit Function
				End If
			ElseIf IsNumeric(Response) Then
				m_iDynCal_Num = CInt(Response)
				ConfigModule.SetDynCal(Response)
			End If
		Loop Until (m_iDynCal_Num = 1 Or m_iDynCal_Num = 2)

		GetDynCalSN = FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS
	End Function

	Private Function GetOperatorName() As String
		m_Status.Text = ""
		Form1.Refresh()
		m_OperatorName = InputBox("Please Enter Your Name (i.e John Smith)", "NEED OPERATOR'S NAME ", ConfigModule.GetOperator)
		ConfigModule.SetOperator(m_OperatorName)
		GetOperatorName = m_OperatorName
	End Function

	Private Sub SetupCablesForDS()
		m_Status.Text = "Connect BNC cables"
		Form1.Refresh()

		MsgBox("Connect a BNC cable from The TCT Box (Applanation Out Wire)->Trigger Output to Channel 1 of TDS3032", vbOKOnly, "Make Connection")
		MsgBox("Connect a BNC cable from The Dynamic Calibrator ->Druck to Channel 2 of TDS3032", vbOKOnly, "Make Connection")

		m_CableSetup = CABLE_SETUP.CABLE_SETUP_DOUBLE_SIDE
	End Sub

	Private Sub SetupCablesForFiveDrops()
		m_Status.Text = "Connect BNC cables"
		Form1.Refresh()

		' Prompt user where the connect the scope probes
		MsgBox("Connect a BNC cable from The TCT Box (TCT sensor) ->Pressure Ramp Out to Channel 1 of TDS3032", vbOKOnly, "Make Connection")
		MsgBox("Connect a BNC cable from The TCT Box (Applanation Out Wire)->Trigger Output to EXT TRIG 2 of TDS3032", vbOKOnly, "Make Connection")
		MsgBox("Connect a BNC cable from The Dynamic Calibrator ->Druck to Channel 2 of TDS3032", vbOKOnly, "Make Connection")

		m_CableSetup = CABLE_SETUP.CABLE_SETUP_FIVE_DROPS
	End Sub

	Private Function VerifyDataInDB() As FUNCTION_RETURN

		'check to see if every value for the table is filled in
		If m_DB_TCT_Trig_Volt.bAllDataFilled = False Then

			MsgBox("Additional data will be collected from this TCT before verification." & vbCrLf & vbCrLf &
							  "Press OK to continue", vbOKOnly, "DATA COLLECTION")

			If Not New_TCT() Then
				VerifyDataInDB = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
				Exit Function
			End If
			If Not First_Voltages() Then
				VerifyDataInDB = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
				Exit Function
			End If
			'establish the cal curve using 3 points
			Call Corr_Fit(3)

			MsgBox("The IOP/Voltage Curve has been set." & vbCrLf & vbCrLf &
							  "Please press OK to continue to verification.", vbOKOnly, "CURVE ESTABLISHED")
		End If
		VerifyDataInDB = FUNCTION_RETURN.FUNCTION_RETURN_SUCCESS
	End Function

	Public Function RunTesting() As Boolean
		Dim eStatus As FUNCTION_RETURN = FUNCTION_RETURN.FUNCTION_RETURN_RESTART
StartApp:
		ChartModule.ClearCharts()
		'ChartModule.InitializeChart()

		m_TCTPatApp.Text = ""
		m_TCTStat.Text = ""
		m_TCTTrig.Text = ""
		m_TCTTrigStat.Text = ""
		m_PatApp.Text = ""
		m_DruckStat.Text = ""
		m_Status.Text = ""
		m_TCTSN.Text = ""

		eStatus = GetUserInputForInitTest()

		If eStatus = FUNCTION_RETURN.FUNCTION_RETURN_RESTART Then
			m_msgResult = MsgBox("Do you want to end the test?", vbYesNo + vbDefaultButton2, "END TESTING?")
			If m_msgResult = MsgBoxResult.Yes Then
				m_Status.Text = "User chose to end test."
				RunTesting = False
				Exit Function
			End If
			GoTo StartApp
		ElseIf eStatus = FUNCTION_RETURN.FUNCTION_RETURN_ERROR Then
			RunTesting = False
			Exit Function
		Else
			If m_CalType = CalType.CAL_TYPE_TCT Then
				eStatus = StartCalVer()
				If eStatus = FUNCTION_RETURN.FUNCTION_RETURN_ERROR Then
					'm_Status.Text = "TCT Test ended in error."
					RunTesting = False
					Exit Function
				ElseIf FUNCTION_RETURN.FUNCTION_RETURN_RESTART Then
					m_msgResult = MsgBox("Do you want to end the test?", vbYesNo + vbDefaultButton2, "END TESTING?")
					If m_msgResult = MsgBoxResult.Yes Then
						m_Status.Text = "User chose to end TCT Calibration Test."
						RunTesting = False
						Exit Function
					End If
					GoTo StartApp
				End If
			ElseIf m_CalType = CalType.CAL_TYPE_DYN Then
				eStatus = StartDynCal()
				If eStatus = FUNCTION_RETURN.FUNCTION_RETURN_ERROR Then
					RunTesting = False
					Exit Function
				ElseIf eStatus = FUNCTION_RETURN.FUNCTION_RETURN_RESTART Then
					m_msgResult = MsgBox("Do you want to end the test?", vbYesNo + vbDefaultButton2, "END TESTING?")
					If m_msgResult = MsgBoxResult.Yes Then
						m_Status.Text = "User chose to end Dynamic Calibration Test."
						RunTesting = False
						Exit Function
					End If
					GoTo StartApp
				End If
			End If
		End If

		RunTesting = True
	End Function


	'**************************************************************************
	'  Function:       scope_io()
	'
	'
	'  Purpose:        This is the main function to call when you
	'                  have to send a command via the serial port
	'                  to an oscilloscope for making a measurement.
	'
	'
	'
	'  Resources:      serial io port
	'
	'  Interactions:
	'
	'  Inputs:         structure of a command and parameters:
	'                  scope.channel
	'                  scope.coupling
	'                  .
	'                  .
	'                  .
	'                  see globals.c for the rest of the parameters
	'
	'  Outputs:        returns a value of the measurement requested
	'                  when called in the form of a string. If a fault
	'                  occurs, then a -99 is returned
	'
	'  Dependencies:   none.
	'  Err Conditions: none.

	'  Notes:
	'
	'  History:        6/10/98 - T.Hoepfinger: Initial version
	'                  02/02/99 - T.Hoepfinger: Revised comm fault to return -99
	'                              instead of a "fault" string(in the (get a measurement) mode)
	'                              Also changed Aquire:Numavg from 8 to 16 for compatibility
	'                              with the TDS210 & TDS340
	'                  09/17/99 - T.Hoepfinger: Added AQUIRE:NUMAV mode to 32  from 8.
	'                              TDS3032 was't showing good waveforms with AVG 8 mode.
	'                              Also added HEADER ON/OFF command to the scope for
	'                              taking single measurements.
	'                  09/20/99 - T.Hoepfinger: REMOVED HEADER ON/OFF commands
	'                  12/6/01 - J. Akin: Revised to allow for sample aquisition mode
	'                  05/20/04 - T.Hoepfinger: Changed bandwidth for each channel to 20MHZ.
	'                                   in an attempt to get more repeatable(cleaner) signals for the scope
	'                   06/02/04 - T.Hoepfinger: Added Ext trigger coupling --> High Freq Reject
	'***************************************************************************/
	Private Function scope_io(ByVal TrigChan As Boolean) As Boolean
		Const Wait As Integer = 5.0#  'timeout time for response from unit
		Dim Readstring As String = ""
		Dim Wfm_setup(6) As String
		Dim Temp As Integer

		'send all required commands to the scope
		'************************************************************
		'                 VERTICAL MENU
		'************************************************************
		'select channel

		m_ioSerialPort.Send("SELECT:CH1 ON" & vbCr)
		TimeDelay(200)

		m_ioSerialPort.Send("SELECT:CH2 ON" & vbCr)
		TimeDelay(200)

		m_ioSerialPort.Send("CH1:BANDWIDTH TWENTY" & vbCr)
		TimeDelay(200)

		m_ioSerialPort.Send("CH2:BANDWIDTH FULL" & vbCr)
		TimeDelay(200)

		'Offset
		m_ioSerialPort.Send("CH" & scope.channel & ":POSITION " & scope.Offset & vbCr)
		TimeDelay(200)

		'send channel and volts/division
		m_ioSerialPort.Send("CH" & scope.channel & ":SCALE " & scope.vert & ";POSITION " & scope.vertposition & vbCr)
		TimeDelay(200)

		'send coupling
		m_ioSerialPort.Send("CH" & scope.channel & ":COUPLING " & scope.coupling & vbCr)
		TimeDelay(200)

		'************************************************************
		'                   HORIZONTAL MENU
		'************************************************************
		'send horizontal-main-scale
		m_ioSerialPort.Send("HOR:MAIN:SCA " & scope.horiz & vbCr)
		TimeDelay(200)

		If TrigChan = True Then
			'horizontal-trigger-position to 50%(middle of wavefrom captured)
			m_ioSerialPort.Send("HOR:TRIG:POS 50" & vbCr)
			TimeDelay(200)

			'************************************************************
			'                   TRIGGER MENU
			'************************************************************
			'trigger-main-edge-slope-rising edge
			m_ioSerialPort.Send("TRIG:MAIN:EDGE:SLOPE " & scope.tr_edge & vbCr)
			TimeDelay(200)

			'send trigger-main-level
			m_ioSerialPort.Send("TRIG:MAIN:LEVEL " & scope.tr_level & vbCr)
			TimeDelay(200)

			'set trigger COUPLINGe
			m_ioSerialPort.Send("TRIG:MAIN:EDGE:COUP HFREJ" & vbCr)
			TimeDelay(200)
		End If

		'************************************************************
		'                   AQUIRE MENU
		'************************************************************
		'WAVEFROM X-FER SET-UP
		If scope.waveform = True Then
			'initialize strings to sent
			Wfm_setup(0) = "DATA:ENC RPB"   'encode data as binary(positive integer 0-255)
			Wfm_setup(1) = "DATA:WID 2"     'encode data width(1 byte)
			Wfm_setup(2) = "DATA:START 1"   'encode data start to first data point for download
			Wfm_setup(3) = "DATA:STOP 999"  'encode data stop to last data point for download
			Wfm_setup(4) = "DATA:SOURCE CH" & scope.channel
			Wfm_setup(5) = "ACQUIRE:MODE " & scope.acq_mode
			Wfm_setup(6) = "ACQUIRE:STOPAFTER SEQUENCE" ' single sequence (on trigger)
			'send all the waveform setup commands
			For Temp = 0 To 6
				m_ioSerialPort.Send(Wfm_setup(Temp) & vbCr)
				TimeDelay(200)
			Next Temp

			'return status
			scope_io = True

		Else 'get a measurement
			'finish sending commands for measurement process
			'set up measure SOURCE and STATE(display on scope) and TYPE
			If scope.acq_mode = "SAMPLE" Then
				m_ioSerialPort.Send("ACQUIRE:MODE SAMPLE" & vbCr)
				TimeDelay(100)
			Else
				m_ioSerialPort.Send("ACQUIRE:MODE AVERAGE")
				TimeDelay(200)
				m_ioSerialPort.Send("ACQUIRE:NUMAV 16")
				TimeDelay(200)
			End If

			m_ioSerialPort.Send("MEASU:MEAS1:SOURCE1 CH" & scope.channel & vbCr)
			TimeDelay(500)

			m_ioSerialPort.Send("MEASU:MEAS1:STATE ON" & vbCr)
			TimeDelay(500)

			m_ioSerialPort.Send("MEASU:MEAS1:TYPE " & scope.measure & vbCr)
			TimeDelay(500)

			'-----------------------------------------------------
			'J. Akin moved to seperate routine Scope_Read 12/6/01
			'-----------------------------------------------------
			'first clear input buffer
			m_ioSerialPort.ClearInputBuffer()

			'Issue the read value command, wait for a response
			If Not m_ioSerialPort.Send("MEASU:MEAS1:VALUE?" & vbCr, Readstring, Wait) Then
				m_Status.Text = "Communication Fault: Oscilloscope not responding"
				scope_io = False
				Exit Function
			End If

			'make sure rest of data is dumped
			'TimeDelay(2000)

			'return the received value as a data type = single
			'scope_io = CSng(Readstring)
			m_Status.Text = Readstring
			Form1.Refresh()

			scope_io = True
		End If
	End Function

	'**************************************************************************
	'  Function:       Double_Side()
	'
	'  Purpose:        Take 5 drops on the L, M, and H settings and verifies
	'                  that the TCT triggers at P1 and P2.  Writes Druck
	'                  voltage at trigger to a CSV file.
	'
	'  Resources:
	'
	'  Interactions:   Writes MySQL database.
	'
	'  Inputs:
	'
	'  Outputs:
	'
	'  Dependencies:
	'  Err Conditions: none.
	'
	'  Notes:
	'
	'  History:        05/17/12 - D. Tolsma: Initial version
	'
	'***************************************************************************/
	Private Function Double_Side() As Boolean
		Dim VoltsPerStep As Single
		Dim iEyesetting As Integer
		Dim Setting As String = ""
		Dim Shot As Integer
		Dim Index As Integer
		Dim last_waveform As Long = 0 'holds a waveform amplitude when searching for TCT trigger
		Dim trigger_count 'tracks how many trigger points were found

		Dim P1_Druck_trigger(EYESETTING.EYESET_MAX - 1, MAX_NUM_SHOTS) As Single
		Dim P2_Druck_trigger(EYESETTING.EYESET_MAX - 1, MAX_NUM_SHOTS) As Single 'these are the Druck voltages at trigger, (eyesetting, shot)
		Dim NUM_SHOTS As Integer = 2

		If m_TCTSerialNum <> "2" Then
			MsgBox("The TCT will now be checked to verify that there are two applanation pulses.", vbOKOnly, "DOUBLE-SIDED CHECK")
		Else
			MsgBox("The TCT will now be checked to verify the applanation pulse.", vbOKOnly, "APP CHECK")
		End If

		If m_CableSetup <> CABLE_SETUP.CABLE_SETUP_DOUBLE_SIDE Then
			SetupCablesForDS()
		End If

		If m_TCTSerialNum <> "2" Then
			m_Status.Text = "Double-sided applanation verification"
		Else
			m_Status.Text = "Applanation verification"
		End If

		'adjust the scope for capturing both applanation points
		scope.horiz = "4E-3"
		m_XAxisLabel.Text = "4.0 ms/Div"
		Form1.Refresh()

		'set up trigger
		scope.tr_edge = "RISE"
		scope.tr_source = "CH2"
		scope.tr_level = "1.0"

		'NOW SEND THE PARAMETERS
		If scope_io(False) = False Then
			MsgBox("Fault setting up Scope", vbOKOnly, "SCOPE FAULT")
			Double_Side = False
			m_Status.Text = "In Double_Side(), call to scope_io failed."
			Exit Function
		End If

		'horizontal-trigger-position to 50%(middle of wavefrom captured)
		m_ioSerialPort.Send("HOR:TRIG:POS 50" & vbCr)
		TimeDelay(200)

		'Set Horizontal Resolution to 500 points
		m_ioSerialPort.Send("HOR:RESOLUTION LOW" & vbCr)
		TimeDelay(200)

		'Set display time delay
		m_ioSerialPort.Send("HOR:DEL:TIME 17.5E-3" & vbCr)
		TimeDelay(200)

		'Set display intensity
		m_ioSerialPort.Send("DISPLAY:INTENSITY:WAVEFORM 100" & vbCr)
		TimeDelay(200)

		'determine the rest pressure
		If Not BaselinePressure() Then
			Double_Side = False
			Exit Function
		End If

		'************************************************************
		'                   TRIGGER MENU
		'************************************************************

		'send trigger-main-level
		m_ioSerialPort.Send("TRIG:MAIN:LEVEL " & scope.tr_level & vbCr)
		TimeDelay(200)

		'trigger-main-edge-slope-rising edge
		m_ioSerialPort.Send("TRIG:MAIN:EDGE:SLOPE " & scope.tr_edge & vbCr)
		TimeDelay(200)

		'set trigger source
		m_ioSerialPort.Send("TRIG:MAIN:EDGE:SOURCE " & scope.tr_source & vbCr)
		TimeDelay(200)

		'set trigger COUPLING
		m_ioSerialPort.Send("TRIG:MAIN:EDGE:COUP HFREJ" & vbCr)
		TimeDelay(200)

		'check each setting
		For iEyesetting = EYESETTING.EYESET_HI_C To EYESETTING.EYESET_LOW_A Step -1

			'clear all status boxes
			m_TCTTrig.Text = ""
			m_TCTTrigStat.Text = ""
			Form1.Refresh()

			If iEyesetting = EYESETTING.EYESET_LOW_A Then
				Setting = "L/A"
			ElseIf iEyesetting = EYESETTING.EYESET_MID_B Then
				Setting = "M/B"
			ElseIf iEyesetting = EYESETTING.EYESET_HI_C Then
				Setting = "H/C"
			End If

			'notify the user if there is a setting change
			MsgBox("Make sure the TCT is on the " & Setting & " position", vbOKOnly, "SET TCT TO THE " & Setting & " POSITION")


			For Shot = 1 To NUM_SHOTS

RetryDrop:
				'reset the trigger-point counter
				trigger_count = 0

				'Re-arm the Scope for another  Single sequence
				m_ioSerialPort.Send("ACQUIRE:MODE SAMPLE" & vbCr)
				TimeDelay(200)

				m_ioSerialPort.Send("ACQUIRE:STATE ON" & vbCr)
				TimeDelay(200)

				m_ioSerialPort.Send("ACQUIRE:STOPAFTER SEQUENCE" & vbCr)
				TimeDelay(200)

				'tell the user to drop
				MsgBox("Manually lift and release the weight on the TCT Dynamic calibrator. Press OK when finished.", vbOKOnly, "DROP THE DEAD WEIGHT NOW")
				m_Status.Text = "TCT Position " & Setting & " Shot # " & Shot & " of " & NUM_SHOTS
				Form1.Refresh()

				'grab the waveforms
				If Not Display_Waveforms(iEyesetting, Shot) Then
					'check to see if a drop was recorded

					'let the operator know
					m_msgResult = MsgBox("Did not read the signal - Retry?", vbYesNo, "SCOPE READING ERROR")
					If m_msgResult = MsgBoxResult.Yes Then
						GoTo RetryDrop
					Else
						m_Status.Text = "Signal not read. User chose to end test."
						Double_Side = False
						Exit Function
					End If
				End If

				VoltsPerStep = 5 / 65535

				'post-process filter the waveform data, on ch1 since ch2 is an impulse
				Filter_Data(m_Waveform2, DATA_POINTS - 1)

				'look for the first trigger point
				For Index = 1 To DATA_POINTS_MIDPT
					last_waveform = m_Waveform1(Index - 1)

					'is there a spike?
					If (m_Waveform1(Index) - last_waveform) > 5000 Then
						'increment the trigger counter
						trigger_count = trigger_count + 1

						'grab druck voltage at P1 trigger
						P1_Druck_trigger(iEyesetting, Shot) = (m_Waveform2(Index) - m_dDruckPressureBaseline) * VoltsPerStep
						Exit For
					End If
				Next Index

				'did the TCT trigger at P1?
				If trigger_count <> 1 Then
					'if not, notify the user
					m_msgResult = MsgBox("Error!  The P1 trigger did not register." & vbCrLf & vbCrLf &
									  "Would you like to try again?" & vbCrLf & vbCrLf &
									  "Selecting NO will end this test and TCT Verification will FAIL." _
									  , vbCritical + vbYesNo, "P1 TRIGGER FAILURE!")
					'try again
					If m_msgResult = MsgBoxResult.Yes Then
						GoTo RetryDrop
						'done, end
					Else
						m_Status.Text = "Test ended in failure."
						Double_Side = False
						Exit Function
					End If
				End If

				If m_TCTSerialNum <> "2" Then

					'look for the second trigger point
					For Index = DATA_POINTS_MIDPT To DATA_POINTS - 1
						last_waveform = m_Waveform1(Index - 1)
						'is there a spike?
						If m_Waveform1(Index) - last_waveform > 5000 Then
							'incriment the trigger counter
							trigger_count = trigger_count + 1

							'grab druck voltage at P1 trigger
							P2_Druck_trigger(iEyesetting, Shot) = (m_Waveform2(Index) - m_dDruckPressureBaseline) * VoltsPerStep
							Exit For
						End If

					Next Index

					'did the TCT trigger at P2?
					If trigger_count <> 2 Then
						'if not, notify the user
						m_msgResult = MsgBox("Error!  The P2 trigger did not register." & vbCrLf & vbCrLf &
										  "Would you like to try again?" & vbCrLf & vbCrLf &
										  "Selecting NO will end this test and TCT Verification will FAIL." _
										  , vbCritical + vbYesNo, "P2 TRIGGER FAILURE!")
						'try again
						If m_msgResult = MsgBoxResult.Yes Then
							GoTo RetryDrop
							'done, end
						Else
							m_Status.Text = "Test ended in failure."
							Double_Side = False
							Exit Function
						End If
					End If

				End If

				OutFileModule.WriteFile1(True, m_TCTSerialNum, iEyesetting, Shot, m_iDynCal_Num, P1_Druck_trigger(iEyesetting, Shot), P2_Druck_trigger(iEyesetting, Shot))
			Next 'Shot
		Next 'Eyesetting


		If m_TCTSerialNum <> "2" Then
			MsgBox("Double-sided applanation verification passed.", vbOKOnly, "APP TRIGGER OK")
		Else
			MsgBox("Applanation verification passed.", vbOKOnly, "APP TRIGGER OK")
		End If

		Double_Side = True
	End Function

	Private Function ReArmScopeForTCTSecondPhase() As Boolean

		're-arm the scope for the rest of the process
		m_Status.Text = "Preparing scope for next phase."
		Form1.Refresh()

		'clear waveforms from screen
		ClearCharts()

		'Set display time delay
		m_ioSerialPort.Send("HOR:DEL:TIME 0.0" & vbCr)
		TimeDelay(200)

		scope.waveform = True
		scope.coupling = "DC"
		scope.channel = "1"
		scope.vert = "1E0"
		scope.horiz = "2E-3"
		scope.Offset = "-3"

		m_XAxisLabel.Text = "2.0 ms/Div"
		m_YAxisLabel.Text = "1.0 volts/Div"
		Form1.Refresh()

		'NOW SEND THE PARAMETERS
		If scope_io(False) = False Then
			MsgBox("Fault setting up Scope", vbOKOnly, "SCOPE FAULT")
			ReArmScopeForTCTSecondPhase = False
			Exit Function
		End If

		'----------------------------------------------------------------------------------------
		' setup Ch 2 and parameters on the scope
		'----------------------------------------------------------------------------------------
		scope.tr_edge = "RISE"
		scope.tr_source = "EXT"
		scope.tr_level = "6E0"
		'horizontal-trigger-position to 50%(middle of wavefrom captured)
		m_ioSerialPort.Send("HOR:TRIG:POS 50" & vbCr)
		TimeDelay(200)

		'Set Horizontal Resolution to 500 points
		m_ioSerialPort.Send("HOR:RESOLUTION LOW" & vbCr)
		TimeDelay(200)

		'************************************************************
		'                   TRIGGER MENU
		'************************************************************

		'trigger-main-edge-slope-rising edge
		m_ioSerialPort.Send("TRIG:MAIN:EDGE:SLOPE " & scope.tr_edge & vbCr)
		TimeDelay(200)

		'set trigger source
		m_ioSerialPort.Send("TRIG:MAIN:EDGE:SOURCE " & scope.tr_source & vbCr)
		TimeDelay(200)

		'set trigger COUPLING
		m_ioSerialPort.Send("TRIG:MAIN:EDGE:COUP HFREJ" & vbCr)
		TimeDelay(200)

		'*************************************************************************
		' Get the TCT Number
		'*************************************************************************

		scope.waveform = True
		scope.channel = "2"
		scope.coupling = "DC"
		scope.Offset = "-3"
		scope.vert = ".5E0"  ''9/6/07  was 2E0  ' Moved druck to this channel
		scope.acq_mode = "SAMPLE"

		'NOW SEND THE PARAMETERS
		If scope_io(False) = False Then
			MsgBox("Fault setting up Scope", vbOKOnly, "SCOPE FAULT")
			ReArmScopeForTCTSecondPhase = False
			Exit Function
		End If

		ReArmScopeForTCTSecondPhase = True
	End Function

	Private Sub TimeDelay(ByVal dTimeInMillisec As Double)

		Dim CommTimer As New Stopwatch()
		Dim ElapsedTime As TimeSpan
		Dim dSeconds As Double = dTimeInMillisec / 1000

		CommTimer.Start()
		Do
			Application.DoEvents()
			ElapsedTime = CommTimer.Elapsed
		Loop Until (ElapsedTime.TotalSeconds > dSeconds)
		CommTimer.Reset()

	End Sub
	'**************************************************************************
	'  Function:       BaselinePressure()
	'
	'  Purpose:        Calculates the rest/equilibrium pressure of the system.
	'                  The value is used to establish a ground voltage and null pressure.
	'
	'  Resources:
	'
	'  Interactions:   Communicates directly with the TDS 3032 scope
	'
	'  Inputs:
	'
	'  Outputs:        Updates variable m_dDruckPressureBaseline, which holds the null pressure
	'                  offset value and is used in waveform voltage calculations.
	'
	'  Dependencies:
	'  Err Conditions: none.
	'
	'  Notes:
	'
	'  History:        05/07/12 - D. Tolsma: Initial version
	'
	'***************************************************************************/
	Private Function BaselinePressure() As Boolean

		Dim Temp As Integer
		Dim Sum As Double 'tracks running sum for average calculation

		Sum = 0 'initialize

		'Re-arm the Scope for another  Single sequence
		m_ioSerialPort.Send("ACQUIRE:MODE SAMPLE" & vbCr)
		TimeDelay(200)

		m_ioSerialPort.Send("ACQUIRE:STATE ON" & vbCr)
		TimeDelay(200)

		m_ioSerialPort.Send("ACQUIRE:STOPAFTER SEQUENCE" & vbCr)
		TimeDelay(200)

		'set the trigger level
		m_ioSerialPort.Send("TRIGGER:FORCE" & vbCr)
		TimeDelay(200)

		'First do the druck
		'dump the pressure data from the druck
		If Not DataDump(SCOPE_CHANNEL.SCOPE_CHANNEL_2, EYESETTING.EYESET_MAX, 1) Then
			BaselinePressure = False
			Exit Function
		End If

		For Temp = 0 To 319
			Sum = Sum + m_Waveform2(Temp)
		Next Temp

		'extract the average pressure for the druck at rest
		m_dDruckPressureBaseline = Sum / 320

		Sum = 0
		'then do the TCT
		If Not DataDump(SCOPE_CHANNEL.SCOPE_CHANNEL_1, EYESETTING.EYESET_MAX, 1) Then
			BaselinePressure = False
			Exit Function
		End If

		For Temp = 0 To 319
			Sum = Sum + m_Waveform2(Temp)
		Next Temp

		'extract the average pressure for the druck at rest
		m_dTCTPressureBaseline = Sum / 320

		BaselinePressure = True

	End Function

	'**************************************************************************
	'  Function:       Display_Waveforms(Eyesetting As Integer, Shot As Integer)
	'
	'  Purpose:        Sets the channel, and cal type desired to display,
	'                  then calls the function datadump()
	'
	'  Resources:
	'
	'  Interactions:   Calls DataDump
	'
	'  Inputs:         -Eyesetting As Integer (0 = L, 1 = M, 2 = H)
	'                  -Shot As Integer (Indicates what shot in the sequence is being taken)
	'
	'  Outputs:
	'
	'  Dependencies:
	'  Err Conditions: none.
	'
	'  Notes:
	'
	'  History:        6/30/98 - T.Hoepfinger: Initial version
	'
	'***************************************************************************/
	Private Function Display_Waveforms(iEyesetting As Integer, Shot As Integer) As Boolean

		Dim sResponse As String = ""

		If m_CalType = CalType.CAL_TYPE_TCT Then

			'First do channel 1 (FOR EITHER CALIBRATION FIXTURE)
			m_ioSerialPort.Send("DATA:SOURCE CH1" & vbCr)
			If Not DataDump(SCOPE_CHANNEL.SCOPE_CHANNEL_1, iEyesetting, Shot) Then
				Display_Waveforms = False
				Exit Function
			End If

			'Now do channel 2
			m_ioSerialPort.Send("DATA:SOURCE CH2" & vbCr)
			If Not DataDump(SCOPE_CHANNEL.SCOPE_CHANNEL_2, iEyesetting, Shot) Then
				Display_Waveforms = False
				Exit Function
			End If

		ElseIf m_CalType = CalType.CAL_TYPE_DYN Then
			m_ioSerialPort.Send("DATA:SOURCE CH2" & vbCr)
			If Not DataDump(SCOPE_CHANNEL.SCOPE_CHANNEL_2, iEyesetting, Shot) Then
				Display_Waveforms = False
				Exit Function
			End If
		End If
		Display_Waveforms = True
	End Function

	'**************************************************************************
	'  Function:       Filter_Data(ByRef array1() As Long, size As Integer)
	'
	'  Purpose:        Receives a waveform array of defined size and returns a filtered waveform.
	'                  The filtered waveform is construction using an 11-point 'boxcar' filter.
	'
	'  Resources:
	'
	'  Interactions:
	'
	'  Inputs:         array1(), the waveform that is to be filtered
	'                  size, the size of the array that is to be filtered
	'
	'  Outputs:        returns filtered version of array1()
	'
	'  Dependencies:   array2() is of fixed size.  This size must be increased if the waveform
	'                  to be filtered has a size larger than 500.
	'  Err Conditions: none.
	'
	'  Notes:
	'
	'  History:        05/07/12 - D. Tolsma: Initial version
	'
	'***************************************************************************/
	Private Sub Filter_Data(ByRef array1() As Long, size As Integer)

		Dim i, k As Integer
		Dim c_sum As Double 'stores the cumulative sum for filtering
		Dim c_avg As Double 'stores the cumulative average for filtering
		Dim array2(DATA_POINTS - 1) As Long 'this is a placeholder/dummy array


		For i = 0 To size
			'set cumulative variables to 0 for each i
			c_sum = 0
			c_avg = 0

			'don't exam start and endpoints
			If i < 5 Then 'do nothing
			ElseIf i > (size - 5) Then 'do nothing
			Else
				'filter the data
				For k = (i - 5) To (i + 5)
					'accumulate data around the point of interest
					c_sum = c_sum + array1(k)
				Next k

				'take the cumulative average for the point of interest
				c_avg = c_sum / 11

				'compile the filtered data array
				array2(i) = c_avg
			End If
		Next i

		'return the filtered data
		For i = 0 To size
			array1(i) = array2(i)
		Next i

	End Sub

	'*************************************************************************************
	'       History:    5/20/04: T.Hoepfinger: Added Baseline Subtraction
	'                   11/2/05: T.Hoepfinger: Changed trig point(and edge for TCT #20 ( New Zero Hys TCT)
	'                   05/08/06 - T.Hoepfinger: Added back old neg edge trigger for TCT #4 only
	'                                            to support Master Xpert Calibration
	'                   09/06/07 - T.Hoepfinger: Removed applanation detection. Only TCT & Druck
	'                                            pressure signals are being evaluated now
	'*************************************************************************************
	Private Function DataDump(ByVal iChannel As Integer, ByVal iEyesetting As Integer, ByVal iShot As Integer) As Boolean
		Dim iTimeLimit As Integer = 6           'timeout is seconds for the scope to respond
		''Dim Starttime, PresentTime, DelayTime As Single
		Dim Binarydump As String
		Dim DumpNums As Integer
		Dim DumpSize As Integer
		Dim Temp As Integer
		Dim Temp2 As Long
		Dim BaselineCh1 As Long


		'/////////////////////////////////////////////////////////////////////////////////
		'read the trigger status to see if the scope saw anything
		Binarydump = "99"
		m_ioSerialPort.Send("ACQ:STATE?" & vbCr, Binarydump, 0.5)

		If Val(Binarydump.ElementAt(0)) = 1 Then
			MsgBox("The Oscope did not read a waveform???" & vbCr & vbCr & "Please verify the TCT, the instrument and the Oscope are setup properly", vbOKOnly + vbCritical, "OSCOPE TRIGGER ERROR")
			m_Status.Text = "In DataDump(), The Oscope did not read a waveform"
			DataDump = False
			Exit Function
		End If
		'/////////////////////////////////////////////////////////////////////////////////


		'get data
		m_ioSerialPort.ClearInputBuffer()
		Dim sRxData As String = ""
		Dim bytBuffer(1500) As Byte
		If Not m_ioSerialPort.SendReadByteArray("CURVE?" & vbCr, bytBuffer, iTimeLimit) Then
			m_Status.Text = "In DataDump(), No communication with the scope"
			DataDump = False
			Exit Function
		End If

		' bytBuffer(0-5) = "#41000"
		DumpNums = CInt(Convert.ToChar(bytBuffer(1)).ToString) 'number of numbers of the download
		DumpSize = CInt(Convert.ToChar(bytBuffer(2)).ToString) * 1000 + CInt(Convert.ToChar(bytBuffer(3)).ToString) * 100 + CInt(Convert.ToChar(bytBuffer(4)).ToString) * 10 + CInt(Convert.ToChar(bytBuffer(5)).ToString)

		'--------------------------------------------------
		'NO SCOPE SIGNAL NOTES:
		'Len Binarydump= 2006
		'DumpSize = 1998   
		'DumpNums = 4
		'If Len(Binarydump) = 2006 Then
		If DumpNums <> 4 Or DumpSize <> 1000 Or bytBuffer.Length <> 1006 Then
			m_Status.Text = "In DataDump(), Error - Size of Tx is: " & DumpSize.ToString
			DataDump = False
			Exit Function
		End If
		'--------------------------------------------------

		'Now fill waveform array
		Dim TempByte As Byte = 0
		For Temp = 6 To bytBuffer.Length - 1 Step 1
			'get the data point(one char) from the BinaryDump string
			TempByte = bytBuffer(Temp)

			'Convert to a long every other byte
			If (Temp Mod 2) = 0 Then
				'upper byte
				Temp2 = TempByte * 256
			Else
				'lower byte
				If iChannel = SCOPE_CHANNEL.SCOPE_CHANNEL_1 Then
					m_Waveform1(Int(Temp / 2) - 3) = CLng(Temp2 + TempByte)
				Else
					m_Waveform2(Int(Temp / 2) - 3) = CLng(Temp2 + TempByte)
				End If
			End If

		Next Temp

		'calc baseline and apply to filtered m_Waveform2(Druck)
		If iChannel = SCOPE_CHANNEL.SCOPE_CHANNEL_1 Then
			'initialize
			BaselineCh1 = 0

			For Temp = 0 To 9
				BaselineCh1 = BaselineCh1 + m_Waveform1(Temp)
			Next Temp

			'final value
			BaselineCh1 = BaselineCh1 / 10
		End If

		'display waveform from scope on VB picture object
		'save to file and plot line on picture box in Form
		Dim iNumPtsToPlot As Integer = DATA_POINTS - 1

		If iChannel = SCOPE_CHANNEL.SCOPE_CHANNEL_1 Then
			ChartModule.DrawLine(iChannel, iEyesetting, iShot, iNumPtsToPlot, m_Waveform1)
		Else
			ChartModule.DrawLine(iChannel, iEyesetting, iShot, iNumPtsToPlot, m_Waveform2)
		End If

		DataDump = True
	End Function

	Private Sub SetSettingToleranceLimits(ByRef strtSetting As SettingMaxMin, ByVal bFixedValues As Boolean)
		Dim sngLimit As Single = 0.18

		If bFixedValues Then
			strtSetting.C_SETTING_MAX = Format(m_DB_Fixed_TCT_Trig_Volt.V_TCT_at_Druck_Trig(EYESETTING.EYESET_HI_C) + sngLimit, "0.00")
			strtSetting.C_SETTING_MIN = Format(m_DB_Fixed_TCT_Trig_Volt.V_TCT_at_Druck_Trig(EYESETTING.EYESET_HI_C) - sngLimit, "0.00")
			strtSetting.B_SETTING_MAX = Format(m_DB_Fixed_TCT_Trig_Volt.V_TCT_at_Druck_Trig(EYESETTING.EYESET_MID_B) + sngLimit, "0.00")
			strtSetting.B_SETTING_MIN = Format(m_DB_Fixed_TCT_Trig_Volt.V_TCT_at_Druck_Trig(EYESETTING.EYESET_MID_B) - sngLimit, "0.00")
			strtSetting.A_SETTING_MAX = Format(m_DB_Fixed_TCT_Trig_Volt.V_TCT_at_Druck_Trig(EYESETTING.EYESET_LOW_A) + sngLimit, "0.00")
			strtSetting.A_SETTING_MIN = Format(m_DB_Fixed_TCT_Trig_Volt.V_TCT_at_Druck_Trig(EYESETTING.EYESET_LOW_A) - sngLimit, "0.00")
		Else
			strtSetting.C_SETTING_MAX = Format(m_DB_TCT_Trig_Volt.V_TCT_at_Druck_Trig(EYESETTING.EYESET_HI_C) + sngLimit, "0.00")
			strtSetting.C_SETTING_MIN = Format(m_DB_TCT_Trig_Volt.V_TCT_at_Druck_Trig(EYESETTING.EYESET_HI_C) - sngLimit, "0.00")
			strtSetting.B_SETTING_MAX = Format(m_DB_TCT_Trig_Volt.V_TCT_at_Druck_Trig(EYESETTING.EYESET_MID_B) + sngLimit, "0.00")
			strtSetting.B_SETTING_MIN = Format(m_DB_TCT_Trig_Volt.V_TCT_at_Druck_Trig(EYESETTING.EYESET_MID_B) - sngLimit, "0.00")
			strtSetting.A_SETTING_MAX = Format(m_DB_TCT_Trig_Volt.V_TCT_at_Druck_Trig(EYESETTING.EYESET_LOW_A) + sngLimit, "0.00")
			strtSetting.A_SETTING_MIN = Format(m_DB_TCT_Trig_Volt.V_TCT_at_Druck_Trig(EYESETTING.EYESET_LOW_A) - sngLimit, "0.00")
		End If
	End Sub

	Private Sub SetDruckToleranceLimits(ByRef strtDruck As DruckMaxMin)
		strtDruck.C_DRUCK_MAX = Format(m_DB_TCT_Trig_Volt.Druck_Trig(EYESETTING.EYESET_HI_C) + 0.04, "0.00")
		strtDruck.C_DRUCK_MIN = Format(m_DB_TCT_Trig_Volt.Druck_Trig(EYESETTING.EYESET_HI_C) - 0.04, "0.00")
		strtDruck.B_DRUCK_MAX = Format(m_DB_TCT_Trig_Volt.Druck_Trig(EYESETTING.EYESET_MID_B) + 0.02, "0.00")
		strtDruck.B_DRUCK_MIN = Format(m_DB_TCT_Trig_Volt.Druck_Trig(EYESETTING.EYESET_MID_B) - 0.02, "0.00")
		strtDruck.A_DRUCK_MAX = Format(m_DB_TCT_Trig_Volt.Druck_Trig(EYESETTING.EYESET_LOW_A) + 0.02, "0.00")
		strtDruck.A_DRUCK_MIN = Format(m_DB_TCT_Trig_Volt.Druck_Trig(EYESETTING.EYESET_LOW_A) - 0.02, "0.00")
	End Sub

	Public Function StartCalVer() As FUNCTION_RETURN
		Dim VoltsPerStep As Single
		Dim iEyesetting As Integer
		Dim Setting As String = ""
		'Dim TCTOffset As Single '6/20/11, used to read proper trigger values for TCT
		'Dim DruckOffset As Single '6/20/11, used to read proper trigger values for Druck
		Dim sngTempMax, sngTempMin As Single

		' this is used for calulating time for Ramp rate calculation

		Dim Index_Ramp_Start, Index_Ramp_Stop As Integer

		Dim strtSetting As SettingMaxMin
		Dim strtDruck As DruckMaxMin

		'*************************************************************************
		' Read the TCT Number data from the Database
		'*************************************************************************
		'Now read the SQL data table for the settings.
		Dim sStatus As String = ""
		If Not Read_SQL_DB(m_TCTSerialNum, m_DB_TCT_Trig_Volt, sStatus) Then
			m_Status.Text = sStatus
			StartCalVer = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			Exit Function
		End If

		' set tolerance limits to test
		SetSettingToleranceLimits(strtSetting, False)

		SetDruckToleranceLimits(strtDruck)

		'determine the rest pressure
		If Not BaselinePressure() Then
			StartCalVer = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			Exit Function
		End If

		' open the file and place header in the file for this session
		OutFileModule.WriteFile2Header(True, m_TCTSerialNum, m_OperatorName, strtDruck)

		If m_CableSetup <> CABLE_SETUP.CABLE_SETUP_FIVE_DROPS Then
			SetupCablesForFiveDrops()
		End If

		'***********************************************************************
		' MAIN TEST LOOP
		'***********************************************************************
		For iEyesetting = EYESETTING.EYESET_HI_C To EYESETTING.EYESET_LOW_A Step -1
			''fetch the fixed voltage triggers
			'' Akin - NO
			'' Read_SQL_DB (0)

			'send trigger-main-level
			m_ioSerialPort.Send("TRIG:MAIN:LEVEL " & m_DB_TCT_Trig_Volt.Druck_Trig(iEyesetting) & vbCr)
			TimeDelay(500)

			'Tell the operator to set the TCT box
			If iEyesetting = EYESETTING.EYESET_LOW_A Then
				Setting = "L/A"
			ElseIf iEyesetting = EYESETTING.EYESET_MID_B Then
				Setting = "M/B"
			ElseIf iEyesetting = EYESETTING.EYESET_HI_C Then
				Setting = "H/C"
			End If
			MsgBox("Make sure the TCT is on the " & Setting & " position", vbOKOnly, "SET TCT TO THE " & Setting & " POSITION")

			For Shot As Integer = 1 To MAX_NUM_SHOTS
RetryDrop:
				'Re-arm the Scope for another  Single sequence
				m_ioSerialPort.Send("ACQUIRE:MODE SAMPLE" & vbCr)
				TimeDelay(200)

				m_ioSerialPort.Send("ACQUIRE:STATE ON" & vbCr)
				TimeDelay(200)

				m_ioSerialPort.Send("ACQUIRE:STOPAFTER SEQUENCE" & vbCr)
				TimeDelay(400)

				'Operator to lift dropper
				MsgBox("Manually lift and release the weight on the TCT Dynamic calibrator. Press OK when finished.", vbOKOnly, "DROP THE DEAD WEIGHT NOW")

				m_Status.Text = "TCT Position " & Setting & " Shot # " & Shot & " of " & MAX_NUM_SHOTS
				Form1.Refresh()

				'Check to make sure the scope got a signal
				If Display_Waveforms(iEyesetting, Shot) = False Then
					'let the operator know
					m_msgResult = MsgBox("Did not read the signal - Retry?", vbYesNo, "SCOPE READING ERROR")
					If m_msgResult = MsgBoxResult.Yes Then
						GoTo RetryDrop
					Else
						StartCalVer = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
						Exit Function
					End If
				End If

				'Tolsma
				VoltsPerStep = 5 / 65535

				'Calculate Ramp rate
				For Index As Integer = 0 To DATA_POINTS - 1
					If m_Waveform1(Index) >= TCT_RAMPLOW_PSI Then
						'label start
						Index_Ramp_Start = Index

						For Index2 As Integer = Index To DATA_POINTS - 1
							If m_Waveform1(Index2) >= TCT_RAMPHIGH_PSI Then
								'label end
								Index_Ramp_Stop = Index2

								'done, get out of Index For Loop
								Index = DATA_POINTS - 1
								Index2 = DATA_POINTS - 1

							End If
						Next Index2
					End If
				Next Index

				If Index_Ramp_Start = Index_Ramp_Stop Then
					m_Status.Text = "In StartCalVer(), cannot calculate TCT_RampRate. Test ended in error."
					StartCalVer = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
					Exit Function
				End If

				' Calculate ramp now
				m_TCT_RampRate(iEyesetting, Shot) = Format(1.8 / ((Index_Ramp_Stop - Index_Ramp_Start) / TCT_CNTS_PER_MS), "0.00")
				m_PatApp.Text = "Ramp Rate(volt/ms) = " & Format(m_TCT_RampRate(iEyesetting, Shot), "0.00")
				Form1.Refresh()

				'6/20/11, Used to read in the correct trigger voltages at each setting
				'If iEyesetting = EYESETTING.EYESET_LOW_A Then
				'	'TCTOffset = 1.97
				'	'DruckOffset = 2.8
				'	TCTOffset = 1.77
				'	DruckOffset = 2.78
				'ElseIf iEyesetting = EYESETTING.EYESET_MID_B Then
				'	'TCTOffset = 1.96
				'	'DruckOffset = 3.29
				'	TCTOffset = 1.76
				'	DruckOffset = 3.22
				'ElseIf iEyesetting = EYESETTING.EYESET_HI_C Then
				'	'TCTOffset = 1.94
				'	'DruckOffset = 3.7
				'	TCTOffset = 1.93
				'	DruckOffset = 3.63
				'End If

				'post-process filter the waveform data
				Call Filter_Data(m_Waveform1, DATA_POINTS - 1)
				Call Filter_Data(m_Waveform2, DATA_POINTS - 1)

				m_TCT_Setting(iEyesetting).V_Druck(Shot) = Format((m_Waveform2(DATA_POINTS_MIDPT) - m_dDruckPressureBaseline) * VoltsPerStep, "0.00")

				m_PatApp.Text = "V(Druck) = " & m_TCT_Setting(iEyesetting).V_Druck(Shot)
				Form1.Refresh()

				'calculate and display pressure voltage at Druck Trigger
				m_TCT_Setting(iEyesetting).V_TCT(Shot) = Format((m_Waveform1(DATA_POINTS_MIDPT) - m_dTCTPressureBaseline) * TCT_VOLTS_PER_STEP + 0.17, "0.00")
				m_TCTPatApp.Text = "V(TCT) = " & m_TCT_Setting(iEyesetting).V_TCT(Shot)
				Form1.Refresh()


				'display the calculated IOPg to the user
				m_TCT_Setting(iEyesetting).Press_Druck(Shot) = (m_TCT_Setting(iEyesetting).V_Druck(Shot) - 0.2429) / 0.027
				m_TCTTrig.Text = Format(m_TCT_Setting(iEyesetting).Press_Druck(Shot), "00.00")
				Form1.Refresh()

				' Test for major problem with the TCT voltage.  6/20/11
				sngTempMax = m_DB_TCT_Trig_Volt.V_TCT_at_Druck_Trig(iEyesetting) + (m_DB_TCT_Trig_Volt.V_TCT_at_Druck_Trig(iEyesetting) * 0.15)
				sngTempMin = m_DB_TCT_Trig_Volt.V_TCT_at_Druck_Trig(iEyesetting) - (m_DB_TCT_Trig_Volt.V_TCT_at_Druck_Trig(iEyesetting) * 0.15)

				If (m_TCT_Setting(iEyesetting).V_TCT(Shot) > sngTempMax) Or (m_TCT_Setting(iEyesetting).V_TCT(Shot) < sngTempMin) Then
					'Single measurement fault
					m_msgResult = MsgBox("Fault: Check to ensure the TCT is set correctly" & vbCr & "Measured TCT Voltage: " & m_TCT_Setting(iEyesetting).V_TCT(Shot) & vbCr & "Expected TCT Voltage: " & (m_DB_TCT_Trig_Volt.V_TCT_at_Druck_Trig(iEyesetting)) & vbCr & vbCr & "Retry?", vbYesNo, "FAULT")
					If m_msgResult = MsgBoxResult.Yes Then
						GoTo RetryDrop
					Else
						m_Status.Text = "In StartCalVer(), TCT Voltage Fault"
						StartCalVer = False
						Exit Function
					End If
				End If

				'Test for a major problem with the Druck voltage. 6/20/11
				sngTempMax = m_DB_TCT_Trig_Volt.Druck_Trig(iEyesetting) + (m_DB_TCT_Trig_Volt.Druck_Trig(iEyesetting) * 0.2)
				sngTempMin = m_DB_TCT_Trig_Volt.Druck_Trig(iEyesetting) - (m_DB_TCT_Trig_Volt.Druck_Trig(iEyesetting) * 0.2)

				If (m_TCT_Setting(iEyesetting).V_Druck(Shot) > sngTempMax) Or (m_TCT_Setting(iEyesetting).V_Druck(Shot) < sngTempMin) Then
					'Single measurement fault
					m_msgResult = MsgBox("Fault: Check to ensure the TCT is set correctly" & vbCr & "Measured Druck Voltage: " & m_TCT_Setting(iEyesetting).V_Druck(Shot) & vbCr & "Expected Druck Voltage: " & (m_DB_TCT_Trig_Volt.Druck_Trig(iEyesetting)) & vbCr & vbCr & "Retry?", vbYesNo, "FAULT")
					If m_msgResult = MsgBoxResult.Yes Then
						GoTo RetryDrop
					Else
						m_Status.Text = "In StartCalVer(), Druck Voltage Fault"
						StartCalVer = False
						Exit Function
					End If
				End If

				' Calc Average and SD and save data to a text file
				Call Statistical(Shot, iEyesetting, Stages.STAGE_VERIFY, 2)

			Next Shot

			'-----------------------------------------------------------------
			' Perform druck and tct verification check here
			' H Setting
			'-----------------------------------------------------------------
			If iEyesetting = EYESETTING.EYESET_HI_C Then
				'check the H setting
				If (m_Averages(iEyesetting).AvgDruck > strtDruck.C_DRUCK_MAX) Or (m_Averages(iEyesetting).AvgDruck < strtDruck.C_DRUCK_MIN) Then
					MsgBox("This TCT has FAILED verification on the H Setting!" & vbCrLf & vbCrLf &
										  "Accepted Druck Voltage Range = " & Format(strtDruck.C_DRUCK_MIN, "0.00") & " - " & Format(strtDruck.C_DRUCK_MAX, "0.00") & " volts" & vbCrLf & vbCrLf &
										  "Measured Druck Voltage Average = " & m_Averages(iEyesetting).AvgDruck & " volts" & vbCrLf & vbCrLf &
										  "Please re-calibrate the TCT", vbCritical + vbOKOnly, "DRUCK H FAILURE")
					StartCalVer = FUNCTION_RETURN.FUNCTION_RETURN_RESTART
					Exit Function
				End If

				If (m_Averages(iEyesetting).AvgTCT > strtSetting.C_SETTING_MAX) Or (m_Averages(iEyesetting).AvgTCT < strtSetting.C_SETTING_MIN) Then
					MsgBox("This TCT has FAILED verification on the H Setting!" & vbCrLf & vbCrLf &
									  "Accepted TCT Voltage Range = " & Format(strtSetting.C_SETTING_MIN, "0.00") & " - " & Format(strtSetting.C_SETTING_MAX, "0.00") & " volts" & vbCrLf & vbCrLf &
									  "Measured Average TCT Voltage = " & m_Averages(iEyesetting).AvgTCT & " volts" & vbCrLf & vbCrLf &
									  "Please Call Engineering", vbCritical + vbOKOnly, "CALL ENGINEERING: TCT H FAILURE")
					StartCalVer = FUNCTION_RETURN.FUNCTION_RETURN_RESTART
					Exit Function
				End If

				'-----------------------------------------------------------------
				' Perform druck and tct verification check here
				' M Setting
				'-----------------------------------------------------------------
			ElseIf iEyesetting = EYESETTING.EYESET_MID_B Then
				'Check the M setting
				If (m_Averages(iEyesetting).AvgDruck > strtDruck.B_DRUCK_MAX) Or (m_Averages(iEyesetting).AvgDruck < strtDruck.B_DRUCK_MIN) Then
					MsgBox("This TCT has FAILED verification on the M Setting!" & vbCrLf & vbCrLf &
										  "Accepted Druck Voltage Range = " & Format(strtDruck.B_DRUCK_MIN, "0.00") & " - " & Format(strtDruck.B_DRUCK_MAX, "0.00") & " volts" & vbCrLf & vbCrLf &
										  "Measured Druck Voltage Average = " & m_Averages(iEyesetting).AvgDruck & " volts" & vbCrLf & vbCrLf &
										  "Please re-calibrate the TCT", vbCritical + vbOKOnly, "DRUCK M FAILURE")
					StartCalVer = FUNCTION_RETURN.FUNCTION_RETURN_RESTART
					Exit Function
				End If

				If (m_Averages(iEyesetting).AvgTCT > strtSetting.B_SETTING_MAX) Or (m_Averages(iEyesetting).AvgTCT < strtSetting.B_SETTING_MIN) Then
					MsgBox("This TCT has FAILED verification on the M Setting!" & vbCrLf & vbCrLf &
									  "Accepted TCT Voltage Range = " & Format(strtSetting.B_SETTING_MIN, "0.00") & " - " & Format(strtSetting.B_SETTING_MAX, "0.00") & " volts" & vbCrLf & vbCrLf &
									  "Measured Average TCT Voltage = " & m_Averages(iEyesetting).AvgTCT & " volts" & vbCrLf & vbCrLf &
									  "Please Call Engineering", vbCritical + vbOKOnly, "CALL ENGINEERING: TCT M FAILURE")
					StartCalVer = FUNCTION_RETURN.FUNCTION_RETURN_RESTART
					Exit Function
				End If

				'-----------------------------------------------------------------
				' Perform druck and tct verification check here
				' L Setting
				'-----------------------------------------------------------------
			ElseIf iEyesetting = EYESETTING.EYESET_LOW_A Then
				'Check the L setting
				If (m_Averages(iEyesetting).AvgDruck > strtDruck.A_DRUCK_MAX) Or (m_Averages(iEyesetting).AvgDruck < strtDruck.A_DRUCK_MIN) Then
					MsgBox("This TCT has FAILED verification on the L Setting!" & vbCrLf & vbCrLf &
										  "Accepted Druck Voltage Range = " & Format(strtDruck.A_DRUCK_MIN, "0.00") & " - " & Format(strtDruck.A_DRUCK_MAX, "0.00") & " volts" & vbCrLf & vbCrLf &
										  "Measured Druck Voltage Average = " & m_Averages(iEyesetting).AvgDruck & "volts" & vbCrLf & vbCrLf &
										  "Please re-calibrate the TCT", vbCritical + vbOKOnly, "DRUCK L FAILURE")
					StartCalVer = FUNCTION_RETURN.FUNCTION_RETURN_RESTART
					Exit Function
				End If

				If (m_Averages(iEyesetting).AvgTCT > strtSetting.A_SETTING_MAX) Or (m_Averages(iEyesetting).AvgTCT < strtSetting.A_SETTING_MIN) Then
					MsgBox("This TCT has FAILED verification on the L Setting!" & vbCrLf & vbCrLf &
									  "Accepted TCT Voltage Range = " & Format(strtSetting.A_SETTING_MIN, "0.00") & " - " & Format(strtSetting.A_SETTING_MAX, "0.00") & " volts" & vbCrLf & vbCrLf &
									  "Measured Average TCT Voltage = " & m_Averages(iEyesetting).AvgTCT & " volts" & vbCrLf & vbCrLf &
									  "Please Call Engineering", vbCritical + vbOKOnly, "CALL ENGINEERING: TCT L FAILURE")
					StartCalVer = FUNCTION_RETURN.FUNCTION_RETURN_RESTART
					Exit Function
				End If
			End If

		Next iEyesetting

		OutFileModule.WriteFile3(True, m_TCTSerialNum, m_OperatorName, MAX_NUM_SHOTS, m_TCT_Setting)


		For iIdx As Integer = 0 To DATA_POINTS - 1
			m_Waveform1(iIdx) = 0
			m_Waveform2(iIdx) = 0
		Next iIdx

		'If we get here, all 3 waveforms have passed
		'create a Certificate of Calibration
		OutFileModule.WriteFile4(False, m_TCTSerialNum, m_OperatorName, m_DB_TCT_Trig_Volt, m_Averages, MAX_NUM_SHOTS, m_iDynCal_Num, strtDruck, strtSetting)

		'clear waveforms from screen
		ClearCharts()

		StartCalVer = FUNCTION_RETURN.FUNCTION_RETURN_RESTART
	End Function

	Public Function StartDynCal() As FUNCTION_RETURN
		Dim NUM_SHOTS As Integer = 3
		Dim Shot As Integer
		Dim MaxPress As Long
		Dim VmaxPress(NUM_SHOTS) As Single
		Dim Index_Ramp_Start, Index_Ramp_Stop As Integer
		Dim Index, Index2 As Integer

		' Baseline Noise(of pressure signal) test limit:
		Const PBASELINEMAX = 13893      ' allow a max of 0.06 volts of baseline noise
		Const VOLTS_PER_STEP = 0.0000763  ' ONLY FOR .5VOLTS/DIVISION

		'Ramp Rate Testing constants:
		Const RAMPLOW_PSI = 19661    ' .5 volts(or .5 PSI)  ' Start voltage of ramp rate
		Const RAMPHIGH_PSI = 39321   ' 2 volts( or 2 PSI)   ' Stop voltage of ramp rate
		Const CNTS_PER_MS = 12.5    ' counts per ms (only if time scale is set-up as 4ms)
		' this is used for calulating time for Ramp rate calculation

		'Test Limits:

		Dim RampRate(NUM_SHOTS) As Single
		Dim BaseLineAvg As Long

		If m_CableSetup <> CABLE_SETUP.CABLE_SETUP_DYN_CAL Then
			MsgBox("Connect a BNC cable from The Dynamic Calibrator(Druck Sensor) ->Pressure Out to Channel 2 of TDS3032", vbOKOnly, "Make Connection")
			m_CableSetup = CABLE_SETUP.CABLE_SETUP_DYN_CAL
		End If

		If Not BaselinePressure() Then
			StartDynCal = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			Exit Function
		End If

		m_Label8.Text = "Druck Pressure Sensor"
		Form1.Refresh()

		' used for input box location between shots so it doesn't interfere with view
		'Xloc = Me.Width / 10
		'Yloc = Me.Height / (Me.Height / 10)


		MsgBox("Make sure the Dynamic Calibrator air tubes are sealed off", vbOKOnly, "ALL AIR TUBES SEALED?")


		'initialize/zero measured values
		For Index = 1 To NUM_SHOTS
			RampRate(Index) = 0
			VmaxPress(Index) = 0
		Next Index

		For Shot = 1 To NUM_SHOTS
RetryDrop:
			'Re-arm the Scope for another  Single sequence
			m_ioSerialPort.Send("ACQUIRE:MODE SAMPLE" & vbCr)
			TimeDelay(200)

			m_ioSerialPort.Send("ACQUIRE:STATE ON" & vbCr)
			TimeDelay(200)

			'set the trigger level
			m_ioSerialPort.Send("TRIGGER:A:LEVEL 0.5" & vbCr)
			TimeDelay(200)

			m_ioSerialPort.Send("ACQUIRE:STOPAFTER SEQUENCE" & vbCr)
			TimeDelay(200)

			MsgBox("Manually lift the drop handle and hold it up for 5 seconds" & vbCr & "Then release the weight on the TCT Dynamic calibrator", vbOKOnly, "DROP DEAD WEIGHT NOW")

			m_Status.Text = "Shot # " & Shot & " of " & NUM_SHOTS.ToString
			Form1.Refresh()

			'Read each waveform from the scope, and display them
			If Display_Waveforms(EYESETTING.EYESET_MID_B, Shot) = False Then
				'let the operator know
				m_msgResult = MsgBox("Did not read the signal - Retry?", vbYesNo, "SCOPE READING ERROR")
				If m_msgResult = MsgBoxResult.Yes Then
					GoTo RetryDrop
				Else
					m_Status.Text = "Scope reading error - no retry"
					StartDynCal = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
					Exit Function
				End If

			End If

			Call Filter_Data(m_Waveform2, DATA_POINTS - 1)

			'Calculate Peak Pressure
			MaxPress = 0

			For Index = 0 To DATA_POINTS - 1
				If m_Waveform2(Index) > MaxPress Then
					MaxPress = m_Waveform2(Index)
				End If
			Next Index

			'convert to voltage/pressure
			VmaxPress(Shot) = Format(Val((MaxPress - m_dDruckPressureBaseline) * VOLTS_PER_STEP), "0.00")
			m_PatApp.Text = "Pmax= " & Format(VmaxPress(Shot), "0.00")
			Form1.Refresh()


			'Calculate Ramp rate between .5 PSI and 2PSI
			For Index = 0 To DATA_POINTS - 1
				If m_Waveform2(Index) >= RAMPLOW_PSI Then
					'label start
					Index_Ramp_Start = Index

					For Index2 = Index To DATA_POINTS - 1
						If m_Waveform2(Index2) >= RAMPHIGH_PSI Then
							'label end
							Index_Ramp_Stop = Index2

							'done, get out of Index For Loop
							Index = DATA_POINTS - 1
							Index2 = DATA_POINTS - 1

						End If
					Next Index2
				End If
			Next Index

			If Index_Ramp_Start = Index_Ramp_Stop Then
				m_Status.Text = "In StartDynCal(), cannot calculate RampRate. Test ended in error."
				StartDynCal = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
				Exit Function
			End If
			'Is it a ramp or straight line


			' Calculate ramp now
			RampRate(Shot) = Format(1.5 / ((Index_Ramp_Stop - Index_Ramp_Start) / CNTS_PER_MS), "0.00")
			m_DruckStat.Text = "Ramp Rate(PSI/ms) = " & Format(RampRate(Shot), "0.00")
			Form1.Refresh()

			'Test the Results
			'First Test baseline voltage
			For Index = 0 To 9
				BaseLineAvg = m_Waveform2(Index)
			Next Index

			BaseLineAvg = BaseLineAvg / 10

			If BaseLineAvg > PBASELINEMAX Then
				'Fault
				m_Status.Text = "Fault: Baseline pressure too high. Meas= " & m_Waveform2(Index) & ". Allowed= " & PBASELINEMAX
				Form1.Refresh()
				MsgBox("Fault: Baseline pressure too high" & vbCr & "Meas= " & m_Waveform2(Index) & " Allowed= " & PBASELINEMAX & vbCr & vbCr & "Click on START TESTING again if you wish to perform another Round", vbOKOnly)

				'clear scope waveform dump arrays
				For iIdx As Integer = 0 To DATA_POINTS - 1
					m_Waveform1(iIdx) = 0
					m_Waveform2(iIdx) = 0
				Next iIdx

				StartDynCal = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
				Exit Function
			End If

			'Now test the Maximum pressure
			If (VmaxPress(Shot) > PMAX) Or (VmaxPress(Shot) < PMIN) Then
				'Fault
				m_Status.Text = "Fault: Max pressure = " & VmaxPress(Shot) & ". Allowed= " & PMIN & " - " & PMAX
				Form1.Refresh()
				MsgBox("CALIBRATION FAILURE!" & vbCrLf & vbCrLf & "Fault: Max pressure" & vbCr & "Meas =   " & VmaxPress(Shot) & " Allowed = " & PMIN & " - " & PMAX & vbCr & vbCr & "Please perform the test again.", vbCritical + vbOKOnly, "CALIBRATION FAILURE!")
				StartDynCal = FUNCTION_RETURN.FUNCTION_RETURN_RESTART
				Exit Function
			End If

			'Now test the ramp rate
			If (RampRate(Shot) > RRMAX) Or (RampRate(Shot) < RRMIN) Then
				'Fault
				m_Status.Text = "Fault: Ramp Rate = " & RampRate(Shot) & ". Allowed= " & RRMIN & " - " & RRMAX
				Form1.Refresh()
				MsgBox("Fault: Ramp Rate" & vbCr & "Meas= " & RampRate(Shot) & " Allowed= " & RRMIN & " - " & RRMAX & vbCr & vbCr & "Click on START again if you wish to perform another test.", vbOKOnly)

				'clear scope waveform dump arrays
				For iIdx As Integer = 0 To DATA_POINTS - 1
					m_Waveform1(iIdx) = 0
					m_Waveform2(iIdx) = 0
				Next iIdx

				StartDynCal = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			End If
		Next Shot


		'If we get here, all 3 waveforms have passed
		'Create a Certificate of Calibration
		OutFileModule.WriteFile5(False, m_TCTSerialNum, m_OperatorName, m_iDynCal_Num, RampRate, VmaxPress)

		'clear waveforms from screen
		ClearCharts()

		StartDynCal = FUNCTION_RETURN.FUNCTION_RETURN_RESTART

	End Function

	'**************************************************************************
	'  Function:       Statistical(Shot As Integer, Eyesetting As Integer)
	'
	'  Purpose:        Performs statistical calculations (average, standard deviation)
	'                  of the scope waveform data.
	'
	'  Resources:
	'
	'  Interactions:
	'
	'  Inputs:         -Eyesetting As Integer (1 = L, 2 = M, 3 = H)
	'                  -Shot As Integer (Indicates what shot in the sequence is being taken)
	'                  -stage determines whether this is being called during the correlation
	'                   curve setting period, or the verification period
	'
	'  Outputs:        -AvgTCT(Eyesetting), the average TCT voltages at a particular setting.
	'                  -SDTCT, standard deviation of TCT voltages at a particular setting.
	'                  -AvgDruck(Eyesetting), average Druck voltages at a particular setting.
	'                  -SDDruck, standard deviation of Druck voltages at a particular setting.
	'                  -AvgPress(Eyesetting), average of calculated IOPg at a particular setting.
	'                  -SDPress, standard deviation of calculated IOPg at a particular setting.
	'
	'  Dependencies:
	'  Err Conditions: none.
	'
	'  Notes:
	'
	'  History:        6/20/11 - D. Tolsma: Initial version
	'                  5/07/12 - D. Tolsma:  Added average and standard deviation calculations
	'                                        for the calculated IOPg function.
	'
	'***************************************************************************/

	Private Sub Statistical(ByVal Shot As Integer, ByVal iEyesetting As Integer, ByVal eStage As Stages, ByVal iFileID As Integer)
		Dim Index As Integer
		Dim TCT_sum, Druck_sum, Press_sum As Single
		Dim TCT_sumsq, Druck_sumsq, Press_sumsq As Single
		Dim V_TCT_Applan, V_TCT_appl_sumsq, V_TCT_appl_sum As Single

		'initialize variables for stats
		Press_sum = 0
		Press_sumsq = 0
		V_TCT_Applan = 0
		V_TCT_appl_sumsq = 0
		V_TCT_appl_sum = 0
		TCT_sum = 0
		Druck_sum = 0
		TCT_sumsq = 0
		Druck_sumsq = 0
		Dim SDDruck As Single = 0
		Dim SDTCT As Single = 0
		Dim SDPress As Single = 0

		For Index = 1 To Shot
			'Calculate sums
			If eStage = Stages.STAGE_VERIFY Then
				TCT_sum = TCT_sum + m_TCT_Setting(iEyesetting).V_TCT(Index)
				'TCT SD
				TCT_sumsq = TCT_sumsq + (m_TCT_Setting(iEyesetting).V_TCT(Index) ^ 2)
				Druck_sum = Druck_sum + m_TCT_Setting(iEyesetting).V_Druck(Index)
				Druck_sumsq = Druck_sumsq + (m_TCT_Setting(iEyesetting).V_Druck(Index) ^ 2)

				Press_sum = Press_sum + m_TCT_Setting(iEyesetting).Press_Druck(Index)
				Press_sumsq = Press_sumsq + (m_TCT_Setting(iEyesetting).Press_Druck(Index) ^ 2)
			ElseIf eStage = Stages.STAGE_CORRELATE Then
				TCT_sum = TCT_sum + m_V_Corr_TCT(iEyesetting, Index)
				'TCT SD
				TCT_sumsq = TCT_sumsq + (m_V_Corr_TCT(iEyesetting, Index) ^ 2)

				Druck_sum = Druck_sum + m_TCT_Setting(iEyesetting).V_Corr_Druck(Index)
				Druck_sumsq = Druck_sumsq + (m_TCT_Setting(iEyesetting).V_Corr_Druck(Index) ^ 2)
			End If

			If Shot > 1 Then
				'If Cal_Type = "TCT" Then 6/20/11
				' Do standard deviation (TCT)
				If eStage = Stages.STAGE_VERIFY Then
					SDTCT = Math.Abs(((Shot * TCT_sumsq) - (TCT_sum ^ 2)) / (Shot * (Shot - 1)))
					SDTCT = Format(Math.Sqrt(SDTCT), "0.000") 'was Sqr in old VB

					' Do standard deviation (Druck)
					SDDruck = Math.Abs(((Shot * Druck_sumsq) - (Druck_sum ^ 2)) / (Shot * (Shot - 1)))
					SDDruck = Format(Math.Sqrt(SDDruck), "0.000") 'was Sqr in old VB

					SDPress = Math.Abs(((Shot * Press_sumsq) - (Press_sum ^ 2)) / (Shot * (Shot - 1)))
					SDPress = Format(Math.Sqrt(SDPress), "0.000") 'was Sqr in old VB
					'End If
				ElseIf eStage = Stages.STAGE_CORRELATE Then
					SDTCT = Math.Abs(((Shot * TCT_sumsq) - (TCT_sum ^ 2)) / (Shot * (Shot - 1)))
					SDTCT = Format(Math.Sqrt(SDTCT), "0.000") 'was Sqr in old VB

					SDDruck = Math.Abs(((Shot * Druck_sumsq) - (Druck_sum ^ 2)) / (Shot * (Shot - 1)))
					SDDruck = Format(Math.Sqrt(SDDruck), "0.000") 'was Sqr in old VB
				End If
			End If
		Next Index

		'Calculate average
		m_Averages(iEyesetting).AvgTCT = Format((TCT_sum / Shot), "0.00")
		m_TCTStat.Text = "Avg.= " & m_Averages(iEyesetting).AvgTCT & "  SD = " & SDTCT

		m_Averages(iEyesetting).AvgDruck = Format((Druck_sum / Shot), "0.00") '6/20/11
		m_DruckStat.Text = "Avg.= " & m_Averages(iEyesetting).AvgDruck & " SD = " & SDDruck

		m_Averages(iEyesetting).AvgPress = Format((Press_sum / Shot), "00.00") '5/08/12
		If eStage = Stages.STAGE_VERIFY Then
			m_TCTTrigStat.Text = "Avg.= " & m_Averages(iEyesetting).AvgPress & " SD = " & SDPress
		End If
		Form1.Refresh()

		OutFileModule.AppendToFile(iFileID, m_TCTSerialNum, eStage, iEyesetting, Shot, MAX_NUM_SHOTS, SDTCT, SDDruck, m_Averages, m_TCT_Setting, m_V_Corr_TCT)
	End Sub

	'**************************************************************************
	'  Function:       First_Voltages()
	'
	'  Purpose:        Takes 5 drops on L, M, H settings and used the data
	'                  to establish correlation/linear curve parameters
	'                  for TCTs that do not have correlation data.  Determines
	'                  the slope and intercept of the linear curve. Performs a
	'                  check on the TCT voltage.  Will not establish correlation
	'                  data if TCT voltage fails.
	'
	'  Resources:
	'
	'  Interactions:   Writes MySQL database.
	'
	'  Inputs:
	'
	'  Outputs:
	'
	'  Dependencies:
	'  Err Conditions: none.
	'
	'  Notes:
	'
	'  History:        05/15/12 - D. Tolsma: Initial version
	'
	'***************************************************************************/
	Private Function First_Voltages() As FUNCTION_RETURN
		Dim Temp As Integer
		Dim VoltsPerStep As Single
		Dim iEyesetting As Integer
		Dim Setting As String = ""
		Dim Shot As Integer
		Dim Index As Integer
		Dim Index2 As Integer
		Dim Index_Ramp_Start, Index_Ramp_Stop As Integer
		Dim sngTempMax, sngTempMin As Single

		'variables to check TCT box
		Dim strtSetting As SettingMaxMin

		' used for input box location between shots so it doesn't interfere with view
		'Xloc = frmMain.Width / 10
		'Yloc = frmMain.Height / (frmMain.Height / 10)

		'determine the rest pressure
		If Not BaselinePressure() Then
			'm_Status.Text = "BaselinePressure() failure in FirstVoltages()"
			First_Voltages = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			Exit Function
		End If

		If m_OperatorName = "" Then
			GetOperatorName()
		End If

		OutFileModule.WriteFile6Header(True, m_TCTSerialNum, m_OperatorName)

		'fetch the fixed voltage triggers
		'reference off of TCT 0, which holds ONLY verification check values for the TCT box
		Dim sStatus As String = ""
		If Not Read_SQL_DB(0, m_DB_Fixed_TCT_Trig_Volt, sStatus) Then
			m_Status.Text = sStatus
			First_Voltages = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
			Exit Function
		End If

		If m_CableSetup <> CABLE_SETUP.CABLE_SETUP_FIVE_DROPS Then
			SetupCablesForFiveDrops()
		End If

		SetSettingToleranceLimits(strtSetting, True)

		For iEyesetting = EYESETTING.EYESET_HI_C To EYESETTING.EYESET_LOW_A Step -1

			'blank out the calculated iop data for this run
			m_TCTTrig.Text = ""
			m_TCTTrigStat.Text = ""
			Form1.Refresh()

			'send trigger-main-level
			m_ioSerialPort.Send("TRIG:MAIN:LEVEL " & m_DB_Fixed_TCT_Trig_Volt.Druck_Trig(iEyesetting) & vbCr)
			TimeDelay(200)

			If iEyesetting = EYESETTING.EYESET_LOW_A Then
				Setting = "L/A"
			ElseIf iEyesetting = EYESETTING.EYESET_MID_B Then
				Setting = "M/B"
			ElseIf iEyesetting = EYESETTING.EYESET_HI_C Then
				Setting = "H/C"
			End If

			MsgBox("Make sure the TCT is on the " & Setting & " position", vbOKOnly, "SET TCT TO THE " & Setting & " POSITION")

			For Shot = 1 To MAX_NUM_SHOTS

RetryDrop:
				'Re-arm the Scope for another  Single sequence
				m_ioSerialPort.Send("ACQUIRE:MODE SAMPLE" & vbCr)
				TimeDelay(200)

				m_ioSerialPort.Send("ACQUIRE:STATE ON" & vbCr)
				TimeDelay(200)

				m_ioSerialPort.Send("ACQUIRE:STOPAFTER SEQUENCE" & vbCr)
				TimeDelay(200)

				MsgBox("Manually lift and release the weight on the TCT Dynamic calibrator. Press OK when finished.", vbOKOnly, "DROP THE DEAD WEIGHT NOW")

				m_Status.Text = "TCT Position " & Setting & " Shot # " & Shot & " of " & MAX_NUM_SHOTS
				Form1.Refresh()

				'Read each waveform from the scope, and display them
				If Not Display_Waveforms(iEyesetting, Shot) Then
					'Check to make sure the scope got a signal
					'let the operator know
					m_msgResult = MsgBox("Did not read the signal - Retry?", vbYesNo, "SCOPE READING ERROR")
					If m_msgResult = MsgBoxResult.Yes Then
						GoTo RetryDrop
					Else
						m_Status.Text = "Scope reading error. User chose to exit."
						First_Voltages = FUNCTION_RETURN.FUNCTION_RETURN_ERROR
						Exit Function
					End If
				End If

				VoltsPerStep = 5 / 65535

				'Calculate Ramp rate
				For Index = 0 To DATA_POINTS - 1
					If m_Waveform1(Index) >= TCT_RAMPLOW_PSI Then
						'label start
						Index_Ramp_Start = Index

						For Index2 = Index To DATA_POINTS - 1
							If m_Waveform1(Index2) >= TCT_RAMPHIGH_PSI Then
								'label end
								Index_Ramp_Stop = Index2

								'done, get out of Index For Loop
								Index = DATA_POINTS - 1
								Index2 = DATA_POINTS - 1

							End If
						Next Index2
					End If
				Next Index

				' Calculate ramp now
				m_TCT_RampRate(iEyesetting, Shot) = Format(1.8 / ((Index_Ramp_Stop - Index_Ramp_Start) / TCT_CNTS_PER_MS), "0.00")
				m_PatApp.Text = "Ramp Rate(volt/ms) = " & Format(m_TCT_RampRate(iEyesetting, Shot), "0.00")
				Form1.Refresh()

				'post-process filter the waveform data
				Call Filter_Data(m_Waveform1, DATA_POINTS - 1)
				Call Filter_Data(m_Waveform2, DATA_POINTS - 1)

				m_TCT_Setting(iEyesetting).V_Corr_Druck(Shot) = Format((m_Waveform2(DATA_POINTS_MIDPT) - m_dDruckPressureBaseline) * VoltsPerStep, "0.00")

				m_PatApp.Text = "V(Druck) = " & m_TCT_Setting(iEyesetting).V_Corr_Druck(Shot)
				Form1.Refresh()

				'calculate and display pressure voltage at Druck Trigger
				''Tolsma
				m_V_Corr_TCT(iEyesetting, Shot) = Format((m_Waveform1(DATA_POINTS_MIDPT) - m_dTCTPressureBaseline) * TCT_VOLTS_PER_STEP + 0.17, "0.00")
				m_TCTPatApp.Text = "V(TCT) = " & m_V_Corr_TCT(iEyesetting, Shot)
				Form1.Refresh()

				sngTempMax = m_DB_Fixed_TCT_Trig_Volt.V_TCT_at_Druck_Trig(iEyesetting) + (m_DB_Fixed_TCT_Trig_Volt.V_TCT_at_Druck_Trig(iEyesetting) * 0.15)
				sngTempMin = m_DB_Fixed_TCT_Trig_Volt.V_TCT_at_Druck_Trig(iEyesetting) - (m_DB_Fixed_TCT_Trig_Volt.V_TCT_at_Druck_Trig(iEyesetting) * 0.15)

				If (m_V_Corr_TCT(iEyesetting, Shot) > sngTempMax) Or (m_V_Corr_TCT(iEyesetting, Shot) < sngTempMin) Then
					'Single measurement fault
					m_msgResult = MsgBox("Fault: Check to ensure the TCT is set correctly" & vbCr & "Measured TCT Voltage: " & m_V_Corr_TCT(iEyesetting, Shot) & vbCr & "Expected TCT Voltage: " & (m_DB_Fixed_TCT_Trig_Volt.V_TCT_at_Druck_Trig(iEyesetting)) & vbCr & vbCr & "Retry?", vbYesNo, "FAULT")
					If m_msgResult = MsgBoxResult.Yes Then
						GoTo RetryDrop
					Else
						m_Status.Text = "In First_Voltages(), TCT Voltage Fault"
						First_Voltages = False
						Exit Function
					End If
				End If

				'Test for a major problem with the Druck voltage. 6/20/11
				sngTempMax = m_DB_Fixed_TCT_Trig_Volt.Druck_Trig(iEyesetting) + (m_DB_Fixed_TCT_Trig_Volt.Druck_Trig(iEyesetting) * 0.2)
				sngTempMin = m_DB_Fixed_TCT_Trig_Volt.Druck_Trig(iEyesetting) - (m_DB_Fixed_TCT_Trig_Volt.Druck_Trig(iEyesetting) * 0.2)

				If (m_TCT_Setting(iEyesetting).V_Corr_Druck(Shot) > sngTempMax) Or (m_TCT_Setting(iEyesetting).V_Corr_Druck(Shot) < sngTempMin) Then
					'Single measurement fault
					m_msgResult = MsgBox("Fault: Check to ensure the TCT is set correctly" & vbCr & "Measured Druck Voltage: " & m_TCT_Setting(iEyesetting).V_Corr_Druck(Shot) & vbCr & "Expected Druck Voltage: " & (m_DB_Fixed_TCT_Trig_Volt.Druck_Trig(iEyesetting)) & vbCr & vbCr & "Retry?", vbYesNo, "FAULT")
					If m_msgResult = MsgBoxResult.Yes Then
						GoTo RetryDrop
					Else
						m_Status.Text = "In First_Voltages(), Druck Voltage Fault"
						First_Voltages = False
						Exit Function
					End If
				End If


				' Calc Average and SD and save data to a text file
				Call Statistical(Shot, iEyesetting, Stages.STAGE_CORRELATE, 6)

			Next Shot


			'perform tct voltage check here
			If iEyesetting = EYESETTING.EYESET_HI_C Then
				'check the H setting for TCT Box

				If (m_Averages(iEyesetting).AvgTCT > strtSetting.C_SETTING_MAX) Or (m_Averages(iEyesetting).AvgTCT < strtSetting.C_SETTING_MIN) Then
					MsgBox("This TCT has FAILED verification on the H Setting!" & vbCrLf & vbCrLf &
								  "Accepted TCT Voltage Range = " & Format(strtSetting.C_SETTING_MIN, "0.00") & " - " & Format(strtSetting.C_SETTING_MAX, "0.00") & " volts" & vbCrLf & vbCrLf &
								  "Measured Average TCT Voltage = " & m_Averages(iEyesetting).AvgTCT & " volts" & vbCrLf & vbCrLf &
								  "Please Call Engineering", vbCritical + vbOKOnly, "CALL ENGINEERING: TCT H FAILURE")
					ClearCharts()
					First_Voltages = FUNCTION_RETURN.FUNCTION_RETURN_RESTART
					Exit Function
				End If

			ElseIf iEyesetting = EYESETTING.EYESET_MID_B Then
				'Check the M setting for TCT Box

				If (m_Averages(iEyesetting).AvgTCT > strtSetting.B_SETTING_MAX) Or (m_Averages(iEyesetting).AvgTCT < strtSetting.B_SETTING_MIN) Then
					MsgBox("This TCT has FAILED verification on the M Setting!" & vbCrLf & vbCrLf &
								  "Accepted TCT Voltage Range = " & Format(strtSetting.B_SETTING_MIN, "0.00") & " - " & Format(strtSetting.B_SETTING_MAX, "0.00") & " volts" & vbCrLf & vbCrLf &
								  "Measured Average TCT Voltage = " & m_Averages(iEyesetting).AvgTCT & " volts" & vbCrLf & vbCrLf &
								  "Please Call Engineering", vbCritical + vbOKOnly, "CALL ENGINEERING: TCT M FAILURE")
					ClearCharts()
					First_Voltages = FUNCTION_RETURN.FUNCTION_RETURN_RESTART
					Exit Function
				End If

			ElseIf iEyesetting = EYESETTING.EYESET_LOW_A Then
				'Check the L setting for TCT Box

				If (m_Averages(iEyesetting).AvgTCT > strtSetting.A_SETTING_MAX) Or (m_Averages(iEyesetting).AvgTCT < strtSetting.A_SETTING_MIN) Then
					MsgBox("This TCT has FAILED verification on the L Setting!" & vbCrLf & vbCrLf &
								  "Accepted TCT Voltage Range = " & Format(strtSetting.A_SETTING_MIN, "0.00") & " - " & Format(strtSetting.A_SETTING_MAX, "0.00") & " volts" & vbCrLf & vbCrLf &
								  "Measured Average TCT Voltage = " & m_Averages(iEyesetting).AvgTCT & " volts" & vbCrLf & vbCrLf &
								  "Please Call Engineering", vbCritical + vbOKOnly, "CALL ENGINEERING: TCT L FAILURE")
					ClearCharts()
					First_Voltages = FUNCTION_RETURN.FUNCTION_RETURN_RESTART
					Exit Function
				End If
			End If
		Next iEyesetting

		OutFileModule.WriteFile7(True, m_TCTSerialNum, m_OperatorName, MAX_NUM_SHOTS, m_V_Corr_TCT)

		For Temp = 0 To DATA_POINTS - 1
			m_Waveform1(Temp) = 0
			m_Waveform2(Temp) = 0
		Next Temp

		'write first time TCT voltages
		Call Write_SQL_DB(m_TCTSerialNum, "Motorola_A_Voltage", CStr(m_Averages(EYESETTING.EYESET_LOW_A).AvgTCT))
		Call Write_SQL_DB(m_TCTSerialNum, "Motorola_B_Voltage", CStr(m_Averages(EYESETTING.EYESET_MID_B).AvgTCT))
		Call Write_SQL_DB(m_TCTSerialNum, "Motorola_C_Voltage", CStr(m_Averages(EYESETTING.EYESET_HI_C).AvgTCT))

		'write first time Druck_Voltages
		Call Write_SQL_DB(m_TCTSerialNum, "Druck_A_Voltage", CStr(m_Averages(EYESETTING.EYESET_LOW_A).AvgDruck))
		Call Write_SQL_DB(m_TCTSerialNum, "Druck_B_Voltage", CStr(m_Averages(EYESETTING.EYESET_MID_B).AvgDruck))
		Call Write_SQL_DB(m_TCTSerialNum, "Druck_C_Voltage", CStr(m_Averages(EYESETTING.EYESET_HI_C).AvgDruck))

		'clear waveforms from screen
		ClearCharts()

		First_Voltages = FUNCTION_RETURN.FUNCTION_RETURN_RESTART
	End Function

	'**************************************************************************
	'  Function:       Corr_Fit(N As Single)
	'
	'  Purpose:        Takes N paired data-points (x,y) and uses least-squared
	'                  regression to return the slope and intercept/offset
	'                  of the line of best fit.  Calculates R^2 value for the
	'                  line of best fit.  Notifies the user of failure and halts
	'                  execution if R^2 is less than 0.975.
	'
	'  Resources:
	'
	'  Interactions:   Writes MySQL database.
	'
	'  Inputs:
	'
	'  Outputs:        TCT_Slope - slope of linear fit
	'                  TCT_Offset - offset of linear fit
	'
	'  Dependencies:
	'  Err Conditions: none.
	'
	'  Notes:
	'
	'  History:        05/15/12 - D. Tolsma: Initial version
	'
	'***************************************************************************/

	Private Sub Corr_Fit(N As Single)
		Dim sStatus As String = ""
		Dim SumX As Single
		Dim SumY As Single
		Dim SumX2 As Single
		Dim SumY2 As Single
		Dim SumXY As Single
		Dim i As Integer

		Dim TCT_R2 As Single
		Dim TCT_Slope As Single
		Dim TCT_Offset As Single

		'y is voltage, x is IOP

		SumX = 0
		SumY = 0
		SumX2 = 0
		SumY2 = 0
		SumXY = 0
		TCT_R2 = 0
		'grab the data
		Call Read_SQL_DB(m_TCTSerialNum, m_DB_TCT_Trig_Volt, sStatus)

		'generate Sumx, SumY
		For i = 0 To 2
			SumX = SumX + CSng(m_DB_TCT_Trig_Volt.IOP_TCT(i))
			SumY = SumY + CSng(m_DB_TCT_Trig_Volt.Druck_Trig(i))
			SumX2 = SumX2 + (CSng(m_DB_TCT_Trig_Volt.IOP_TCT(i))) ^ 2
			SumY2 = SumY2 + (CSng(m_DB_TCT_Trig_Volt.Druck_Trig(i))) ^ 2
			SumXY = SumXY + (CSng(m_DB_TCT_Trig_Volt.IOP_TCT(i)) * CSng(m_DB_TCT_Trig_Volt.Druck_Trig(i)))
		Next i

		'calculate least squares fit parameters
		TCT_Slope = (N * SumXY - SumX * SumY) / (N * SumX2 - (SumX) ^ 2)
		TCT_Offset = (SumY - TCT_Slope * SumX) / N

		TCT_R2 = ((N * SumXY - SumX * SumY) / (Math.Sqrt(N * SumX2 - (SumX) ^ 2) * Math.Sqrt(N * SumY2 - (SumY) ^ 2))) ^ 2

		If TCT_R2 < 0.975 Then
			MsgBox("Error.  The R^2 coefficient for Measured Druck Voltage vs. Cal Sheet IOPg is too low!" & vbCrLf & vbCrLf &
						  "Measured R^2 = " & TCT_R2 & vbCrLf & vbCrLf &
						  "Minimum Accepted R^2 = 0.975" & vbCrLf & vbCrLf &
						  "Please call Engineering.", vbCritical + vbOKOnly, "CORRELATION ERROR")
		End If

		'write parameters to the database
		Call Write_SQL_DB(m_TCTSerialNum, "Slope", CStr(TCT_Slope))
		Call Write_SQL_DB(m_TCTSerialNum, "Offset", CStr(TCT_Offset))
		Call Write_SQL_DB(m_TCTSerialNum, "R2", CStr(TCT_R2))

		'find the tolerance settings
	End Sub

	'**************************************************************************
	'  Function:       New_TCT()
	'
	'  Purpose:        Writes cal sheet data IOPg (L, M, H) values to the
	'                  MySQL database if the TCT under verification doesn
	'                  not have correlation data.
	'
	'  Resources:
	'
	'  Interactions:   Writes MySQL database.
	'
	'  Inputs:
	'
	'  Outputs:
	'
	'  Dependencies:
	'  Err Conditions: none.
	'
	'  Notes:
	'
	'  History:        05/15/12 - D. Tolsma: Initial version
	'
	'***************************************************************************/
	Private Function New_TCT() As Boolean
		Dim Response As String = ""

		'write the IOPg (mmHg) L value
		'****************************************************************************
L:
		Response = InputBox("From the calbiration sheet for TCT " & m_TCTSerialNum & " please enter:" & vbCrLf & vbCrLf & vbCrLf & "IOPg (mmHg) L")

		If Response = "" Or Not IsNumeric(Response) Then
			m_msgResult = MsgBox("Would you like to quit?", vbYesNo, "QUIT?")
			If m_msgResult = MsgBoxResult.Yes Then
				m_Status.Text = "In New_TCT(), user quit."
				New_TCT = False
				Exit Function
			Else
				GoTo L
			End If
		End If

		m_DB_TCT_Trig_Volt.IOP_TCT(EYESETTING.EYESET_LOW_A) = CSng(Response)

		If Not Write_SQL_DB(m_TCTSerialNum, "IOPg_A", Response) Then
			m_Status.Text = "In New_TCT(), Write IOPg_A to DB failed."
			New_TCT = False
			Exit Function
		End If

		'****************************************************************************

		'write the IOPg (mmHg) M Value
		'****************************************************************************
M:
		Response = InputBox("From the calbiration sheet for TCT " & m_TCTSerialNum & " please enter:" & vbCrLf & vbCrLf & vbCrLf & "IOPg (mmHg) M")

		If Response = "" Or Not IsNumeric(Response) Then
			m_msgResult = MsgBox("Would you like to quit?", vbYesNo, "QUIT?")
			If m_msgResult = MsgBoxResult.Yes Then
				m_Status.Text = "In New_TCT(), user quit."
				New_TCT = False
				Exit Function
			Else
				GoTo M
			End If
		End If

		m_DB_TCT_Trig_Volt.IOP_TCT(EYESETTING.EYESET_MID_B) = CSng(Response)
		If Not Write_SQL_DB(m_TCTSerialNum, "IOPg_B", Response) Then
			m_Status.Text = "In New_TCT(), Write IOPg_B to DB failed."
			New_TCT = False
			Exit Function
		End If


		'****************************************************************************

		'Write the IOPg (mmHg) H value
		'****************************************************************************
H:
		Response = InputBox("From the calbiration sheet for TCT " & m_TCTSerialNum & " please enter:" & vbCrLf & vbCrLf & vbCrLf & "IOPg (mmHg) H")

		If Response = "" Or Not IsNumeric(Response) Then
			m_msgResult = MsgBox("Would you like to quit?", vbYesNo, "QUIT?")
			If m_msgResult = MsgBoxResult.Yes Then
				m_Status.Text = "In New_TCT(), user quit."
				New_TCT = False
				Exit Function
			Else
				GoTo H
			End If
		End If

		m_DB_TCT_Trig_Volt.IOP_TCT(EYESETTING.EYESET_HI_C) = CSng(Response)
		If Not Write_SQL_DB(m_TCTSerialNum, "IOPg_C", Response) Then
			m_Status.Text = "In New_TCT(), Write IOPg_C to DB failed."
			New_TCT = False
			Exit Function
		End If
		'****************************************************************************
		New_TCT = True
	End Function


	'**************************************************************************
	'  Function:       New_Transducer()
	'
	'  Purpose:        This will allow the user to clear the SQL database
	'
	'
	'  Resources:      User input
	'
	'  Interactions:
	'
	'  Inputs:
	'
	'  Outputs:
	'
	'  Dependencies:
	'  Err Conditions: none.
	'
	'  Notes:
	'
	'  History:        08/10/16 - J. Akin: Initial version
	'***************************************************************************/
	Private Function New_Transducer() As Boolean
		Dim Response As String = ""
		'-------------------------------------------------------------------------
		' Check to see if the Transducer was Changed
		'-------------------------------------------------------------------------
		m_msgResult = MsgBox("Was the Transducer Changed?" & vbCr & vbCr &
			"If YES: The settings will be cleared" & vbCr &
			"If NO: The test will use existing settings", vbYesNo + vbDefaultButton2, "NEW TRANSDUCER?")
		If m_msgResult = MsgBoxResult.Yes Then
			m_msgResult = MsgBox("Are you sure you want to clear the TCT settings?", vbYesNo + vbDefaultButton2, "CLEAR SETTINGS?")
			If m_msgResult = MsgBoxResult.Yes Then
				'clear settings
				Call Write_SQL_DB(m_TCTSerialNum, "IOPg_A", "")
				Call Write_SQL_DB(m_TCTSerialNum, "IOPg_B", "")
				Call Write_SQL_DB(m_TCTSerialNum, "IOPg_C", "")
				'write first time TCT voltages
				Call Write_SQL_DB(m_TCTSerialNum, "Motorola_A_Voltage", "") 'CStr(m_Averages(EYESETTING.EYESET_LOW_A).AvgTCT))
				Call Write_SQL_DB(m_TCTSerialNum, "Motorola_B_Voltage", "") ' CStr(m_Averages(EYESETTING.EYESET_MID_B).AvgTCT))
				Call Write_SQL_DB(m_TCTSerialNum, "Motorola_C_Voltage", "") ' CStr(m_Averages(EYESETTING.EYESET_HI_C).AvgTCT))
				'write first time Druck_Voltages
				Call Write_SQL_DB(m_TCTSerialNum, "Druck_A_Voltage", "") ' CStr(m_Averages(EYESETTING.EYESET_LOW_A).AvgDruck))
				Call Write_SQL_DB(m_TCTSerialNum, "Druck_B_Voltage", "") ' CStr(m_Averages(EYESETTING.EYESET_MID_B).AvgDruck))
				Call Write_SQL_DB(m_TCTSerialNum, "Druck_C_Voltage", "") ' CStr(m_Averages(EYESETTING.EYESET_HI_C).AvgDruck))
				'write parameters to the database
				Call Write_SQL_DB(m_TCTSerialNum, "Slope", "")
				Call Write_SQL_DB(m_TCTSerialNum, "Offset", "")
				Call Write_SQL_DB(m_TCTSerialNum, "R2", "")

				'now write default values
				If Not New_TCT() Then
					New_Transducer = False
					Exit Function
				End If
			End If
		End If

		New_Transducer = True
	End Function

End Module
