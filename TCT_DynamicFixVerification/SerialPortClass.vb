﻿'****************************************************************************
'
'	File:		SerialPortClass.vb
'
'	Project:	
'
'	History:    02/05/18 - J. Dillon: Initial Version 
'
'	Purpose:	This file defines a class that handles serial port access and usage.
'
'	Functions:	Sub ClearInputBuffer()
'               Sub Close()
'               Sub GetAvailableSerialPortNames(ByRef sPortNames() As String)
'               IsOpen() As Boolean
'               Sub New (Constructor)
'               OpenPort(ByVal sName As String, ByRef sMsg As String) As Boolean
'               Receive(ByRef sRecvData As String) As Boolean
'               Send(ByVal strToSend As String) as Boolean
'               Send(ByVal strToSend As String, ByRef strReceived As String, ByVal sngSeconds As Single) As Boolean
'               SendWithVerification(ByVal strToSend As String, ByVal strToRecv As String, 
'                                    ByVal sngSeconds As Single) As Boolean
'               SendWithVerification(ByVal strToSend As String, ByVal strToRecv As String, 
'                                    ByRef strReceived As String, ByVal sngSeconds As Single) As Boolean
'               Sub SetPortData(ByVal iBaudRate As Integer, ByVal iDataBits As Integer, 
'                               ByVal ioParity As IO.Ports.Parity, ByVal ioStopBits As IO.Ports.StopBits)
'               Sub SetPortName(ByVal sPortName As String)
'
'	Functions affecting patient safety:	none.
'
'	Copyright (C) Reichert Inc. 2018.  All rights reserved.
'
'***************************************************************************
Public Class SerialPortClass
    Public Const MAX_COM_PORTS As Integer = 16
    Private m_SerialPort As System.IO.Ports.SerialPort

    '****************************************************************************
    '
    '	Name:			Sub ClearInputBuffer()
    '
    '	Purpose:		Clear/discard the current contents of the input buffer.
    '
    '	Resources:		NONE
    '
    '	Interactions:	NONE
    '
    '	Inputs:			NONE
    '
    '	Outputs:		NONE
    '
    '	Dependencies: 	NONE
    '
    '	Err Conditions:	NONE
    '
    '	Notes:		    
    '
    '	History:		02/05/18 - J. Dillon: Created.
    '
    '***************************************************************************
    Public Sub ClearInputBuffer()
        Try
            m_SerialPort.DiscardInBuffer()
        Catch ex As Exception
            MessageBox.Show("Port Error: " & ex.Message, "Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

	'****************************************************************************
	'
	'	Name:			Sub Close()
	'
	'	Purpose:		Close the serial port
	'
	'	Resources:		NONE
	'
	'	Interactions:	NONE
	'
	'	Inputs:			NONE
	'
	'	Outputs:		NONE
	'
	'	Dependencies: 	NONE
	'
	'	Err Conditions:	NONE
	'
	'	Notes:		    
	'
	'	History:		02/05/18 - J. Dillon: Created.
	'
	'***************************************************************************
	Public Sub Close()
		Try
			If m_SerialPort.IsOpen() Then
				m_SerialPort.Close()
			End If
		Catch ex As Exception
			MessageBox.Show("Port Error: " & ex.Message, "Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
		End Try
	End Sub

	'****************************************************************************
	'
	'	Name:			IsOpen() As Boolean
	'
	'	Purpose:		Check if the port is open
	'
	'	Resources:		NONE
	'
	'	Interactions:	NONE
	'
	'	Inputs:			NONE
	'
	'	Outputs:		NONE
	'
	'	Dependencies: 	NONE
	'
	'	Err Conditions:	NONE
	'
	'	Notes:		    
	'
	'	History:		02/05/18 - J. Dillon: Created.
	'
	'***************************************************************************
	Public Function IsOpen() As Boolean
        Try
            IsOpen = m_SerialPort.IsOpen
        Catch ex As Exception
            IsOpen = False
        End Try
    End Function

    '****************************************************************************
    '
    '	Name:			New (Constructor)
    '
    '	Purpose:		Create an instance, set defaults
    '
    '	Resources:		NONE
    '
    '	Interactions:	NONE
    '
    '	Inputs:			NONE
    '
    '	Outputs:		NONE
    '
    '	Dependencies: 	NONE
    '
    '	Err Conditions:	NONE
    '
    '	Notes:		    
    '
    '	History:		02/05/18 - J. Dillon: Created.
    '
    '***************************************************************************
    Public Sub New()
		m_SerialPort = New System.IO.Ports.SerialPort With {
		.BaudRate = 19200,
		.DataBits = 8,
		.Parity = IO.Ports.Parity.None,
		.StopBits = IO.Ports.StopBits.One
		}
	End Sub

    '****************************************************************************
    '
    '	Name:			OpenPort(ByVal sName As String, ByRef sMsg As String) As Boolean
    '
    '	Purpose:		Opens a serial port
    '
    '	Resources:		NONE
    '
    '	Interactions:	NONE
    '
    '	Inputs:			sName - The name of the port to be opened
    '                   sMsg - Error message as to why opening the port failed
    '
    '	Outputs:		Returns TRUE if port opened successfully
    '
    '	Dependencies: 	NONE
    '
    '	Err Conditions:	NONE
    '
    '	Notes:		    
    '
    '	History:		02/05/18 - J. Dillon: Created.
    '
    '***************************************************************************
    Public Function OpenPort(ByVal sName As String, ByRef sMsg As String) As Boolean
        Dim bSuccess As Boolean = False

        If m_SerialPort.IsOpen Then
            m_SerialPort.Close()
        End If

        Try
            m_SerialPort.PortName = sName
			m_SerialPort.Open()
			bSuccess = True
        Catch ex As Exception
            m_SerialPort.Close()
            bSuccess = False
            sMsg = $"Port Error: {ex.Message}"
        End Try

        OpenPort = bSuccess
    End Function

    '****************************************************************************
    '
    '	Name:			Receive(ByRef sRecvData As String) As Boolean
    '
    '	Purpose:		Checks COM port for received bytes and copies them to a buffer if present.
    '
    '	Resources:		NONE
    '
    '	Interactions:	NONE
    '
    '	Inputs:			sRecvData - Received data copied to this
    '
    '	Outputs:		Return True if bytes recevied and False if no data available
    '
    '	Dependencies: 	NONE
    '
    '	Err Conditions:	NONE
    '
    '	Notes:		    
    '
    '	History:		02/05/18 - J. Dillon: Created.
    '
    '***************************************************************************
    Public Function Receive(ByRef sRecvData As String) As Boolean
        Dim strBuffer As String = ""
        Try
            If m_SerialPort.IsOpen Then
                If m_SerialPort.BytesToRead > 0 Then
                    strBuffer = StrConv(m_SerialPort.ReadExisting, VbStrConv.None)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Port Error: " & ex.Message, "Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            strBuffer = ""
        End Try

        sRecvData = strBuffer
        Receive = Len(strBuffer) > 0
    End Function

    '****************************************************************************
    '
    '	Name:			Function Send(ByVal strToSend As String) as Boolean
    '
    '	Purpose:		Writes a string to the serial port
    '
    '	Resources:		NONE
    '
    '	Interactions:	NONE
    '
    '	Inputs:			strToSend - String to write
    '
    '	Outputs:		Returns TRUE if port is functioning
    '
    '	Dependencies: 	NONE
    '
    '	Err Conditions:	NONE
    '
    '	Notes:		    
    '
    '	History:		02/05/18 - J. Dillon: Created.
    '
    '***************************************************************************
    Public Function Send(ByVal strToSend As String) As Boolean
        Try
            m_SerialPort.Write(strToSend)
            Send = True
        Catch ex As Exception
            MessageBox.Show("Port Error: " & ex.Message, "Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Send = False
        End Try
    End Function

	Public Function SendReadByteArray(ByVal strToSend As String, ByRef bytArray() As Byte, ByVal sngSeconds As Single) As Boolean
		Dim strBuffer(1500) As Byte
		Dim CommTimer As New Stopwatch()
		Dim ElapsedTime As TimeSpan
		Dim iIdx As Integer = 0
		Dim bEndFound As Boolean = False
		Dim bytCurr, bytPrev As Byte

		bytCurr = 0
		bytPrev = 0

		Try
			' Clear input buffer
			m_SerialPort.DiscardInBuffer()

			' Send command
			m_SerialPort.Write(strToSend)

			' Start timer to wait for response
			CommTimer.Start()
			Do
				Do While m_SerialPort.BytesToRead > 0

					strBuffer(iIdx) = m_SerialPort.ReadByte
					bytCurr = strBuffer(iIdx)
					If iIdx > 0 Then
						bytPrev = strBuffer(iIdx - 1)
					End If
					iIdx += 1

					If bytPrev = 13 And bytCurr = 10 Then
						bEndFound = True
						ReDim Preserve strBuffer(iIdx - 3)
					End If

					' Check if time elasped
					ElapsedTime = CommTimer.Elapsed
					If ElapsedTime.TotalSeconds > sngSeconds Or bEndFound Then
						Exit Do
					End If
				Loop
				ElapsedTime = CommTimer.Elapsed
			Loop Until (ElapsedTime.TotalSeconds > sngSeconds Or bEndFound)
			SendReadByteArray = True
		Catch ex As Exception
			MessageBox.Show("Port Error: " & ex.Message, "Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
			SendReadByteArray = False
		End Try
		bytArray = strBuffer
	End Function

	'****************************************************************************
	'
	'	Name:			Function Send(ByVal strToSend As String, ByRef strReceived As String, 
	'                            ByVal sngSeconds As Single) As Boolean
	'
	'	Purpose:		Writes a string to the serial port, waits for the specified 
	'                   amount of time then reads any incoming data (response) on 
	'                   the port.
	'
	'	Resources:		NONE
	'
	'	Interactions:	NONE
	'
	'	Inputs:			strToSend - String to write
	'                   strReceived - The data received on the port
	'                   sngSeconds - Time to wait for any incoming port data
	'
	'	Outputs:		Returns TRUE if port is functioning
	'
	'	Dependencies: 	NONE
	'
	'	Err Conditions:	NONE
	'
	'	Notes:		    
	'
	'	History:		02/05/18 - J. Dillon: Created.
	'
	'***************************************************************************
	Public Function Send(ByVal strToSend As String, ByRef strReceived As String, ByVal sngSeconds As Single) As Boolean
        Dim strBuffer As String = ""
        Dim CommTimer As New Stopwatch()
        Dim ElapsedTime As TimeSpan

        Try
			' Clear input buffer
			m_SerialPort.DiscardInBuffer()

			' Send command
			m_SerialPort.Write(strToSend)

            ' Start timer to wait for response
            CommTimer.Start()
			Do
				Do While m_SerialPort.BytesToRead > 0
					strBuffer = strBuffer & StrConv(m_SerialPort.ReadExisting, VbStrConv.None)

					' Check if time elasped
					ElapsedTime = CommTimer.Elapsed
					If ElapsedTime.TotalSeconds > sngSeconds Then
						Exit Do
					End If
				Loop
				ElapsedTime = CommTimer.Elapsed
			Loop Until (ElapsedTime.TotalSeconds > sngSeconds)
			Send = True
        Catch ex As Exception
            MessageBox.Show("Port Error: " & ex.Message, "Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Send = False
        End Try
        strReceived = strBuffer
    End Function

    '****************************************************************************
    '
    '	Name:			SendWithVerification(ByVal strToSend As String, ByVal strToRecv As String, 
    '                                        ByVal sngSeconds As Single) As Boolean
    '
    '	Purpose:		Sends a command via COM and waits for an expected response.
    '                   Will read the input buffer until time has elapsed OR the expected 
    '                   String was found.
    '
    '	Resources:		NONE
    '
    '	Interactions:	NONE
    '
    '   Inputs:         strToSend - Command to send
    '                   strToRecv - Expecting to receive this
    '                   strReceived - All the data received
    '                   sngSeconds - Timeout for send/receive attempt
    '
    '	Outputs:		strReceived - The actual string received from COM
    '                   Return value FALSE if COM error
    '
    '	Dependencies: 	NONE
    '
    '	Err Conditions:	NONE
    '
    '	Notes:		    
    '
    '	History:		02/05/18 - J. Dillon: Created.
    '
    '***************************************************************************
    Public Function SendWithVerification(ByVal strToSend As String, ByVal strToRecv As String, ByVal sngSeconds As Single) As Boolean
        Dim strBuffer As String = ""
        Dim bVerificationFound = False

        Dim CommTimer As New Stopwatch()
        Dim ElapsedTime As TimeSpan

        Try
            m_SerialPort.DiscardInBuffer()

            ' Send command
            m_SerialPort.Write(strToSend)

            ' Start timer to wait for response
            CommTimer.Start()
            ElapsedTime = CommTimer.Elapsed

			Do ' While waiting for expected response or timeout
				' Build the response string
				Do While m_SerialPort.BytesToRead > 0
					strBuffer = strBuffer & StrConv(m_SerialPort.ReadExisting, VbStrConv.None)
				Loop

				bVerificationFound = (InStr(strBuffer, strToRecv) > 0)

				' Check if time elasped before checking for more bytes to read
				ElapsedTime = CommTimer.Elapsed
			Loop Until (bVerificationFound Or ElapsedTime.TotalSeconds > sngSeconds)

		Catch ex As Exception
            MessageBox.Show("Port Error: " & ex.Message, "Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        SendWithVerification = bVerificationFound

    End Function

	'****************************************************************************
	'
	'	Name:			SendWithVerification(ByVal strToSend As String, ByVal strToRecv As String, 
	'                                        ByRef strReceived As String, ByVal sngSeconds As Single) As Boolean
	'
	'	Purpose:		Sends a command via COM and waits for an expected response. 
	'                   Will keep reading the input buffer until the specified time has expired.
	'
	'	Resources:		NONE
	'
	'	Interactions:	NONE
	'
	'   Inputs:         strToSend - Command to send
	'                   strToRecv - Expecting to receive this
	'                   strReceived - All the data received
	'                   sngSeconds - Timeout for send/receive attempt
	'
	'	Outputs:		strReceived - The actual string received from COM
	'                   Return value FALSE if COM error
	'
	'	Dependencies: 	NONE
	'
	'	Err Conditions:	NONE
	'
	'	Notes:		    
	'
	'	History:		02/05/18 - J. Dillon: Created.
	'
	'***************************************************************************
	Public Function SendWithVerification(ByVal strToSend As String, ByVal strToRecv As String, ByRef strReceived As String, ByVal sngSeconds As Single) As Boolean
		Dim strBuffer As String = ""
		Dim bVerificationFound = False

		Dim CommTimer As New Stopwatch()
		Dim ElapsedTime As TimeSpan

		Try
			m_SerialPort.DiscardInBuffer()

			' Send command
			m_SerialPort.Write(strToSend)

			' Start timer to wait for response
			CommTimer.Start()
			ElapsedTime = CommTimer.Elapsed

			Do ' While waiting for expected response or timeout
				' Build the response string
				Do While m_SerialPort.BytesToRead > 0
					strBuffer = strBuffer & StrConv(m_SerialPort.ReadExisting, VbStrConv.None)
				Loop

				bVerificationFound = (InStr(strBuffer, strToRecv) > 0)

				' Check if time elasped before checking for more bytes to read
				ElapsedTime = CommTimer.Elapsed
			Loop Until (ElapsedTime.TotalSeconds > sngSeconds)

		Catch ex As Exception
			MessageBox.Show("Port Error: " & ex.Message, "Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
		End Try

		strReceived = strBuffer
		SendWithVerification = bVerificationFound
	End Function
End Class
